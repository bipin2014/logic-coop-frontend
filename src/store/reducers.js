import { combineReducers } from 'redux';
import AuthReducers from '../modules/auth/store/AuthReducers';
import ToasterReducer from '../modules/components/Toast/store/ToasterReducer';
import DashboardReducer from '../modules/dashboard/store/DashboardReducer';
const rootReducer = combineReducers({
  authReducer: AuthReducers,
  data: DashboardReducer,
  toasterReducer: ToasterReducer,
});
export default rootReducer;
