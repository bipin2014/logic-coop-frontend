import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ROUTELIST from './routeList';

const AuthRoute = () => {
  return (
    <>
      <React.Suspense fallback={<div>Loading...</div>}>
        <Switch>
          {ROUTELIST.map((route, key) => (
            <Route
              path={route.path}
              name={route.name}
              component={route.component}
              exact={route.exact}
              key={key}
            />
          ))}
        </Switch>
      </React.Suspense>
    </>
  );
};
export default AuthRoute;
