import Login from '../Login';
import Register from '../Register';

// eslint-disable-next-line
export default [
  {
    path: '/',
    name: 'login',
    exact: true,
    component: Login,
  },
  {
    path: '/login',
    name: 'login',
    exact: true,
    component: Login,
  },
  {
    path: '/register',
    name: 'register',
    exact: true,
    component: Register,
  },
];
