import jwt_decode from 'jwt-decode';
import { API_URL } from '../../../config';
import { PostRequest } from '../../../utils/axios';
import { setToasterState } from '../../components/Toast/store/ToasterAction';
export const loginSuccess = (payload) => {
  return {
    type: 'LOGIN_SUCCESS',
    payload: payload,
  };
};

export const logoutSuccess = () => {
  return {
    type: 'LOGOUT',
  };
};

export const loginFailed = () => {
  return {
    type: 'LOGIN_FAIL',
  };
};

export const setLoading = (payload) => {
  return {
    type: 'LOADING',
    payload: payload,
  };
};

export const authenticate =
  (payload = {}) =>
  (dispatch) => {
    dispatch(setLoading(true));
    PostRequest(
      API_URL + '/login',
      {
        email: payload.email,
        password: payload.password,
      },
      {}
    )
      .then((response) => {
        console.log(response.data);
        dispatch(loginSuccess(response.data));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Welcome Back!',
            message: 'Login Success!',
          })
        );
      })
      .catch((errors) => {
        dispatch(setLoading(false));
        dispatch(loginFailed());
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message: errors?.response?.data?.message || 'Login Failed!',
          })
        );
      });
  };

export const reauthenticate = () => (dispatch) => {
  try {
    let token = localStorage.getItem('token');
    if (token && token !== 'token') {
      //skip this
      let decode = jwt_decode(token);
      if (decode?.exp > Date.now()/1000) {
        dispatch(loginSuccess({ token: token }));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Welcome Back!',
            message: 'Sucess Reauthentication!',
          })
        );
      } else {
        console.log('token expires');
        logout()(dispatch);
      }
    }
  } catch (e) {
    console.log('token expires');
    logout()(dispatch);
  }
};

export const logout = () => (dispatch) => {
  localStorage.removeItem('token');
  dispatch(logoutSuccess());
  dispatch(
    setToasterState({
      open: true,
      title: 'success',
      name: 'See You!',
      message: 'Logout Success!',
    })
  );
};
