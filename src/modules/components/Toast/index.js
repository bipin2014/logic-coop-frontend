import { Snackbar, Typography } from '@mui/material';
import Alert from '@mui/material/Alert';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from './store/ToasterAction';

export default function Toast() {
  const { message, open, title, name } = useSelector(
    (state) => state.toasterReducer
  );
  const showTime = 5000;
  const dispatch = useDispatch();

  const handleClose = () => {
    const toasterData = {
      open: false,
      type: '',
      name: '',
      message: '',
    };
    dispatch(actions.setToasterState(toasterData));
  };
  return (
    <Snackbar open={open} autoHideDuration={showTime} onClose={handleClose}>
      <Alert onClose={handleClose} severity={title}>
        <Typography variant='h6' color={title}>
          {name}
        </Typography>
        <div> {message}</div>
      </Alert>
    </Snackbar>
  );
}
