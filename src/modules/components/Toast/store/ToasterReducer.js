import * as actionTypes from './ToasterType';
const initialState = {
  open: false,
  title: '',
  name: '',
  message: '',
};

const ToasterReducer = (state = initialState, action) => {
  const { type, open, title, name, message } = action;
  switch (type) {
    case actionTypes.SET_TOASTER:
      return {
        ...state,
        open: open,
        title: title,
        name: name,
        message: message,
      };
    default:
      return state;
  }
};

export default ToasterReducer;
