import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import {
  addClientTransaction,
  editClientTransaction,
  setSuccess,
} from '../../../dashboard/store/client/ClientActions';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../../config';
import { fDate } from '../../../../utils/formatTime';

// ----------------------------------------------------------------------

export default function AddTransactionForm({
  handleClose,
  client,
  transaction,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const employees = useSelector((state) => state.data.employees);
  const [nepaliDate, setNepaliDate] = useState(new NepaliDate());
  // let nepaliDate = new NepaliDate();
  let date = new Date();

  useEffect(() => {
    dispatch(setSuccess(false));
    dispatch(getTransactionTypes());
    dispatch(getEmployees());

    console.log(nepaliDate);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    transaction &&
      setNepaliDate(new NepaliDate(new Date(transaction.transaction_date)));
  }, [transaction]);

  const TransactionSchema = Yup.object().shape({
    amount: Yup.number().required('Amount is required'),
    transaction_type_id: Yup.number().required('TransactionType is required'),
    employee_id: Yup.number().required('Employee  is required'),
    transaction_date: Yup.date().required('Transaction Date is required'),
  });

  const formik = useFormik({
    initialValues: {
      amount: transaction?.amount || '',
      transaction_type_id: transaction?.transaction_type_id || '',
      remarks: transaction?.remarks || '',
      employee_id: transaction?.employee_id || '',
      transaction_date:
        (transaction?.transaction_date &&
          fDate(transaction.transaction_date)) ||
        `${date.getFullYear()}-${checkOneDigitDate(
          date.getMonth() + 1
        )}-${checkOneDigitDate(date.getDate())}`,
      type: 'normal',
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      let nepalidate = new NepaliDate(new Date(data.transaction_date)).getBS();
      data.nepali_year = nepalidate.year;
      data.nepali_month = checkOneDigitDate(nepalidate.month + 1);
      data.nepali_date = checkOneDigitDate(nepalidate.date);

      let transactionType = transactionTypes.find(
        (transactionType) => transactionType.id === data.transaction_type_id
      );
      if (
        transactionType?.action === 'DECREASE_LOAN_BALANCE' ||
        transactionType?.action === 'INCREASE_LOAN_BALANCE'
      ) {
        data.type = 'loan';
      } else {
        data.type = 'normal';
      }
      console.log(data);
      if (transaction) {
        dispatch(
          editClientTransaction(data, client.account_number, transaction.id)
        );
      } else {
        dispatch(addClientTransaction(data, client.account_number));
      }

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            type='text'
            label='Account Number'
            value={client.account_number}
            disabled={true}
          />

          <TextField
            fullWidth
            type='string'
            label='Amount'
            {...getFieldProps('amount')}
            error={Boolean(touched.amount && errors.amount)}
            helperText={touched.amount && errors.amount}
          />
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Transaction Type'
              {...getFieldProps('transaction_type_id')}
              error={Boolean(
                touched.transaction_type_id && errors.transaction_type_id
              )}
              helperText={
                touched.transaction_type_id && errors.transaction_type_id
              }
            >
              {transactionTypes.map((transactionType) => (
                <MenuItem key={transactionType.id} value={transactionType.id}>
                  {transactionType.name}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              fullWidth
              select
              label='Employee'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employees.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <TextField
            fullWidth
            type='date'
            label='Date'
            {...getFieldProps('transaction_date')}
            error={Boolean(touched.transaction_date && errors.transaction_date)}
            helperText={touched.transaction_date && errors.transaction_date}
          />
          <Typography variant='body1' gutterBottom>
            Nepali Date: {nepaliDate.format('ddd, MMMM-DD YYYY')} B.S.
          </Typography>

          <TextField
            type='text'
            label='Remarks'
            {...getFieldProps('remarks')}
            error={Boolean(touched.remarks && errors.remarks)}
            helperText={touched.remarks && errors.remarks}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {transaction ? 'Edit Transaction' : 'Add Transaction'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
