import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider, FieldArray, getIn } from 'formik';
// material
import {
  Stack,
  TextField,
  MenuItem,
  Button,
  Autocomplete,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  addMultipleTransaction,
  setSuccess,
} from '../../../dashboard/store/client/ClientActions';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../../config';
import { Box } from '@mui/system';

// ----------------------------------------------------------------------

export default function NewMultipleTransactionForm({
  handleClose,
  memberAccounts,
  transactionTypeAccount,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [transactionTypeList, setTransactionTypeList] = useState([]);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const [nepaliDate] = useState(new NepaliDate());
  // let nepaliDate = new NepaliDate();
  let date = new Date();

  useEffect(() => {
    dispatch(setSuccess(false));
    dispatch(getTransactionTypes());

    console.log(nepaliDate);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (transactionTypes) {
      const transactionTypeList = transactionTypes.filter(
        (transactionType) =>
          transactionType.account_type === transactionTypeAccount
      );
      setTransactionTypeList(transactionTypeList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [transactionTypes]);

  const TransactionSchema = Yup.object().shape({
    transaction_date: Yup.date().required('Transaction Date Required'), // these constraints take precedence
    transactions: Yup.array().of(
      Yup.object().shape({
        member_account_id: Yup.string().required('Member Account is Required'), // these constraints take precedence
        amount: Yup.number().required('Amount  is required'), // these constraints take precedence
        transaction_type_id: Yup.number().required(
          'Transaction Type  is required'
        ), // these constraints take precedence
      })
    ),
  });

  const formik = useFormik({
    initialValues: {
      remarks: '',
      transaction_date: `${date.getFullYear()}-${checkOneDigitDate(
        date.getMonth() + 1
      )}-${checkOneDigitDate(date.getDate())}`,
      transactions: [
        {
          amount: '',
          transaction_type_id: '',
          member_account_id: '',
        },
      ],
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      data.transactions.forEach((_, index) => {
        data.transactions[index].employee_id = data.employee_id;
        data.transactions[index].transaction_date = data.transaction_date;
        data.transactions[index].remarks = '';
        if (transactionTypeAccount === 'deposit') {
          data.transactions[index].member_loan_account_id = null;
        } else if (transactionTypeAccount === 'loan') {
          data.transactions[index].member_loan_account_id =
            data.transactions[index].member_account_id;
          data.transactions[index].member_account_id = null;
        }
      });

      console.log(data);

      dispatch(addMultipleTransaction(data));

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    values,
    setValues,
    setSubmitting,
  } = formik;

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  const changeClient = (value, index) => {
    values.transactions[index] = {
      ...values.transactions[index],
      member_account_id: value?.id,
    };
    console.log(values);
    setValues({
      ...values,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='date'
              label='Date'
              {...getFieldProps(`transaction_date`)}
              error={Boolean(
                getIn(touched, `transaction_date`) &&
                  getIn(errors, `transaction_date`)
              )}
              helperText={
                getIn(touched, `transaction_date`) &&
                getIn(errors, `transaction_date`)
              }
            />
          </Stack>
          <FieldArray
            name='transactions'
            render={(arrayHelpers) =>
              values.transactions &&
              values.transactions.length > 0 &&
              values.transactions.map((transacation, index) => (
                <Stack direction='row' spacing={1}>
                  <Autocomplete
                    fullWidth
                    options={memberAccounts}
                    onChange={(e, newValue) => changeClient(newValue, index)}
                    autoHighlight
                    getOptionLabel={(option) =>
                      `${option.member.name} ${option.account_number}`
                    }
                    renderOption={(props, option) => (
                      <Box component='li' sx={{ width: 250 }} {...props}>
                        {option.member.name} - {option.account_number}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label='Choose a member account'
                        inputProps={{
                          ...params.inputProps,
                        }}
                      />
                    )}
                  />
                  <TextField
                    fullWidth
                    select
                    label='Transaction Type'
                    {...getFieldProps(
                      `transactions[${index}].transaction_type_id`
                    )}
                    error={Boolean(
                      getIn(
                        touched,
                        `transactions[${index}].transaction_type_id`
                      ) &&
                        getIn(
                          errors,
                          `transactions[${index}].transaction_type_id`
                        )
                    )}
                    helperText={
                      getIn(
                        touched,
                        `transactions[${index}].transaction_type_id`
                      ) &&
                      getIn(
                        errors,
                        `transactions[${index}].transaction_type_id`
                      )
                    }
                  >
                    {transactionTypeList.map((transactionType) => (
                      <MenuItem
                        key={transactionType.id}
                        value={transactionType.id}
                      >
                        {transactionType.name}
                      </MenuItem>
                    ))}
                  </TextField>

                  <TextField
                    fullWidth
                    type='string'
                    label='Amount'
                    {...getFieldProps(`transactions[${index}].amount`)}
                    error={Boolean(
                      getIn(touched, `transactions[${index}].amount`) &&
                        getIn(errors, `transactions[${index}].amount`)
                    )}
                    helperText={
                      getIn(touched, `transactions[${index}].amount`) &&
                      getIn(errors, `transactions[${index}].amount`)
                    }
                  />
                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                  >
                    X
                  </Button>

                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() =>
                      arrayHelpers.push({
                        ...transacation,
                        amount: '',
                      })
                    } // insert an empty string at a position
                  >
                    Add
                  </Button>
                </Stack>
              ))
            }
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Transaction
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
