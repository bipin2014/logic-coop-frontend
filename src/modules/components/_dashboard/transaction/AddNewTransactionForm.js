import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { setSuccess } from '../../../dashboard/store/client/ClientActions';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../../config';
import { fDate } from '../../../../utils/formatTime';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';
import 'nepali-datepicker-reactjs/dist/index.css';
import {
  addTransaction,
  updateTransaction,
} from '../../../dashboard/store/transaction/TransactionActions';

// ----------------------------------------------------------------------

export default function AddNewTransactionForm({
  handleClose,
  memberAccount,
  transaction,
  transactionTypeAccount,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [transactionTypeList, setTransactionTypeList] = useState([]);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const [nepaliDate, setNepaliDate] = useState('');
  // const [nepaliDate, setNepaliDate] = useState('')
  const date = new Date();

  useEffect(() => {
    dispatch(setSuccess(false));
    dispatch(getTransactionTypes());

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log(nepaliDate);
  }, [nepaliDate]);

  useEffect(() => {
    if (transactionTypes) {
      const transactionTypeList = transactionTypes.filter(
        (transactionType) =>
          transactionType.account_type === transactionTypeAccount
      );
      setTransactionTypeList(transactionTypeList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [transactionTypes]);

  useEffect(() => {
    transaction &&
      setNepaliDate(new NepaliDate(new Date(transaction.transaction_date)));
  }, [transaction]);

  const TransactionSchema = Yup.object().shape({
    amount: Yup.number().required('Amount is required'),
    transaction_type_id: Yup.number().required('TransactionType is required'),
    transaction_date: Yup.date().required('Transaction Date is required'),
  });

  const formik = useFormik({
    initialValues: {
      amount: transaction?.amount || '',
      transaction_type_id: transaction?.transaction_type_id || '',
      remarks: transaction?.remarks || '',
      transaction_date:
        (transaction?.transaction_date &&
          fDate(transaction.transaction_date)) ||
        `${date.getFullYear()}-${checkOneDigitDate(
          date.getMonth() + 1
        )}-${checkOneDigitDate(date.getDate())}`,
      type: 'normal',
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      if (transactionTypeAccount === 'deposit') {
        data.member_account_id = memberAccount.id;
        data.member_loan_account_id = null;
      } else if (transactionTypeAccount === 'loan') {
        data.member_account_id = null;
        data.member_loan_account_id = memberAccount.id;
      }
      if (transaction) {
        dispatch(updateTransaction(data, transaction.id));
      } else {
        dispatch(addTransaction(data));
      }

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    values,
    setValues,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  useEffect(() => {
    if (values.transaction_date) {
      let date = new NepaliDate(new Date(values.transaction_date)).getBS();
      console.log(date);
      setNepaliDate(
        `${date.year}-${checkOneDigitDate(date.month + 1)}-${checkOneDigitDate(
          date.date
        )}`
      );
    }
  }, [values.transaction_date]);

  const changeNepaliDate = (value) => {
    console.log(value);
    setNepaliDate(value);
    let date = new NepaliDate(value);
    // setNepaliDate(date);
    let englishDate = date.getAD();
    console.log(
      `${englishDate.year}/${checkOneDigitDate(
        englishDate.month + 1
      )}/${checkOneDigitDate(englishDate.date)}`
    );
    setValues({
      ...values,
      transaction_date: `${englishDate.year}-${checkOneDigitDate(
        englishDate.month + 1
      )}-${checkOneDigitDate(englishDate.date)}`,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            type='text'
            label='Account Number'
            value={memberAccount.account_number}
            disabled={true}
          />

          <TextField
            fullWidth
            type='string'
            label='Amount'
            {...getFieldProps('amount')}
            error={Boolean(touched.amount && errors.amount)}
            helperText={touched.amount && errors.amount}
          />
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Transaction Type'
              {...getFieldProps('transaction_type_id')}
              error={Boolean(
                touched.transaction_type_id && errors.transaction_type_id
              )}
              helperText={
                touched.transaction_type_id && errors.transaction_type_id
              }
            >
              {transactionTypeList.map((transactionType) => (
                <MenuItem key={transactionType.id} value={transactionType.id}>
                  {transactionType.name}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <Stack spacing={2} direction={'row'}>
            <TextField
              type='date'
              label='Date'
              {...getFieldProps('transaction_date')}
              error={Boolean(
                touched.transaction_date && errors.transaction_date
              )}
              helperText={touched.transaction_date && errors.transaction_date}
            />
            <NepaliDatePicker
              onChange={changeNepaliDate}
              value={nepaliDate}
              options={{ calenderLocale: 'en', valueLocale: 'en' }}
            />
          </Stack>

          <TextField
            type='text'
            label='Remarks'
            {...getFieldProps('remarks')}
            error={Boolean(touched.remarks && errors.remarks)}
            helperText={touched.remarks && errors.remarks}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {transaction ? 'Edit Transaction' : 'Add Transaction'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
