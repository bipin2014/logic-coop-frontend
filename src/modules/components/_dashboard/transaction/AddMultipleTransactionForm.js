import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider, FieldArray, getIn } from 'formik';
// material
import {
  Stack,
  TextField,
  MenuItem,
  Button,
  Switch,
  FormControlLabel,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import {
  addMultipleTransaction,
  setSuccess,
} from '../../../dashboard/store/client/ClientActions';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../../config';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';

// ----------------------------------------------------------------------

export default function AddMultipleTransactionForm({
  handleClose,
  client,
  transaction,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const employees = useSelector((state) => state.data.employees);
  const [useNepaliDate, setUseNepaliDate] = useState(false);
  const [nepaliDate, setNepaliDate] = useState(new NepaliDate());
  // let nepaliDate = new NepaliDate();
  let date = new Date();

  useEffect(() => {
    dispatch(setSuccess(false));
    dispatch(getTransactionTypes());
    dispatch(getEmployees());

    console.log(nepaliDate);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log(transaction);
    transaction &&
      setNepaliDate(new NepaliDate(new Date(transaction.transaction_date)));
  }, [transaction]);

  const TransactionSchema = Yup.object().shape({
    employee_id: Yup.number().required('Employee  is required'),
    transactions: Yup.array().of(
      Yup.object().shape({
        transaction_date: Yup.date().required('Transaction Date Required'), // these constraints take precedence
        amount: Yup.number().required('Amount  is required'), // these constraints take precedence
        transaction_type_id: Yup.number().required(
          'Transaction Type  is required'
        ), // these constraints take precedence
      })
    ),
  });

  const formik = useFormik({
    initialValues: {
      remarks: transaction?.remarks || '',
      employee_id: transaction?.employee_id || '',
      transactions: [
        {
          amount: '',
          transaction_type_id: '',
          transaction_date: `${date.getFullYear()}-${checkOneDigitDate(
            date.getMonth() + 1
          )}-${checkOneDigitDate(date.getDate())}`,
        },
      ],
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      data.transactions.forEach((_, index) => {
        let nepalidate = new NepaliDate(
          new Date(data.transactions[index].transaction_date)
        ).getBS();
        data.transactions[index].nepali_year = nepalidate.year;
        data.transactions[index].nepali_month = checkOneDigitDate(
          nepalidate.month + 1
        );
        data.transactions[index].nepali_date = checkOneDigitDate(
          nepalidate.date
        );
        data.transactions[index].acc_no = client.account_number;
        data.transactions[index].employee_id = data.employee_id;

        let transaction = transactionTypes.find(
          (transactionType) =>
            transactionType.id === data.transactions[index].transaction_type_id
        );
        if (
          transaction?.action === 'DECREASE_LOAN_BALANCE' ||
          transaction?.action === 'INCREASE_LOAN_BALANCE'
        ) {
          data.transactions[index].type = 'loan';
        } else {
          data.transactions[index].type = 'normal';
        }
      });

      console.log(data);

      dispatch(addMultipleTransaction(data));

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    values,
    setValues,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  useEffect(() => {
    console.log({ errors, touched });
    console.log(getIn(touched, 'transactions[0].transaction_type_id'));
    // eslint-disable-next-line
  }, [errors, touched]);

  const changeNepaliDate = (value, index) => {
    let date = new NepaliDate(value);
    let nepalidate = date.getAD();

    values.transactions[index] = {
      ...values.transactions[index],
      transaction_date: `${nepalidate.year}-${checkOneDigitDate(
        nepalidate.month + 1
      )}-${checkOneDigitDate(nepalidate.date)}`,
    };
    setValues({
      ...values,
    });
  };

  const changeSwitchNepaliDate = (e) => {
    setUseNepaliDate(!useNepaliDate);
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='text'
              label='Account Number'
              value={client.account_number}
              disabled={true}
            />

            <TextField
              fullWidth
              select
              label='Employee'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employees.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>
            <FormControlLabel
              control={
                <Switch
                  value={useNepaliDate}
                  onChange={changeSwitchNepaliDate}
                />
              }
              label='Nepali Date'
            />
          </Stack>
          <FieldArray
            name='transactions'
            render={(arrayHelpers) =>
              values.transactions &&
              values.transactions.length > 0 &&
              values.transactions.map((transacation, index) => (
                <Stack direction='row' spacing={2}>
                  <TextField
                    fullWidth
                    select
                    label='Transaction Type'
                    {...getFieldProps(
                      `transactions[${index}].transaction_type_id`
                    )}
                    error={Boolean(
                      getIn(
                        touched,
                        `transactions[${index}].transaction_type_id`
                      ) &&
                        getIn(
                          errors,
                          `transactions[${index}].transaction_type_id`
                        )
                    )}
                    helperText={
                      getIn(
                        touched,
                        `transactions[${index}].transaction_type_id`
                      ) &&
                      getIn(
                        errors,
                        `transactions[${index}].transaction_type_id`
                      )
                    }
                  >
                    {transactionTypes.map((transactionType) => (
                      <MenuItem
                        key={transactionType.id}
                        value={transactionType.id}
                      >
                        {transactionType.name}
                      </MenuItem>
                    ))}
                  </TextField>
                  {useNepaliDate ? (
                    <NepaliDatePicker
                      onChange={(value) => changeNepaliDate(value, index)}
                    />
                  ) : (
                    <TextField
                      fullWidth
                      type='date'
                      label='Date'
                      {...getFieldProps(
                        `transactions[${index}].transaction_date`
                      )}
                      error={Boolean(
                        getIn(
                          touched,
                          `transactions[${index}].transaction_date`
                        ) &&
                          getIn(
                            errors,
                            `transactions[${index}].transaction_date`
                          )
                      )}
                      helperText={
                        getIn(
                          touched,
                          `transactions[${index}].transaction_date`
                        ) &&
                        getIn(errors, `transactions[${index}].transaction_date`)
                      }
                    />
                  )}

                  <TextField
                    fullWidth
                    type='string'
                    label='Amount'
                    {...getFieldProps(`transactions[${index}].amount`)}
                    error={Boolean(
                      getIn(touched, `transactions[${index}].amount`) &&
                        getIn(errors, `transactions[${index}].amount`)
                    )}
                    helperText={
                      getIn(touched, `transactions[${index}].amount`) &&
                      getIn(errors, `transactions[${index}].amount`)
                    }
                  />
                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                  >
                    X
                  </Button>

                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() =>
                      arrayHelpers.push({
                        ...transacation,
                        amount: '',
                      })
                    } // insert an empty string at a position
                  >
                    Add
                  </Button>
                </Stack>
              ))
            }
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Transaction
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
