import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { removeEmptyKeyFromObject } from '../../../../config';
import {
  addMember,
  updateMember,
} from '../../../dashboard/store/member/MemberActions';

// ----------------------------------------------------------------------

const accounts = ['Individual', 'Company', 'Group', 'Others'];

export default function AddMemberForm({ handleClose, member }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const MemberSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    account: Yup.string().required('Account is required'),
  });

  const formik = useFormik({
    initialValues: {
      name: member?.name || '',
      account: member?.account || '',
      email: member?.email || '',
      phone: member?.phone || '',
      address: member?.address || '',
      dob: member?.dob || '',
      gender: member?.gender || '',
      share_number: member?.share_number || '',
    },
    validationSchema: MemberSchema,
    onSubmit: (data) => {
      // console.log(data);
      const newdata = removeEmptyKeyFromObject(data);
      console.log(newdata);
      if (member) {
        dispatch(updateMember(newdata, member.id));
      } else {
        dispatch(addMember(newdata));
      }
      setSubmit(true);
      // handleClose();
      // navigate('/dashboard', { replace: true });
    },
  });

  useEffect(() => {
    if (success && submit) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [success, submit]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              autoComplete='name'
              type='text'
              label='Full Name'
              {...getFieldProps('name')}
              error={Boolean(touched.name && errors.name)}
              helperText={touched.name && errors.name}
            />
            <TextField
              fullWidth
              type='text'
              label='Share Number'
              {...getFieldProps('share_number')}
              error={Boolean(touched.share_number && errors.share_number)}
              helperText={touched.share_number && errors.share_number}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              autoComplete='username'
              type='email'
              label='Email address'
              {...getFieldProps('email')}
              error={Boolean(touched.email && errors.email)}
              helperText={touched.email && errors.email}
            />
            <TextField
              fullWidth
              autoComplete='phone'
              type='string'
              label='Phone Number'
              {...getFieldProps('phone')}
              error={Boolean(touched.phone && errors.phone)}
              helperText={touched.phone && errors.phone}
            />
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Gender'
              {...getFieldProps('gender')}
              error={Boolean(touched.gender && errors.gender)}
              helperText={touched.gender && errors.gender}
            >
              <MenuItem key={'M'} value={'M'}>
                Male
              </MenuItem>
              <MenuItem key={'F'} value={'F'}>
                Female
              </MenuItem>
            </TextField>

            <TextField
              fullWidth
              select
              label='Member Type'
              {...getFieldProps('account')}
              error={Boolean(touched.account && errors.account)}
              helperText={touched.account && errors.account}
            >
              {accounts.map((account) => (
                <MenuItem key={account} value={account}>
                  {account}
                </MenuItem>
              ))}
            </TextField>
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='text'
              label='Full Address'
              {...getFieldProps('address')}
              error={Boolean(touched.address && errors.address)}
              helperText={touched.address && errors.address}
            />
            <TextField
              fullWidth
              InputLabelProps={{ shrink: true }}
              type='date'
              label='Date of Birth'
              {...getFieldProps('dob')}
              error={Boolean(touched.dob && errors.dob)}
              helperText={touched.dob && errors.dob}
            />
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {member ? 'Update Member' : 'Add Member'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
