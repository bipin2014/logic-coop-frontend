import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  addBankAccount,
  updateBankAccount,
} from '../../../dashboard/store/bank/BankActions';

// ----------------------------------------------------------------------

export default function AddBankAccountForm({ handleClose, bankAccount }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const BankAccountSchema = Yup.object().shape({
    name: Yup.string().required('Bank Name is required'),
    account_number: Yup.string().required('Account Number is required'),
  });

  const formik = useFormik({
    initialValues: {
      name: bankAccount?.name || '',
      account_number: bankAccount?.account_number || '',
    },
    validationSchema: BankAccountSchema,
    onSubmit: (data) => {
      console.log(data);
      if (bankAccount) {
        dispatch(updateBankAccount(data, bankAccount.id));
      } else {
        dispatch(addBankAccount(data));
      }
      setSubmit(true);
    },
  });

  useEffect(() => {
    if (success && submit) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [success, submit]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              autoComplete='name'
              type='text'
              label='Full Name'
              {...getFieldProps('name')}
              error={Boolean(touched.name && errors.name)}
              helperText={touched.name && errors.name}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              autoComplete='username'
              type='text'
              label='Account Number'
              {...getFieldProps('account_number')}
              error={Boolean(touched.account_number && errors.account_number)}
              helperText={touched.account_number && errors.account_number}
            />
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {bankAccount ? 'Update Member' : 'Add Member'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
