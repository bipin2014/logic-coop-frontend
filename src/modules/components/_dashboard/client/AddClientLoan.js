import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import {
  addClientLoan,
  editClientLoan,
} from '../../../dashboard/store/client/ClientActions';
import NepaliDate from 'nepali-date-converter';
import {
  addMonthToDate,
  checkOneDigitDate,
  getFormatedDateFromDateObject,
} from '../../../../config';
import { monthList, yearList } from '../../../_mocks_/datelist';

// ----------------------------------------------------------------------

export default function AddClientLoan({ handleClose, client, loanAccount }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);

  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);

  let nepaliDate = new NepaliDate();

  useEffect(() => {
    dispatch(getEmployees());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (loanAccount && loanAccount.loan_date) {
      const nepalidate = new NepaliDate(
        new Date(loanAccount.loan_date)
      ).getBS();
      console.log(nepalidate);
      loanAccount.nepali_month = checkOneDigitDate(nepalidate.month + 1);
      loanAccount.nepali_year = nepalidate.year;
      loanAccount.nepali_date = checkOneDigitDate(nepalidate.date);
    }
    console.log(loanAccount);
    // eslint-disable-next-line
  }, [loanAccount]);

  const LoanSchema = Yup.object().shape({
    type: Yup.string().required('Type is required'),
    loan_amount: Yup.number().required('Loan amount is required'),
    interest: Yup.number().required('Intrest is required'),
    charge: Yup.number().required('Charge is required'),
    nepali_year: Yup.number().required('Year  is required'), // these constraints take precedence
    nepali_month: Yup.number().required('Month  is required'), // these constraints take precedence
    nepali_date: Yup.number().max(32).min(1).required('Date  is required'), // these constraints take precedence
  });

  const formik = useFormik({
    initialValues: {
      type: loanAccount?.type || '',
      interest: loanAccount?.interest || '',
      charge: loanAccount?.charge || '',
      maturity_date: loanAccount?.maturity_date || '',
      nepali_year: loanAccount?.nepali_year || nepaliDate.getYear(),
      nepali_month:
        loanAccount?.nepali_month ||
        checkOneDigitDate(nepaliDate.getMonth() + 1),
      nepali_date: loanAccount?.nepali_month || nepaliDate.getDate(),
      loan_amount: loanAccount?.loan_amount || '',
    },
    validationSchema: LoanSchema,
    onSubmit: (data) => {
      let englishDate = new NepaliDate(
        `${data.nepali_year}-${data.nepali_month}-${checkOneDigitDate(
          data.nepali_date
        )}`
      ).getAD();
      data.loan_date = getFormatedDateFromDateObject(englishDate);
      console.log(data);
      if (loanAccount) {
        dispatch(editClientLoan(data, loanAccount.id));
      } else {
        data.maturity_date = addMonthToDate(englishDate);
        data.remaning_amount = data.loan_amount;
        dispatch(addClientLoan(data, client.id));
      }
      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            type='text'
            label='Loan Amount'
            {...getFieldProps('loan_amount')}
            error={Boolean(touched.loan_amount && errors.loan_amount)}
            helperText={touched.loan_amount && errors.loan_amount}
          />
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Year'
              {...getFieldProps(`nepali_year`)}
              error={Boolean(touched.nepali_year && errors.nepali_year)}
              helperText={touched.nepali_year && errors.nepali_year}
            >
              {yearList.map((year) => (
                <MenuItem key={year} value={year}>
                  {year}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              fullWidth
              select
              label='Month'
              {...getFieldProps(`nepali_month`)}
              error={Boolean(touched.nepali_month && errors.nepali_month)}
              helperText={touched.nepali_month && errors.nepali_month}
            >
              {monthList.map((month) => (
                <MenuItem key={month.value} value={month.value}>
                  {month.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              fullWidth
              type='number'
              label='Date'
              {...getFieldProps(`nepali_date`)}
              error={Boolean(touched.nepali_date && errors.nepali_date)}
              helperText={touched.nepali_date && errors.nepali_date}
            />
          </Stack>

          <TextField
            fullWidth
            select
            label='Repayment Type'
            {...getFieldProps('type')}
            error={Boolean(touched.type && errors.type)}
            helperText={touched.type && errors.type}
          >
            <MenuItem key={'DAILY'} value={'DAILY'}>
              DAILY
            </MenuItem>
            <MenuItem key={'WEEKLY'} value={'WEEKLY'}>
              WEEKLY
            </MenuItem>
            <MenuItem key={'MONTHLY'} value={'MONTHLY'}>
              MONTHLY
            </MenuItem>
            <MenuItem key={'NOT FIXED'} value={'NOT FIXED'}>
              NOT FIXED
            </MenuItem>
          </TextField>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='number'
              label='Intrest'
              {...getFieldProps('interest')}
              error={Boolean(touched.interest && errors.interest)}
              helperText={touched.interest && errors.interest}
            />
            <TextField
              fullWidth
              type='number'
              label='Charge'
              {...getFieldProps('charge')}
              error={Boolean(touched.charge && errors.charge)}
              helperText={touched.charge && errors.charge}
            />
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {loanAccount ? 'Edit Loan' : 'Add Loan'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
