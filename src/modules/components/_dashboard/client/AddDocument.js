import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { API_URL, BACKEND_URL } from '../../../../config';
import { PostRequest } from '../../../../utils/axios';
import { setToasterState } from '../../Toast/store/ToasterAction';
// ----------------------------------------------------------------------

export default function AddDocument({ handleClose, client }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const [document, setDocument] = useState('');
  useEffect(() => {
    // eslint-disable-next-line
  }, []);

  const TransactionSchema = Yup.object().shape({});

  const formik = useFormik({
    initialValues: {
      document: '',
      client_id: client?.id,
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      console.log(data);
      let formData = new FormData();
      formData.append('client_id', client?.id);
      formData.append('document', document);
      uploadDocument(formData);
      handleClose();
    },
  });

  const uploadDocument = (payload) => {
    PostRequest(`${API_URL}/client-documents`, payload)
      .then((res) => {
        console.log(res);
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Document!',
            message: 'Successfully Added Client Dcoument!',
          })
        );
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to add client document!',
          })
        );
      });
  };

  const onChangeFile = (e) => {
    setDocument(e.target.files[0]);
  };

  const { errors, touched, isSubmitting, handleSubmit, setSubmitting } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack direction='row' spacing={2}>
          {client.documents.length > 0 &&
            client.documents.map((doc) => (
              <img src={`${BACKEND_URL}/storage/image/${doc.url}`} alt='yo' />
            ))}
        </Stack>
        <Stack direction='column' spacing={2}>
          <TextField
            type='file'
            label='Document'
            onChange={onChangeFile}
            error={Boolean(touched.remarks && errors.remarks)}
            helperText={touched.remarks && errors.remarks}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Document
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
