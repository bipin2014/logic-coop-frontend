import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem, Switch, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getAccounts } from '../../../dashboard/store/account/AccountActions';
import { getAccountTypes } from '../../../dashboard/store/accounttype/AccountTypeActions';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import { updateClient } from '../../../dashboard/store/client/ClientActions';
import { fDate } from '../../../../utils/formatTime';
import { removeEmptyKeyFromObject } from '../../../../config';

// ----------------------------------------------------------------------

export default function EditClientDetails({ client }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);

  const accounts = useSelector((state) => state.data.accounts);
  const accountsTypes = useSelector((state) => state.data.account_types);
  const employees = useSelector((state) => state.data.employees);
  const [isminor, setIsminor] = useState(false);

  useEffect(() => {
    console.log('clinet', client);
    dispatch(getAccounts());
    dispatch(getAccountTypes());
    dispatch(getEmployees());
    // eslint-disable-next-line
  }, []);

  const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Email must be a valid email address'),
    name: Yup.string().required('Name is required'),
    account_number: Yup.string().required('Account Number is required'),
    account_id: Yup.number().required('Account is required'),
    account_type_id: Yup.number().required('AccountType is required'),
    employee_id: Yup.number().required('Employee  is required'),
    dob: Yup.date(),
    interest: Yup.number(),
  });

  const formik = useFormik({
    initialValues: {
      name: client.name ? client.name : '',
      account_number: client.account_number ? client.account_number : '',
      father_name: client.father_name ? client.father_name : '',
      grandfather_name: client.grandfather_name ? client.grandfather_name : '',
      account_id: client?.account?.id || '',
      account_type_id: client?.account_type?.id || '',
      address: client?.address || '',
      employee_id: '',
      email: client.email || '',
      phone: client?.phone,
      dob: (client?.dob && fDate(client?.dob)) || '',
      interest: client?.interest || '',
      calculate_interest: client?.calculate_interest,
      gender: client?.gender,
      minor: {
        name: '',
        relation: '',
      },
    },
    validationSchema: LoginSchema,
    onSubmit: (data) => {
      console.log(data);
      const newdata = removeEmptyKeyFromObject(data);
      console.log(newdata);
      dispatch(updateClient(newdata, client.id));
      // navigate('/dashboard', { replace: true });
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              type='text'
              label='Account Number'
              {...getFieldProps('account_number')}
              error={Boolean(touched.account_number && errors.account_number)}
              helperText={touched.account_number && errors.account_number}
            />
            <TextField
              fullWidth
              autoComplete='name'
              type='text'
              label='Full Name'
              {...getFieldProps('name')}
              error={Boolean(touched.name && errors.name)}
              helperText={touched.name && errors.name}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              autoComplete='username'
              type='email'
              label='Email address'
              {...getFieldProps('email')}
              error={Boolean(touched.email && errors.email)}
              helperText={touched.email && errors.email}
            />
            <TextField
              fullWidth
              autoComplete='phone'
              type='string'
              label='Phone Number'
              {...getFieldProps('phone')}
              error={Boolean(touched.phone && errors.phone)}
              helperText={touched.phone && errors.phone}
            />
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='text'
              label='Father Name'
              {...getFieldProps('father_name')}
              error={Boolean(touched.father_name && errors.father_name)}
              helperText={touched.father_name && errors.father_name}
            />

            <TextField
              fullWidth
              type='text'
              label='GrandFather Name'
              {...getFieldProps('grandfather_name')}
              error={Boolean(
                touched.grandfather_name && errors.grandfather_name
              )}
              helperText={touched.grandfather_name && errors.grandfather_name}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Gender'
              {...getFieldProps('gender')}
              error={Boolean(touched.gender && errors.gender)}
              helperText={touched.gender && errors.gender}
            >
              <MenuItem key={'M'} value={'M'}>
                Male
              </MenuItem>
              <MenuItem key={'F'} value={'F'}>
                Female
              </MenuItem>
            </TextField>

            <TextField
              fullWidth
              select
              label='Account'
              {...getFieldProps('account_id')}
              error={Boolean(touched.account_id && errors.account_id)}
              helperText={touched.account_id && errors.account_id}
            >
              {accounts.map((account) => (
                <MenuItem key={account.id} value={account.id}>
                  {account.title}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              fullWidth
              select
              label='AccountType'
              {...getFieldProps('account_type_id')}
              error={Boolean(touched.account_type_id && errors.account_type_id)}
              helperText={touched.account_type_id && errors.account_type_id}
            >
              {accountsTypes.map((accountType) => (
                <MenuItem key={accountType.id} value={accountType.id}>
                  {accountType.title}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Employee'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employees.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              fullWidth
              type='text'
              label='Full Address'
              {...getFieldProps('address')}
              error={Boolean(touched.address && errors.address)}
              helperText={touched.address && errors.address}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='date'
              label='DOB'
              {...getFieldProps('dob')}
              error={Boolean(touched.dob && errors.dob)}
              helperText={touched.dob && errors.dob}
            />
            <TextField
              fullWidth
              type='text'
              label='Interest'
              {...getFieldProps('interest')}
              error={Boolean(touched.interest && errors.interest)}
              helperText={touched.interest && errors.interest}
            />
            <TextField
              fullWidth
              select
              label='Calculate Interest'
              {...getFieldProps('calculate_interest')}
              error={Boolean(
                touched.calculate_interest && errors.calculate_interest
              )}
              helperText={
                touched.calculate_interest && errors.calculate_interest
              }
            >
              <MenuItem key='0' value='0'>
                DISABLE
              </MenuItem>
              <MenuItem key='1' value='1'>
                ENABLE
              </MenuItem>
            </TextField>
          </Stack>

          <Stack direction='row' spacing={2}>
            <Typography variant='body'>Minor?</Typography>
            <Switch value={isminor} onChange={(e) => setIsminor(!isminor)} />
          </Stack>
          {isminor && (
            <Stack direction='row' spacing={2}>
              <TextField
                fullWidth
                type='string'
                label='Minor Name'
                {...getFieldProps('minor.name')}
                error={Boolean(touched.email && errors.email)}
                helperText={touched.email && errors.email}
              />
              <TextField
                fullWidth
                type='string'
                label='Minor Relation'
                {...getFieldProps('minor.relation')}
                error={Boolean(touched.phone && errors.phone)}
                helperText={touched.phone && errors.phone}
              />
            </Stack>
          )}

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Update Client
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
