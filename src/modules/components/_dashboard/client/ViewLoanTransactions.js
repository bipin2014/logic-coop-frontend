import { filter } from 'lodash';
import { useEffect, useState } from 'react';
// material
import {
  Card,
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import { useDispatch, useSelector } from 'react-redux';
import { DeleteRequest, GetRequest } from '../../../../utils/axios';
import { API_URL, checkOneDigitDate } from '../../../../config';
import { setToasterState } from '../../Toast/store/ToasterAction';
import { setSuccess } from '../../../dashboard/store/client/ClientActions';
import { UserListHead, UserMoreMenu } from '../user';
import NepaliDate from 'nepali-date-converter';
import Label from '../../Label';
import AddTransactionForm from '../transaction/AddTransactionForm';
import MyDialog from '../../MyDialog';
import MyAlert from '../../MyAlert';
import SearchNotFound from '../../SearchNotFound';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'date', label: 'Date', alignRight: false },
  { id: 'amount', label: 'Amount', alignRight: false },
  { id: 'type', label: 'Type', alignRight: false },
  { id: 'balance', label: 'Balance', alignRight: false },
  { id: 'remarks', label: 'Remarks', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.title.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function ViewLoanTransactions({ loan }) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('title');
  const [filterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [transactions, setTransactions] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [activeData, setActiveData] = useState('');
  const dispatch = useDispatch();
  const success = useSelector((state) => state.data.success);

  const getClientTransactions = () => {
    GetRequest(`${API_URL}/transactions`, { loan_account_id: loan.id })
      .then((res) => {
        console.log(res.data);
        setTransactions(res.data.data.reverse());
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to fetch transactions!',
          })
        );
      });
  };

  const deleteClientTransactions = (id, client) => {
    DeleteRequest(`${API_URL}/clients/${client.id}/transactions/${id}`)
      .then((res) => {
        setOpen(false);
        getClientTransactions();
        dispatch(setSuccess(true));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Transaction Delete!',
            message: 'Successfully Deleted Transaction!',
          })
        );
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to delete transactions!',
          })
        );
      });
  };

  useEffect(() => {
    getClientTransactions();
    // eslint-disable-next-line
  }, []);
  useEffect(() => {
    if (success) {
      getClientTransactions();
    }
    // eslint-disable-next-line
  }, [success]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = transactions.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - transactions.length) : 0;

  const filteredUsers = applySortFilter(
    transactions,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const editOnClick = (transaction) => {
    setActiveData(transaction);
    setOpen(true);
    setDialog('editTransaction');
  };
  const openDeleteAlert = () => {
    setOpen(true);
    setDialog('deleteTransaction');
  };

  const deleteOnClick = (id) => {
    setActiveData(id);
    openDeleteAlert();
  };

  return (
    <Card>
      <TableContainer sx={{ minWidth: 800 }}>
        <Table>
          <UserListHead
            order={order}
            orderBy={orderBy}
            headLabel={TABLE_HEAD}
            rowCount={transactions.length}
            numSelected={selected.length}
            onRequestSort={handleRequestSort}
            onSelectAllClick={handleSelectAllClick}
          />
          <TableBody>
            {filteredUsers
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                const {
                  id,
                  remarks,
                  transaction_date,
                  amount,
                  balance,
                  transaction_type,
                } = row;

                let nepaliTransactionDate = new NepaliDate(
                  new Date(transaction_date)
                ).getBS();
                let td = new NepaliDate(
                  `${nepaliTransactionDate.year}-${checkOneDigitDate(
                    nepaliTransactionDate.month + 1
                  )}-${checkOneDigitDate(nepaliTransactionDate.date)}`
                );
                const isItemSelected = selected.indexOf(remarks) !== -1;

                return (
                  <TableRow
                    hover
                    key={id}
                    tabIndex={-1}
                    role='checkbox'
                    selected={isItemSelected}
                    aria-checked={isItemSelected}
                  >
                    <TableCell padding='checkbox'>
                      <Checkbox
                        checked={isItemSelected}
                        onChange={(event) =>
                          handleClick(event, transaction_date)
                        }
                      />
                    </TableCell>

                    <TableCell component='th' scope='row'>
                      {td.format('YYYY-MM-DD')}
                    </TableCell>
                    <TableCell component='th' scope='row' padding='none'>
                      Rs. {amount}
                    </TableCell>
                    <TableCell component='th' scope='row' padding='none'>
                      <Label
                        variant='ghost'
                        color={
                          (transaction_type?.type === 'DEBIT' && 'error') ||
                          'success'
                        }
                      >
                        {transaction_type?.name}
                      </Label>
                    </TableCell>

                    <TableCell align='left'>Rs. {balance}</TableCell>
                    <TableCell align='left'>{remarks}</TableCell>

                    <TableCell align='right'>
                      <UserMoreMenu
                        editOnClick={() => editOnClick(row)}
                        deleteOnClick={() => deleteOnClick(id)}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          {isUserNotFound && (
            <TableBody>
              <TableRow>
                <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                  <SearchNotFound searchQuery={filterName} />
                </TableCell>
              </TableRow>
            </TableBody>
          )}
        </Table>
      </TableContainer>

      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component='div'
        count={transactions.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />

      {open && dialog === 'deleteTransaction' && (
        <MyAlert
          open={open}
          handleClose={() => setOpen(false)}
          handleSubmit={() => deleteClientTransactions(activeData)}
          title='Delete Transaction'
        />
      )}

      {open && dialog === 'editTransaction' && (
        <MyDialog
          open={open}
          title='Edit transaction'
          handleClose={() => setOpen(false)}
        >
          <AddTransactionForm
            client={activeData.client}
            handleClose={() => setOpen(false)}
            transaction={activeData}
          />
        </MyDialog>
      )}
    </Card>
  );
}
