import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem, Autocomplete, Box } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { API_URL } from '../../../../config';
import { GetRequest } from '../../../../utils/axios';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import { setToasterState } from '../../Toast/store/ToasterAction';
import {
  addMemberAccount,
  updateMemberAccount,
} from '../../../dashboard/store/memberaccount/MemberAccountActions';

// ----------------------------------------------------------------------

export default function AddMemberAccountForm({
  handleClose,
  memberAccount,
  accountType,
  memberAccountList,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const employeeList = useSelector((state) => state.data.employees);
  const memberList = useSelector((state) => state.data.members);
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const [filteredMemberAccountList, setFilteredMemberAccountList] = useState(
    []
  );

  useEffect(() => {
    if (memberAccountList) {
      console.log(memberAccountList);
      const filterMemberAccounts = memberAccountList.filter(
        (memberAccount) => memberAccount?.account_type?.can_withdraw
      );
      console.log(filterMemberAccounts);
      setFilteredMemberAccountList(filterMemberAccounts);
    }
  }, [memberAccountList]);

  const MemberAccountSchema = Yup.object().shape({
    member_id: Yup.string().required('Name is required'),
    account_type_id: Yup.string().required('Account Type is required'),
    account_number: Yup.string().required('Account Number is required'),
    employee_id: Yup.string().required('Employee Id is required'),
    interest_type: Yup.string().required('Interest Type is required'),
    interest: Yup.string().required('Interest is required'),
  });

  const formik = useFormik({
    initialValues: {
      member_id: memberAccount?.member?.id || '',
      account_type_id: memberAccount?.account_type?.id || '',
      employee_id: memberAccount?.employee_id || '',
      account_number: memberAccount?.account_number || '',
      interest_type: memberAccount?.interest_type || '',
      interest: memberAccount?.interest || '',
      interest_collection: memberAccount?.interest_collection || '',
    },
    validationSchema: MemberAccountSchema,
    onSubmit: (data) => {
      // console.log(data);
      data.balance = 0;
      if (memberAccount) {
        dispatch(updateMemberAccount(data, memberAccount.id));
      } else {
        dispatch(addMemberAccount(data));
      }

      setSubmit(true);
      // handleClose();
      // navigate('/dashboard', { replace: true });
    },
  });

  useEffect(() => {
    dispatch(getEmployees());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const generateAccountNumber = (count) => {
    let accountNumber = accountType.prefix;
    count++;
    for (
      let index = count.toString().length;
      index < accountType.account_number_length;
      index++
    ) {
      accountNumber += '0';
    }
    return accountNumber + count;
  };

  useEffect(() => {
    const getTotalAccount = () => {
      GetRequest(`${API_URL}/member-account/count`, {
        account_type_id: accountType.id,
      })
        .then((res) => {
          setValues({
            ...values,
            account_number: generateAccountNumber(res.data.data),
            account_type_id: accountType.id,
            interest: accountType.intrest,
            interest_type: 'default',
          });
        })
        .catch((err) => {
          dispatch(
            setToasterState({
              open: true,
              title: 'error',
              name: 'Sorry!',
              message:
                errors?.response?.data?.message ||
                'Failed to fetch account count!',
            })
          );
        });
    };

    if (accountType && !memberAccount) {
      getTotalAccount();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountType]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
    setValues,
    values,
  } = formik;

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  useEffect(() => {
    if (success && submit) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [success, submit]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  const changeMember = (newValue) => {
    setValues({
      ...values,
      member_id: newValue.id,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              disabled
              type='text'
              InputLabelProps={{ shrink: true }}
              label='Deposit Account Type'
              value={accountType.title}
            />
            <TextField
              disabled
              fullWidth
              type='text'
              label='Account Number'
              {...getFieldProps('account_number')}
              error={Boolean(touched.account_number && errors.account_number)}
              helperText={touched.account_number && errors.account_number}
            />
          </Stack>

          <Stack direction='row' spacing={2}>
            {memberAccount ? (
              <TextField
                disabled
                fullWidth
                type='text'
                label='Member'
                value={memberAccount.member.name}
              />
            ) : (
              <Autocomplete
                fullWidth
                options={memberList}
                onChange={(e, newValue) => changeMember(newValue)}
                autoHighlight
                getOptionLabel={(option) => `${option.name}-${option.id}`}
                renderOption={(props, option) => (
                  <Box component='li' sx={{ width: 250 }} {...props}>
                    {option.name} - {option.id}
                  </Box>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label='Choose a member'
                    inputProps={{
                      ...params.inputProps,
                    }}
                  />
                )}
              />
            )}

            <TextField
              fullWidth
              select
              label='Money Collector'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employeeList.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='number'
              label='Interest'
              {...getFieldProps('interest')}
              error={Boolean(touched.interest && errors.interest)}
              helperText={touched.interest && errors.interest}
            />
            <TextField
              fullWidth
              select
              label='Interest Type'
              {...getFieldProps('interest_type')}
              error={Boolean(touched.interest_type && errors.interest_type)}
              helperText={touched.interest_type && errors.interest_type}
            >
              <MenuItem key={'default'} value={'default'}>
                Default
              </MenuItem>
              <MenuItem key={'PERCENTAGE'} value={'PERCENTAGE'}>
                Percentage
              </MenuItem>
              <MenuItem key={'AMOUNT'} value={'AMOUNT'}>
                Amount
              </MenuItem>
            </TextField>
          </Stack>

          {!accountType.can_withdraw && (
            <Autocomplete
              fullWidth
              options={filteredMemberAccountList}
              autoHighlight
              getOptionLabel={(option) =>
                `${option?.account_number}-${option?.member?.name}`
              }
              renderOption={(props, option) => (
                <Box component='li' sm={{ width: 250 }} {...props}>
                  {option?.account_number} - {option?.member?.name}
                </Box>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Choose a member account to deposit'
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />
          )}

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {memberAccount ? 'Update Member Account' : 'Add Member Account'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
