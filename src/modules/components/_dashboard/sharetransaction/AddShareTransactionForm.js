import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../../config';
import { fDate } from '../../../../utils/formatTime';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';
import 'nepali-datepicker-reactjs/dist/index.css';
import {
  addShareTransaction,
  updateShareTransaction,
} from '../../../dashboard/store/sharetransaction/ShareTransactionActions';

// ----------------------------------------------------------------------

export default function AddShareTransactionForm({
  handleClose,
  transaction,
  memberShare,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [transactionTypeList, setTransactionTypeList] = useState([]);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const [nepaliDate, setNepaliDate] = useState('');
  // const [nepaliDate, setNepaliDate] = useState('')
  const date = new Date();

  useEffect(() => {
    dispatch(getTransactionTypes());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log(transactionTypes);
    const filteredData = transactionTypes.filter(
      (transactionType) => transactionType.account_type === 'share'
    );
    setTransactionTypeList(filteredData);
  }, [transactionTypes]);

  useEffect(() => {
    transaction &&
      setNepaliDate(new NepaliDate(new Date(transaction.transaction_date)));
  }, [transaction]);

  const TransactionSchema = Yup.object().shape({
    amount: Yup.number().required('Amount is required'),
    transaction_type_id: Yup.number().required('TransactionType is required'),
    transaction_date: Yup.date().required('Transaction Date is required'),
  });

  const formik = useFormik({
    initialValues: {
      amount: transaction?.amount || '',
      transaction_type_id: transaction?.transaction_type_id || '',
      remarks: transaction?.remarks || '',
      transaction_date:
        (transaction?.transaction_date &&
          fDate(transaction.transaction_date)) ||
        `${date.getFullYear()}-${checkOneDigitDate(
          date.getMonth() + 1
        )}-${checkOneDigitDate(date.getDate())}`,
      member_share_id: transaction?.member_share_id || memberShare.id,
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      console.log(data);
      if (transaction) {
        dispatch(updateShareTransaction(data, transaction.id));
      } else {
        dispatch(addShareTransaction(data));
      }

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    values,
    setValues,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  useEffect(() => {
    if (values.transaction_date) {
      let date = new NepaliDate(new Date(values.transaction_date)).getBS();
      console.log(date);
      setNepaliDate(
        `${date.year}-${checkOneDigitDate(date.month + 1)}-${checkOneDigitDate(
          date.date
        )}`
      );
    }
  }, [values.transaction_date]);

  const changeNepaliDate = (value) => {
    console.log(value);
    setNepaliDate(value);
    let date = new NepaliDate(value);
    // setNepaliDate(date);
    let englishDate = date.getAD();
    console.log(
      `${englishDate.year}/${checkOneDigitDate(
        englishDate.month + 1
      )}/${checkOneDigitDate(englishDate.date)}`
    );
    setValues({
      ...values,
      transaction_date: `${englishDate.year}-${checkOneDigitDate(
        englishDate.month + 1
      )}-${checkOneDigitDate(englishDate.date)}`,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack spacing={2} direction={'row'}>
            <TextField
              type='date'
              label='Date'
              {...getFieldProps('transaction_date')}
              error={Boolean(
                touched.transaction_date && errors.transaction_date
              )}
              helperText={touched.transaction_date && errors.transaction_date}
            />
            <NepaliDatePicker
              onChange={changeNepaliDate}
              value={nepaliDate}
              options={{ calenderLocale: 'en', valueLocale: 'en' }}
            />
          </Stack>

          <TextField
            fullWidth
            type='string'
            label='Member Name'
            disabled={true}
            value={memberShare?.member?.name}
          />
          <TextField
            fullWidth
            type='string'
            label='Amount'
            {...getFieldProps('amount')}
            error={Boolean(touched.amount && errors.amount)}
            helperText={touched.amount && errors.amount}
          />
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Transaction Type'
              {...getFieldProps('transaction_type_id')}
              error={Boolean(
                touched.transaction_type_id && errors.transaction_type_id
              )}
              helperText={
                touched.transaction_type_id && errors.transaction_type_id
              }
            >
              {transactionTypeList.map((transactionType) => (
                <MenuItem key={transactionType.id} value={transactionType.id}>
                  {transactionType.name}
                </MenuItem>
              ))}
            </TextField>
          </Stack>

          <TextField
            type='text'
            label='Remarks'
            {...getFieldProps('remarks')}
            error={Boolean(touched.remarks && errors.remarks)}
            helperText={touched.remarks && errors.remarks}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {transaction ? 'Edit Transaction' : 'Add Transaction'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
