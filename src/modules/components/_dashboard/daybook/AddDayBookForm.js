import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { setSuccess } from '../../../dashboard/store/client/ClientActions';
import { addDayBook } from '../../../dashboard/store/daybook/DaybookActions';
import { checkOneDigitDate } from '../../../../config';
// ----------------------------------------------------------------------

export default function AddDayBookForm({ handleClose }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  let date = new Date();

  useEffect(() => {
    dispatch(setSuccess(false));
    // eslint-disable-next-line
  }, []);

  const TransactionSchema = Yup.object().shape({
    opening_balance: Yup.number().required('Opening Balance is required'),
    total_income: Yup.number().required('Total Income is required'),
    total_expenses: Yup.number().required('Total Expenses  is required'),
    closing_balance: Yup.number().required('Closing balance  is required'),
    date: Yup.date().required(' Date is required'),
  });

  const formik = useFormik({
    initialValues: {
      opening_balance: '',
      total_income: '',
      total_expenses: '',
      closing_balance: '',
      date: `${date.getFullYear()}-${checkOneDigitDate(
        date.getMonth() + 1
      )}-${checkOneDigitDate(date.getDate())}`,
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      setSubmit(true);
      dispatch(addDayBook(data));
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            type='text'
            label='Opening Balance'
            {...getFieldProps('opening_balance')}
            error={Boolean(touched.opening_balance && errors.opening_balance)}
            helperText={touched.opening_balance && errors.opening_balance}
          />

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='string'
              label='Total Income'
              {...getFieldProps('total_income')}
              error={Boolean(touched.total_income && errors.total_income)}
              helperText={touched.total_income && errors.total_income}
            />

            <TextField
              fullWidth
              type='string'
              label='Total Expenses'
              {...getFieldProps('total_expenses')}
              error={Boolean(touched.total_expenses && errors.total_expenses)}
              helperText={touched.total_expenses && errors.total_expenses}
            />
          </Stack>
          <TextField
            fullWidth
            type='string'
            label='Closing Balance'
            {...getFieldProps('closing_balance')}
            error={Boolean(touched.closing_balance && errors.closing_balance)}
            helperText={touched.closing_balance && errors.closing_balance}
          />
          <TextField
            fullWidth
            type='date'
            label='Date'
            {...getFieldProps('date')}
            error={Boolean(touched.date && errors.date)}
            helperText={touched.date && errors.date}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add DayBook
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
