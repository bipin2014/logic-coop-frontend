import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem, Autocomplete, Box } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { API_URL, checkOneDigitDate } from '../../../../config';
import { GetRequest } from '../../../../utils/axios';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import { getMembers } from '../../../dashboard/store/member/MemberActions';
import { setToasterState } from '../../Toast/store/ToasterAction';
import { addLoanAccount, updateLoanAccount } from '../../../dashboard/store/loanaccount/LoanAccountActions';

// ----------------------------------------------------------------------

export default function AddLoanAccountForm({
  handleClose,
  loanAccount,
  loanType,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const employeeList = useSelector((state) => state.data.employees);
  const memberList = useSelector((state) => state.data.members);
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);
  const startDate = new Date();

  const MemberAccountSchema = Yup.object().shape({
    member_id: Yup.string().required('Name is required'),
    loan_type_id: Yup.string().required('Loan Type is required'),
    account_number: Yup.string().required('Account Number is required'),
    employee_id: Yup.number().required('Employee Id is required'),
    issue_amount: Yup.number().required('Issue amount is required'),
    interest_rate: Yup.number().required('Interest is required'),
    loan_start_date: Yup.string().required('Loan start Date is required'),
    loan_end_date: Yup.string().required('Loan End Date is required'),
    service_charge: Yup.number().required('Service Charge is required'),
    period: Yup.number().required('Period is required'),
    period_type: Yup.string().required('Period type is required'),
    installment_type: Yup.string().required('Installment type is required'),
  });

  const formik = useFormik({
    initialValues: {
      member_id: loanAccount?.member?.id || '',
      loan_type_id: loanAccount?.loan_type?.id || '',
      employee_id: loanAccount?.employee_id || '',
      account_number: loanAccount?.account_number || '',
      issue_amount: loanAccount?.issue_amount || '',
      interest_rate: loanAccount?.interest_rate || '',
      interest_collection: loanAccount?.interest_collection || '',
      loan_start_date:
        loanAccount?.loan_start_date ||
        `${startDate.getFullYear()}-${checkOneDigitDate(
          startDate.getMonth() + 1
        )}-${checkOneDigitDate(startDate.getDate())}`,
      loan_end_date: loanAccount?.loan_end_date || '',
      service_charge: loanAccount?.service_charge || '',
      period: loanAccount?.period || '',
      period_type: loanAccount?.period_type || '',
      installment_type: loanAccount?.installment_type || '',
    },
    validationSchema: MemberAccountSchema,
    onSubmit: (data) => {
      // console.log(data);
      data.balance = 0;
      if (loanAccount) {
        dispatch(updateLoanAccount(data, loanAccount.id));
      } else {
        dispatch(addLoanAccount(data));
      }

      setSubmit(true);
      // handleClose();
      // navigate('/dashboard', { replace: true });
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
    setValues,
    values,
  } = formik;

  useEffect(() => {
    if (values.loan_start_date) {
      if (values.period && values.period_type) {
        let endDate = new Date(values.loan_start_date);
        if (values.period_type === 'months') {
          endDate.setMonth(endDate.getMonth() + parseInt(values.period));
        } else if (values.period_type === 'days') {
          endDate.setDate(endDate.getDate() + parseInt(values.period));
        }
        setValues({
          ...values,
          loan_end_date: `${endDate.getFullYear()}-${checkOneDigitDate(
            endDate.getMonth() + 1
          )}-${checkOneDigitDate(endDate.getDate())}`,
        });
      }
    }
    // eslint-disable-next-line
  }, [values.loan_start_date, values.period_type, values.period]);

  useEffect(() => {
    if (values.issue_amount) {
      let service_charge = 0;
      if (loanType.service_charge_type === 'PERCENTAGE') {
        service_charge =
          parseInt(values.issue_amount) * (loanType.service_charge / 100);
      } else {
        service_charge = loanType.service_charge;
      }

      setValues({
        ...values,
        service_charge,
      });
    }
    // eslint-disable-next-line
  }, [values.issue_amount]);

  useEffect(() => {
    dispatch(getEmployees());
    dispatch(getMembers());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const generateAccountNumber = (count) => {
    let accountNumber = loanType.prefix;
    count++;
    for (let index = count.toString().length; index < 4; index++) {
      accountNumber += '0';
    }
    return accountNumber + count;
  };

  useEffect(() => {
    console.log(loanType);
    const getTotalAccount = () => {
      GetRequest(`${API_URL}/loan-accounts/count`, {
        loan_type_id: loanType.id,
      })
        .then((res) => {
          setValues({
            ...values,
            account_number: generateAccountNumber(res.data.data),
            loan_type_id: loanType.id,
            interest_rate: loanType.interest_rate,
            period: '3',
            period_type: 'months',
          });
        })
        .catch((err) => {
          dispatch(
            setToasterState({
              open: true,
              title: 'error',
              name: 'Sorry!',
              message:
                errors?.response?.data?.message ||
                'Failed to fetch account count!',
            })
          );
        });
    };

    if (loanType && !loanAccount) {
      getTotalAccount();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loanType]);

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  useEffect(() => {
    if (success && submit) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [success, submit]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  const changeMember = (newValue) => {
    setValues({
      ...values,
      member_id: newValue.id,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              disabled
              type='text'
              InputLabelProps={{ shrink: true }}
              label='Deposit Account Type'
              value={loanType.name}
            />
            <TextField
              disabled
              fullWidth
              type='text'
              label='Account Number'
              {...getFieldProps('account_number')}
              error={Boolean(touched.account_number && errors.account_number)}
              helperText={touched.account_number && errors.account_number}
            />
          </Stack>

          <Stack direction='row' spacing={2}>
            {loanAccount ? (
              <TextField
                disabled
                fullWidth
                type='text'
                label='Member'
                value={loanAccount.member.name}
              />
            ) : (
              <Autocomplete
                fullWidth
                options={memberList}
                onChange={(e, newValue) => changeMember(newValue)}
                autoHighlight
                getOptionLabel={(option) => `${option.name}-${option.id}`}
                renderOption={(props, option) => (
                  <Box component='li' sx={{ width: 250 }} {...props}>
                    {option.name} - {option.id}
                  </Box>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label='Choose a member'
                    inputProps={{
                      ...params.inputProps,
                    }}
                  />
                )}
              />
            )}

            <TextField
              fullWidth
              type='number'
              label='Loan Amount'
              {...getFieldProps('issue_amount')}
              error={Boolean(touched.issue_amount && errors.issue_amount)}
              helperText={touched.issue_amount && errors.issue_amount}
            />
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='date'
              label='Start Date'
              {...getFieldProps('loan_start_date')}
              error={Boolean(touched.loan_start_date && errors.loan_start_date)}
              helperText={touched.loan_start_date && errors.loan_start_date}
            />
            <TextField
              fullWidth
              type='date'
              label='End Date'
              {...getFieldProps('loan_end_date')}
              error={Boolean(touched.loan_end_date && errors.loan_end_date)}
              helperText={touched.loan_end_date && errors.loan_end_date}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='number'
              label='Interest Rate'
              {...getFieldProps('interest_rate')}
              error={Boolean(touched.interest_rate && errors.interest_rate)}
              helperText={touched.interest_rate && errors.interest_rate}
            />
            <TextField
              fullWidth
              type='number'
              label='Service Charge'
              {...getFieldProps('service_charge')}
              error={Boolean(touched.service_charge && errors.service_charge)}
              helperText={touched.service_charge && errors.service_charge}
            />
          </Stack>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              type='number'
              label='Period'
              {...getFieldProps('period')}
              error={Boolean(touched.period && errors.period)}
              helperText={touched.period && errors.period}
            />
            <TextField
              fullWidth
              select
              label='Period Type'
              {...getFieldProps('period_type')}
              error={Boolean(touched.period_type && errors.period_type)}
              helperText={touched.period_type && errors.period_type}
            >
              <MenuItem key='day' value='days'>
                Days
              </MenuItem>
              <MenuItem key='months' value='months'>
                Months
              </MenuItem>
            </TextField>
          </Stack>

          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Money Collector'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employeeList.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              fullWidth
              select
              label='Installment Type'
              {...getFieldProps('installment_type')}
              error={Boolean(
                touched.installment_type && errors.installment_type
              )}
              helperText={touched.installment_type && errors.installment_type}
            >
              <MenuItem key='daily' value='daily'>
                Daily
              </MenuItem>
              <MenuItem key='weekly' value='weekly'>
                Weekly
              </MenuItem>
              <MenuItem key='monthly' value='monthly'>
                Monthly
              </MenuItem>
            </TextField>
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {loanAccount ? 'Update Member Account' : 'Add Member Account'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
