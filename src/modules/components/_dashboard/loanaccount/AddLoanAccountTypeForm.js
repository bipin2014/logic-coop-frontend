import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  addLoanAccountType,
  updateLoanAccountType,
} from '../../../dashboard/store/loanaccounttype/LoanAccountTypeActions';

export default function AddLoanAccountTypeForm({
  handleClose,
  loanAccountType,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const AccountSchema = Yup.object().shape({
    name: Yup.string().required('Title is required'),
    interest_rate: Yup.number().required('Intrest is required'),
    calculation_type: Yup.string().required('Calculation Type is required'),
    calculation_period: Yup.string().required('Calculation Period is required'),
    service_charge: Yup.string().required('Service Charge is required'),
    service_charge_type: Yup.string().required(
      'Service Charge Type is required'
    ),
    prefix: Yup.string().required('Prefix is required'),
    collector_commission: Yup.number().required(
      'Collector Commission is required'
    ),
  });

  const formik = useFormik({
    initialValues: {
      name: loanAccountType?.name || '',
      interest_rate: loanAccountType?.interest_rate || 14.75,
      calculation_type: loanAccountType?.calculation_type || '',
      calculation_period: loanAccountType?.calculation_period || '',
      prefix: loanAccountType?.prefix || '',
      service_charge: loanAccountType?.service_charge || '',
      service_charge_type: loanAccountType?.service_charge_type || '',
      collector_commission: loanAccountType?.collector_commission || 0,
    },
    validationSchema: AccountSchema,
    onSubmit: (data) => {
      if (loanAccountType) {
        dispatch(updateLoanAccountType(data, loanAccountType.id));
      } else {
        dispatch(addLoanAccountType(data));
      }
      setSubmit(true);
      // handleClose();
    },
  });

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submit, success]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='text'
              label='Name'
              {...getFieldProps('name')}
              error={Boolean(touched.name && errors.name)}
              helperText={touched.name && errors.name}
            />

            <TextField
              fullWidth
              type='number'
              label='Interest rate'
              {...getFieldProps('interest_rate')}
              error={Boolean(touched.interest_rate && errors.interest_rate)}
              helperText={touched.interest_rate && errors.interest_rate}
            />
          </Stack>

          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              select
              label='Interest Calculation Type'
              {...getFieldProps('calculation_type')}
              error={Boolean(
                touched.calculation_type && errors.calculation_type
              )}
              helperText={touched.calculation_type && errors.calculation_type}
            >
              <MenuItem key={'flat'} value={1}>
                Flat
              </MenuItem>
              <MenuItem key={'diminishing'} value={0}>
                Diminishing
              </MenuItem>
            </TextField>
            <TextField
              fullWidth
              select
              label='Interest Calculation Period'
              {...getFieldProps('calculation_period')}
              error={Boolean(
                touched.calculation_period && errors.calculation_period
              )}
              helperText={
                touched.calculation_period && errors.calculation_period
              }
            >
              <MenuItem key={'monthly'} value={'monthly'}>
                Monthly
              </MenuItem>
              <MenuItem key={'quaterly'} value={'quaterly'}>
                Quaterly
              </MenuItem>
              <MenuItem key={'yearly'} value={'yearly'}>
                Yearly
              </MenuItem>
            </TextField>
          </Stack>
          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='text'
              label='Prefix'
              {...getFieldProps('prefix')}
              error={Boolean(touched.prefix && errors.prefix)}
              helperText={touched.prefix && errors.prefix}
            />

            <TextField
              fullWidth
              type='number'
              label='Collector Commission'
              {...getFieldProps('collector_commission')}
              error={Boolean(
                touched.collector_commission && errors.collector_commission
              )}
              helperText={
                touched.collector_commission && errors.collector_commission
              }
            />
          </Stack>

          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='number'
              label='Service Charge'
              {...getFieldProps('service_charge')}
              error={Boolean(touched.service_charge && errors.service_charge)}
              helperText={touched.service_charge && errors.service_charge}
            />

            <TextField
              fullWidth
              select
              label='Service Charge Type'
              {...getFieldProps('service_charge_type')}
              error={Boolean(
                touched.service_charge_type && errors.service_charge_type
              )}
              helperText={
                touched.service_charge_type && errors.service_charge_type
              }
            >
              <MenuItem key={'PERCENTAGE'} value={'PERCENTAGE'}>
                Percentage
              </MenuItem>
              <MenuItem key={'RUPEES'} value={'RUPEES'}>
                Rupees
              </MenuItem>
            </TextField>
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {loanAccountType ? 'Update Loan Account Type' : 'Add Loan Account Type'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
