import { useEffect, useState } from 'react';
// material
import { Box, Button, Stack, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { closeLoanAccount } from '../../../dashboard/store/loanaccount/LoanAccountActions';

// ----------------------------------------------------------------------

export default function CloseLoanAccount({ handleClose, loanAccount }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const closeAccountClick = () => {
    const payload = {
      status: !loanAccount.status,
    };
    console.log(payload);
    dispatch(closeLoanAccount(loanAccount.id, payload));
    setSubmit(true);
  };

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  return (
    <Box>
      <Stack direction={'column'} spacing={1}>
        <Typography>Are you sure want to close this account?</Typography>
        <Typography>
          Loan Account Number: {loanAccount.account_number}
        </Typography>
        <Typography>
          Loan Issue Amount: Rs.{loanAccount.issue_amount}
        </Typography>
        <Typography>Total Interest: Rs.{loanAccount.total_interest}</Typography>
        <Typography>Total Paid Amount: Rs.{loanAccount.paid_amount}</Typography>
        {loanAccount.status ? (
          loanAccount.paid_amount >= loanAccount.issue_amount ? (
            <LoadingButton
              fullWidth
              size='large'
              type='submit'
              variant='contained'
              onClick={closeAccountClick}
              loading={loading}
            >
              Close Loan Account
            </LoadingButton>
          ) : (
            <Button variant='outlined'>Cannot Close this account </Button>
          )
        ) : (
          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            onClick={closeAccountClick}
            loading={loading}
          >
            Open Loan Account
          </LoadingButton>
        )}
      </Stack>
    </Box>
  );
}
