import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider, FieldArray, getIn } from 'formik';
// material
import {
  Stack,
  TextField,
  MenuItem,
  Button,
  Autocomplete,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getEmployees } from '../../../dashboard/store/employee/EmployeeActions';
import {
  addMultipleLoan,
  setSuccess,
} from '../../../dashboard/store/client/ClientActions';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import NepaliDate from 'nepali-date-converter';
import {
  checkOneDigitDate,
  getFormatedDateFromDateObject,
} from '../../../../config';
import { Box } from '@mui/system';
import { monthList, yearList } from '../../../_mocks_/datelist';

// ----------------------------------------------------------------------

export default function MultipleLoanForm({ handleClose, clients }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const employees = useSelector((state) => state.data.employees);
  const [nepaliDate] = useState(new NepaliDate());
  // let nepaliDate = new NepaliDate();

  useEffect(() => {
    dispatch(setSuccess(false));
    dispatch(getTransactionTypes());
    dispatch(getEmployees());

    console.log(nepaliDate);
    // eslint-disable-next-line
  }, []);

  const TransactionSchema = Yup.object().shape({
    employee_id: Yup.number().required('Employee  is required'),
    loans: Yup.array().of(
      Yup.object().shape({
        interest: Yup.number().required('Interest is Required'), // these constraints take precedence
        charge: Yup.number().required('Charge is Required'), // these constraints take precedence
        loan_amount: Yup.number().required('Loan Amount  is required'), // these constraints take precedence
        nepali_year: Yup.number().required('Year  is required'), // these constraints take precedence
        nepali_month: Yup.number().required('Month  is required'), // these constraints take precedence
        nepali_date: Yup.number().max(32).min(1).required('Date  is required'), // these constraints take precedence
      })
    ),
  });

  const formik = useFormik({
    initialValues: {
      employee_id: '',
      loans: [
        {
          type: 'NOT FIXED',
          interest: '',
          charge: '',
          maturity_date: '',
          nepali_year: nepaliDate.getYear(),
          nepali_month: checkOneDigitDate(nepaliDate.getMonth() + 1),
          nepali_date: nepaliDate.getDate(),
          loan_amount: '',
        },
      ],
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      data.loans.forEach((loan, index) => {
        data.loans[index].employee_id = data.employee_id;
        data.loans[index].remaning_amount = loan.loan_amount;
        let englishDate = new NepaliDate(
          `${loan.nepali_year}-${loan.nepali_month}-${checkOneDigitDate(
            loan.nepali_date
          )}`
        ).getAD();
        data.loans[index].loan_date =
          getFormatedDateFromDateObject(englishDate);
      });

      console.log(data);

      dispatch(addMultipleLoan(data));

      setSubmit(true);
      // handleClose();
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    values,
    setValues,
    setSubmitting,
  } = formik;

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  const changeClient = (value, index) => {
    values.loans[index] = {
      ...values.loans[index],
      client_id: value?.id,
    };
    setValues({
      ...values,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Employee'
              {...getFieldProps('employee_id')}
              error={Boolean(touched.employee_id && errors.employee_id)}
              helperText={touched.employee_id && errors.employee_id}
            >
              {employees.map((employee) => (
                <MenuItem key={employee.id} value={employee.id}>
                  {employee.name}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <FieldArray
            name='loans'
            render={(arrayHelpers) =>
              values.loans &&
              values.loans.length > 0 &&
              values.loans.map((transacation, index) => (
                <Stack direction='row' spacing={1}>
                  <Autocomplete
                    fullWidth
                    options={clients}
                    onChange={(e, newValue) => changeClient(newValue, index)}
                    autoHighlight
                    getOptionLabel={(option) =>
                      `${option.name} ${option.account_number}`
                    }
                    renderOption={(props, option) => (
                      <Box component='li' sx={{ width: 250 }} {...props}>
                        {option.name} - {option.account_number}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label='Choose a client'
                        inputProps={{
                          ...params.inputProps,
                        }}
                      />
                    )}
                  />

                  <TextField
                    fullWidth
                    select
                    label='Year'
                    {...getFieldProps(`loans[${index}].nepali_year`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].nepali_year`) &&
                        getIn(errors, `loans[${index}].nepali_year`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].nepali_year`) &&
                      getIn(errors, `loans[${index}].nepali_year`)
                    }
                  >
                    {yearList.map((year) => (
                      <MenuItem key={year} value={year}>
                        {year}
                      </MenuItem>
                    ))}
                  </TextField>
                  <TextField
                    fullWidth
                    select
                    label='Month'
                    {...getFieldProps(`loans[${index}].nepali_month`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].nepali_month`) &&
                        getIn(errors, `loans[${index}].nepali_month`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].nepali_month`) &&
                      getIn(errors, `loans[${index}].nepali_month`)
                    }
                  >
                    {monthList.map((month) => (
                      <MenuItem key={month.value} value={month.value}>
                        {month.name}
                      </MenuItem>
                    ))}
                  </TextField>
                  <TextField
                    fullWidth
                    type='number'
                    label='Date'
                    {...getFieldProps(`loans[${index}].nepali_date`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].nepali_date`) &&
                        getIn(errors, `loans[${index}].nepali_date`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].nepali_date`) &&
                      getIn(errors, `loans[${index}].nepali_date`)
                    }
                  />
                  <TextField
                    fullWidth
                    type='string'
                    label='Amount'
                    {...getFieldProps(`loans[${index}].loan_amount`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].loan_amount`) &&
                        getIn(errors, `loans[${index}].loan_amount`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].loan_amount`) &&
                      getIn(errors, `loans[${index}].loan_amount`)
                    }
                  />
                  <TextField
                    fullWidth
                    type='string'
                    label='Interest'
                    {...getFieldProps(`loans[${index}].interest`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].interest`) &&
                        getIn(errors, `loans[${index}].interest`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].interest`) &&
                      getIn(errors, `loans[${index}].interest`)
                    }
                  />
                  <TextField
                    fullWidth
                    type='string'
                    label='Charge'
                    {...getFieldProps(`loans[${index}].charge`)}
                    error={Boolean(
                      getIn(touched, `loans[${index}].charge`) &&
                        getIn(errors, `loans[${index}].charge`)
                    )}
                    helperText={
                      getIn(touched, `loans[${index}].charge`) &&
                      getIn(errors, `loans[${index}].charge`)
                    }
                  />
                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                  >
                    X
                  </Button>

                  <Button
                    variant='outlined'
                    size='large'
                    onClick={() =>
                      arrayHelpers.push({
                        ...transacation,
                        amount: '',
                      })
                    } // insert an empty string at a position
                  >
                    Add
                  </Button>
                </Stack>
              ))
            }
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Transaction
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
