import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, Autocomplete } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@mui/system';
import { editFiscalYearBalance } from '../../../dashboard/store/fiscalyear/FiscalYearActions';
import { addFiscalYearLoanBalance } from '../../../dashboard/store/fiscalyear/FiscalYearLoanActions';

// ----------------------------------------------------------------------

export default function AddFiscalYearLoanForm({ handleClose, fiscalYearLoan }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const clientList = useSelector((state) => state.data.clients);
  const [clients, setClients] = useState([]);

  const LoginSchema = Yup.object().shape({
    year: Yup.number().required('Year is required'),
    remaining_balance: Yup.number().required('Balance is required'),
  });

  useEffect(() => {
    const filteredClients = clientList.filter(
      (client) => client.loan_accounts.length > 0
    );
    setClients(filteredClients);
  }, [clientList]);

  useEffect(() => {
    console.log(fiscalYearLoan);
    if (fiscalYearLoan) {
      changeClient(fiscalYearLoan.client);
    }
    // eslint-disable-next-line
  }, [fiscalYearLoan]);

  const formik = useFormik({
    initialValues: {
      year: fiscalYearLoan?.year || '',
      remaining_balance: fiscalYearLoan?.remaining_balance || '',
      loan_account_id: fiscalYearLoan?.loan_account_id || '',
    },
    validationSchema: LoginSchema,
    onSubmit: (data) => {
      console.log(data);
      // handleClose();
      // navigate('/dashboard', { replace: true });
      if (fiscalYearLoan) {
        dispatch(editFiscalYearBalance(data, fiscalYearLoan.id));
      } else {
        dispatch(addFiscalYearLoanBalance(data));
      }
      setSubmit(true);
    },
  });

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
    values,
    setValues,
  } = formik;

  const changeClient = (value) => {
    if (value) {
      const loan = value.loan_accounts.filter((loan) => loan.is_active);
      if (loan.length > 0) {
        values.loan_account_id = loan[0].id;
        setValues({
          ...values,
        });
      }
    }
  };

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2} direction='column'>
          {fiscalYearLoan ? (
            <TextField
              type='number'
              label='Client Id'
              {...getFieldProps('client_id')}
              error={Boolean(touched.client_id && errors.client_id)}
              helperText={touched.client_id && errors.client_id}
            />
          ) : (
            <Autocomplete
              fullWidth
              options={clients}
              onChange={(e, newValue) => changeClient(newValue)}
              autoHighlight
              getOptionLabel={(option) =>
                `${option.name} ${option.account_number}`
              }
              renderOption={(props, option) => (
                <Box component='li' {...props}>
                  {option.name} - {option.account_number}
                </Box>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Choose a client'
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />
          )}

          <TextField
            type='number'
            label='Year'
            {...getFieldProps('year')}
            error={Boolean(touched.year && errors.year)}
            helperText={touched.year && errors.year}
          />
          <TextField
            type='number'
            label='Balance'
            {...getFieldProps('remaining_balance')}
            error={Boolean(
              touched.remaining_balance && errors.remaining_balance
            )}
            helperText={touched.remaining_balance && errors.remaining_balance}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {fiscalYearLoan ? 'Update Balance' : 'Add Balance'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
