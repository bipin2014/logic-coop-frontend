import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, Autocomplete } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@mui/system';
import {
  addFiscalYearBalance,
  editFiscalYearBalance,
} from '../../../dashboard/store/fiscalyear/FiscalYearActions';

// ----------------------------------------------------------------------

export default function AddFiscalYearForm({ handleClose, fiscalYear }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const [submit, setSubmit] = useState(false);
  const success = useSelector((state) => state.data.success);
  const clients = useSelector((state) => state.data.clients);

  const LoginSchema = Yup.object().shape({
    year: Yup.number().required('Year is required'),
    balance: Yup.number().required('Balance is required'),
  });

  useEffect(() => {
    console.log(fiscalYear);
    if (fiscalYear) {
      changeClient(fiscalYear.client);
    }
    // eslint-disable-next-line
  }, [fiscalYear]);

  const formik = useFormik({
    initialValues: {
      year: fiscalYear?.year || '',
      balance: fiscalYear?.balance || '',
      client_id: fiscalYear?.client?.id || '',
    },
    validationSchema: LoginSchema,
    onSubmit: (data) => {
      console.log(data);
      // handleClose();
      // navigate('/dashboard', { replace: true });
      if (fiscalYear) {
        dispatch(editFiscalYearBalance(data, fiscalYear.id));
      } else {
        dispatch(addFiscalYearBalance(data));
      }
      setSubmit(true);
    },
  });

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line
  }, [submit, success]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
    values,
    setValues,
  } = formik;

  const changeClient = (value) => {
    if (value) {
      values.client_id = value.id;
      setValues({
        ...values,
      });
    }
  };

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2} direction='column'>
          {fiscalYear ? (
            <TextField
              type='number'
              label='Client Id'
              {...getFieldProps('client_id')}
              error={Boolean(touched.client_id && errors.client_id)}
              helperText={touched.client_id && errors.client_id}
            />
          ) : (
            <Autocomplete
              fullWidth
              options={clients}
              onChange={(e, newValue) => changeClient(newValue)}
              autoHighlight
              getOptionLabel={(option) =>
                `${option.name} ${option.account_number}`
              }
              renderOption={(props, option) => (
                <Box component='li' {...props}>
                  {option.name} - {option.account_number}
                </Box>
              )}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Choose a client'
                  inputProps={{
                    ...params.inputProps,
                  }}
                />
              )}
            />
          )}

          <TextField
            type='number'
            label='Year'
            {...getFieldProps('year')}
            error={Boolean(touched.year && errors.year)}
            helperText={touched.year && errors.year}
          />
          <TextField
            type='number'
            label='Balance'
            {...getFieldProps('balance')}
            error={Boolean(touched.balance && errors.balance)}
            helperText={touched.balance && errors.balance}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {fiscalYear ? 'Update Balance' : 'Add Balance'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
