import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { MenuItem, Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getTransactionTypes } from '../../../dashboard/store/transactiontype/TransactionypeActions';
import { PostRequest } from '../../../../utils/axios';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../Toast/store/ToasterAction';
import { formatDate } from '../../../../utils/formatTime';

// ----------------------------------------------------------------------

export default function CalculateInterestForm({
  handleClose,
  interestCalculationList,
  type,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const transactionTypes = useSelector((state) => state.data.transaction_types);
  const [transactionTypeList, setTransactionTypeList] = useState([]);
  const [taxTransactionTypeList, setTaxTransactionTypeList] = useState([]);

  let date = new Date();

  useEffect(() => {
    dispatch(getTransactionTypes());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (type) {
      let transactionTypeList = transactionTypes.filter(
        (transactionType) => transactionType.account_type === type
      );
      setTransactionTypeList(transactionTypeList);
      transactionTypeList = transactionTypes.filter(
        (transactionType) => transactionType.account_type === 'tax'
      );
      setTaxTransactionTypeList(transactionTypeList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [transactionTypes]);

  const addInterestToAccount = (data) => {
    PostRequest(`${API_URL}/member-accounts/add-interest-to-account`, data)
      .then((res) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Success!',
            message: 'Interest Added To Account!',
          })
        );
        handleClose();
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message || 'Failed to fetch employee!',
          })
        );
      });
  };

  const addInterestToLoanAccount = (data) => {
    PostRequest(`${API_URL}/loan-accounts/add-interest-to-account`, data)
      .then((res) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Success!',
            message: 'Interest Added To Loan Account!',
          })
        );
        handleClose();
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message || 'Failed to fetch employee!',
          })
        );
      });
  };

  const BankAccountSchema = Yup.object().shape({
    transaction_type_id: Yup.string().required(
      'Transaction Type Id is required'
    ),
  });

  const formik = useFormik({
    initialValues: {
      transaction_type_id: '',
    },
    validationSchema: BankAccountSchema,
    onSubmit: (data) => {
      // data.members_accounts = [];
      console.log(interestCalculationList);
      const transactions = interestCalculationList.map(
        (interestCalculation) => {
          return {
            member_account_id:
              type === 'deposit' ? interestCalculation.member_account.id : null,
            member_loan_account_id:
              type === 'loan' ? interestCalculation.loan_account.id : null,
            interest: interestCalculation.interest,
            tax: interestCalculation?.tax || 0,
            transaction_date: formatDate(date),
          };
        }
      );
      data.transactions = transactions;

      console.log(data);
      if (type === 'deposit') {
        addInterestToAccount(data);
      } else {
        addInterestToLoanAccount(data);
      }
    },
  });

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            select
            label='Transaction Type'
            {...getFieldProps('transaction_type_id')}
            error={Boolean(
              touched.transaction_type_id && errors.transaction_type_id
            )}
            helperText={
              touched.transaction_type_id && errors.transaction_type_id
            }
          >
            {transactionTypeList.map((transactionType) => (
              <MenuItem key={transactionType.id} value={transactionType.id}>
                {transactionType.name}
              </MenuItem>
            ))}
          </TextField>

          <TextField
            fullWidth
            select
            label='Tax Transaction Type'
            {...getFieldProps('tax_type_id')}
            error={Boolean(touched.tax_type_id && errors.tax_type_id)}
            helperText={touched.tax_type_id && errors.tax_type_id}
          >
            {taxTransactionTypeList.map((transactionType) => (
              <MenuItem key={transactionType.id} value={transactionType.id}>
                {transactionType.name}
              </MenuItem>
            ))}
          </TextField>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Interest To Account
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
