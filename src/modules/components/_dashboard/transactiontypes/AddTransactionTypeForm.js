import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  addTransactionType,
  updateTransactionType,
} from '../../../dashboard/store/transactiontype/TransactionypeActions';

const actions = [
  { title: 'INCREASE_ACCOUNT_BALANCE', type: 'deposit' },
  { title: 'DECREASE_ACCOUNT_BALANCE', type: 'deposit' },
  { title: 'INCREASE_LOAN_BALANCE', type: 'loan' },
  { title: 'INCREASE_LOAN_PAID', type: 'loan' },
  { title: 'INCREASE_LOAN_INTEREST', type: 'loan' },
  { title: 'INCREASE_SHARE_AMOUNT', type: 'share' },
  { title: 'DECREASE_SHARE_AMOUNT', type: 'share' },
];

export default function AddTransactionTypeForm({
  handleClose,
  transactionType,
}) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [actionList, setActionList] = useState([]);
  const [submit, setSubmit] = useState(false);

  const TransactionSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
    type: Yup.string().required('Type is required'),
  });

  const formik = useFormik({
    initialValues: {
      name: transactionType?.name || '',
      action: transactionType?.action || '',
      type: transactionType?.type || '',
      account_type: transactionType?.account_type || '',
    },
    validationSchema: TransactionSchema,
    onSubmit: (data) => {
      console.log(data);
      if (transactionType) {
        dispatch(updateTransactionType(data, transactionType.id));
      } else {
        dispatch(addTransactionType(data));
      }
      setSubmit(true);
      // handleClose();
    },
  });

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submit, success]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
    values,
  } = formik;

  useEffect(() => {
    console.log(values);
    if (values.account_type) {
      const newActions = actions.filter(
        (action) => action.type === values.account_type
      );
      setActionList(newActions);
    } else {
      setActionList([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values.account_type]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <TextField
            fullWidth
            type='text'
            label='Name'
            {...getFieldProps('name')}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />
          <Stack direction='row' spacing={2}>
            <TextField
              fullWidth
              select
              label='Type'
              {...getFieldProps('type')}
              error={Boolean(touched.type && errors.type)}
              helperText={touched.type && errors.type}
            >
              <MenuItem key={'DEBIT'} value={'DEBIT'}>
                DEBIT
              </MenuItem>
              <MenuItem key={'CREDIT'} value={'CREDIT'}>
                CREDIT
              </MenuItem>
              <MenuItem key={'BOTH'} value={'BOTH'}>
                BOTH
              </MenuItem>
            </TextField>

            <TextField
              fullWidth
              select
              label='Account Type'
              {...getFieldProps('account_type')}
              error={Boolean(touched.account_type && errors.account_type)}
              helperText={touched.account_type && errors.account_type}
            >
              <MenuItem key={'deposit'} value={'deposit'}>
                Deposit
              </MenuItem>
              <MenuItem key={'loan'} value={'loan'}>
                Loan
              </MenuItem>
              <MenuItem key={'tax'} value={'tax'}>
                Tax
              </MenuItem>
              <MenuItem key={'share'} value={'share'}>
                Share
              </MenuItem>
            </TextField>
          </Stack>

          <TextField
            fullWidth
            select
            label='Action'
            {...getFieldProps('action')}
            error={Boolean(touched.action && errors.action)}
            helperText={touched.action && errors.action}
          >
            {actionList.map((action) => (
              <MenuItem key={action.title} value={action.title}>
                {action.title}
              </MenuItem>
            ))}
          </TextField>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {transactionType
              ? 'Update Transaction Type'
              : 'Add Transaction Type'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
