import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, Autocomplete, Box } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { getMembers } from '../../../dashboard/store/member/MemberActions';
import { addMemberShare } from '../../../dashboard/store/membershare/MemberShareActions';

// ----------------------------------------------------------------------

export default function AddMemberShareForm({ handleClose }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const memberList = useSelector((state) => state.data.members);
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  useEffect(() => {
    dispatch(getMembers());
    // eslint-disable-next-line
  }, []);

  const MemberAccountSchema = Yup.object().shape({
    member_id: Yup.string().required('Name is required'),
  });

  const formik = useFormik({
    initialValues: {
      member_id: '',
    },
    validationSchema: MemberAccountSchema,
    onSubmit: (data) => {
      // console.log(data);
      dispatch(addMemberShare(data));

      setSubmit(true);
    },
  });

  const {
    isSubmitting,
    handleSubmit,
    setSubmitting,
    setValues,
    values,
  } = formik;

  useEffect(() => {
    if (success && submit) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [success, submit]);

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  const changeMember = (newValue) => {
    setValues({
      ...values,
      member_id: newValue.id,
    });
  };

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Autocomplete
            fullWidth
            options={memberList}
            onChange={(e, newValue) => changeMember(newValue)}
            autoHighlight
            getOptionLabel={(option) => `${option.name}-${option.id}`}
            renderOption={(props, option) => (
              <Box component='li' sx={{ width: 250 }} {...props}>
                {option.name} - {option.id}
              </Box>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label='Choose a member'
                inputProps={{
                  ...params.inputProps,
                }}
              />
            )}
          />

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            Add Member Share
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
