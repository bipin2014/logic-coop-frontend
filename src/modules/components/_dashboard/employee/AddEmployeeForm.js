import * as Yup from "yup";
import { useEffect } from "react";
import { useFormik, Form, FormikProvider } from "formik";
// material
import { Stack, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { useDispatch, useSelector } from "react-redux";
import { addEmployee } from "../../../dashboard/store/employee/EmployeeActions";
import { removeEmptyKeyFromObject } from "../../../../config";

// ----------------------------------------------------------------------

export default function AddEmployeeForm({ handleClose }) {
    // const navigate = useNavigate();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.data.loading);

    const LoginSchema = Yup.object().shape({
        email: Yup.string().email("Email must be a valid email address"),
        name: Yup.string().required("Name is required"),
        phone: Yup.string().required("Phone is required"),
        address: Yup.string().required("Account is required"),
        joined_date: Yup.date().required("Joined Date is required"),
        position: Yup.string().required("Position is required"),
        salary: Yup.number().required("Salary is required"),
    });

    const formik = useFormik({
        initialValues: {
            name: "",
            email: "",
            phone: "",
            address: "",
            joined_date: "",
            salary: "",
            position: "",
        },
        validationSchema: LoginSchema,
        onSubmit: (data) => {
            console.log(data);
            const newdata = removeEmptyKeyFromObject(data);
            console.log(newdata);
            dispatch(addEmployee(newdata));
            // handleClose();
            // navigate('/dashboard', { replace: true });
        },
    });

    const {
        errors,
        touched,
        isSubmitting,
        handleSubmit,
        getFieldProps,
        setSubmitting,
    } = formik;

    useEffect(() => {
        setSubmitting(loading);
        // eslint-disable-next-line
    }, [loading]);

    return (
        <FormikProvider value={formik}>
            <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                <Stack spacing={2}>
                    <Stack direction="row" spacing={2}>
                        <TextField
                            fullWidth
                            autoComplete="name"
                            type="text"
                            label="Full Name"
                            {...getFieldProps("name")}
                            error={Boolean(touched.name && errors.name)}
                            helperText={touched.name && errors.name}
                        />
                        <TextField
                            fullWidth
                            autoComplete="username"
                            type="email"
                            label="Email address"
                            {...getFieldProps("email")}
                            error={Boolean(touched.email && errors.email)}
                            helperText={touched.email && errors.email}
                        />
                    </Stack>
                    <Stack direction="row" spacing={2}>
                        <TextField
                            autoComplete="phone"
                            type="string"
                            label="Phone Number"
                            {...getFieldProps("phone")}
                            error={Boolean(touched.phone && errors.phone)}
                            helperText={touched.phone && errors.phone}
                        />
                        {
                            // <LocalizationProvider dateAdapter={AdapterDateFns}>
                            //     <DatePicker
                            //         label="Basic example"
                            //         {...getFieldProps("joined_date")}
                            //         renderInput={(params) => (
                            //             <TextField {...params} />
                            //         )}
                            //     />
                            // </LocalizationProvider>
                        }
                        {
                            <TextField
                                type="date"
                                label="Joined Date"
                                {...getFieldProps("joined_date")}
                                error={Boolean(
                                    touched.joined_date && errors.joined_date
                                )}
                                helperText={
                                    touched.joined_date && errors.joined_date
                                }
                            />
                        }
                    </Stack>
                    <Stack direction="row" spacing={2}>
                        <TextField
                            type="number"
                            label="Salary"
                            {...getFieldProps("salary")}
                            error={Boolean(touched.salary && errors.salary)}
                            helperText={touched.salary && errors.salary}
                        />
                        <TextField
                            type="text"
                            label="Position"
                            {...getFieldProps("position")}
                            error={Boolean(touched.position && errors.position)}
                            helperText={touched.position && errors.position}
                        />
                    </Stack>
                    <TextField
                        type="text"
                        label="Full Address"
                        {...getFieldProps("address")}
                        error={Boolean(touched.address && errors.address)}
                        helperText={touched.address && errors.address}
                    />

                    <LoadingButton
                        fullWidth
                        size="large"
                        type="submit"
                        variant="contained"
                        loading={isSubmitting}
                    >
                        Add Employee
                    </LoadingButton>
                </Stack>
            </Form>
        </FormikProvider>
    );
}
