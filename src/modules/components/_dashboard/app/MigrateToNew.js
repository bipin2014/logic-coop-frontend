import { Stack, Card, Typography, CardHeader, Button } from '@mui/material';
// utils
import { PostRequest } from '../../../../utils/axios';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../Toast/store/ToasterAction';
import { useDispatch } from 'react-redux';
//

export default function MigrateToNew() {
  const dispatch = useDispatch();

  const migrateClientToMember = (payload) => {
    PostRequest(`${API_URL}/client-to-member`, payload)
      .then((res) => {
        console.log(res.data);
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Sucess!',
            message: 'Sucessfully Migrated!',
          })
        );
      })
      .catch((errors) => {
        console.log(errors);
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to fetch transactions!',
          })
        );
      });
  };

  const migrateLoanToMemberLoan = () => {
    PostRequest(`${API_URL}/loan-to-member-loan`)
      .then((res) => {
        console.log(res.data);
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Sucess!',
            message: 'Sucessfully Migrated To Member Loan!',
          })
        );
      })
      .catch((errors) => {
        console.log(errors);
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to fetch transactions!',
          })
        );
      });
  };

  return (
    <Card>
      <CardHeader title='Migrate Client- To-Member' />

      <Stack spacing={3} sx={{ p: 3 }}>
        <Typography>
          First pull everthing, Make a backup of previous Db, Migrate
          Database,Then Start this process Create Transaction Type 105 id
          Opening Balance
        </Typography>
        <Button
          variant='contained'
          onClick={() =>
            migrateClientToMember({ account_type_id: '1', prefix: 'OS' })
          }
        >
          Client to Member-Prefix- OS - account_type_id-1
        </Button>
        <Button
          variant='contained'
          onClick={() =>
            migrateClientToMember({ account_type_id: '2', prefix: 'PS' })
          }
        >
          Client to Member-Prefix- PS - account_type_id-2
        </Button>
        <Button
          variant='contained'
          onClick={() =>
            migrateClientToMember({ account_type_id: '3', prefix: 'CES' })
          }
        >
          Client to Member-Prefix- CES - account_type_id-3
        </Button>
        <Button
          variant='contained'
          onClick={() =>
            migrateClientToMember({ account_type_id: '4', prefix: 'MS' })
          }
        >
          Client to Member-Prefix- MS - account_type_id-4
        </Button>
        <Button
          variant='contained'
          onClick={() =>
            migrateClientToMember({ account_type_id: '5', prefix: 'FD' })
          }
        >
          Client to Member-Prefix- FD - account_type_id-5
        </Button>
      </Stack>

      <CardHeader title='Migrate To Member-Loan-Account' />

      <Stack spacing={3} sx={{ p: 3 }}>
        <Typography>First create one loan type</Typography>
        <Button variant='contained' onClick={migrateLoanToMemberLoan}>
          Loan to Member Loan
        </Button>
      </Stack>
    </Card>
  );
}
