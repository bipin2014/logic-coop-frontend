import { Link as RouterLink } from 'react-router-dom';
// material
import {
  Box,
  Stack,
  Link,
  Card,
  Typography,
  CardHeader,
  Avatar,
} from '@mui/material';
// utils
import { GetRequest } from '../../../../utils/axios';
import { API_URL } from '../../../../config';
import { useEffect, useState } from 'react';
//

// ----------------------------------------------------------------------

function UserItem({ client }) {
  const { name, dob } = client;

  return (
    <Stack direction='row' alignItems='center' spacing={2}>
      <Avatar alt={name} src={''} />
      <Box sx={{ minWidth: 240 }}>
        <Link to='#' color='inherit' underline='hover' component={RouterLink}>
          <Typography variant='subtitle2' noWrap>
            {name}
          </Typography>
        </Link>
        <Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
          {dob}
        </Typography>
      </Box>
    </Stack>
  );
}

export default function AppBirthDayList() {
  const [today, setToday] = useState([]);
  const [tomorrow, setTomorrow] = useState([]);

  useEffect(() => {
    const getUserBirthday = () => {
      GetRequest(`${API_URL}/clients/bday`).then((data) => {
        console.log(data.data);
        setToday(data.data.today);
        setTomorrow(data.data.tomorrow);
      });
    };
    getUserBirthday();
  }, []);
  return (
    <Card>
      <CardHeader title='Today-BirthDay' />

      <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
        {today.length > 0 ? (
          today.map((client) => <UserItem key={client} client={client} />)
        ) : (
          <Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
            No Birthday Today
          </Typography>
        )}
      </Stack>
      <CardHeader title='Tomorrow-BirthDay' />

      <Stack spacing={3} sx={{ p: 3, pr: 0 }}>
        {tomorrow.length > 0 ? (
          tomorrow.map((client) => <UserItem key={client} client={client} />)
        ) : (
          <Typography variant='body2' sx={{ color: 'text.secondary' }} noWrap>
            No Birthday Tomorrow
          </Typography>
        )}
      </Stack>
    </Card>
  );
}
