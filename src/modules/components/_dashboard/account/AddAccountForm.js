import * as Yup from "yup";
import { useEffect } from "react";
import { useFormik, Form, FormikProvider } from "formik";
// material
import { Stack, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { useDispatch, useSelector } from "react-redux";
import { addAccount } from "../../../dashboard/store/account/AccountActions";

export default function AddAccountForm({ handleClose }) {
    // const navigate = useNavigate();
    const dispatch = useDispatch();
    const loading = useSelector((state) => state.data.loading);

    const AccountSchema = Yup.object().shape({
        title: Yup.string().required("Title is required"),
    });

    const formik = useFormik({
        initialValues: {
            title: "",
        },
        validationSchema: AccountSchema,
        onSubmit: (data) => {
            dispatch(addAccount(data));
            // handleClose();
        },
    });

    const {
        errors,
        touched,
        isSubmitting,
        handleSubmit,
        getFieldProps,
        setSubmitting,
    } = formik;

    useEffect(() => {
        setSubmitting(loading);
        // eslint-disable-next-line
    }, [loading]);

    return (
        <FormikProvider value={formik}>
            <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                <Stack spacing={2}>
                    <TextField
                        fullWidth
                        autoComplete="name"
                        type="text"
                        label="Title"
                        {...getFieldProps("title")}
                        error={Boolean(touched.title && errors.title)}
                        helperText={touched.title && errors.title}
                    />

                    <LoadingButton
                        fullWidth
                        size="large"
                        type="submit"
                        variant="contained"
                        loading={isSubmitting}
                    >
                        Add Account
                    </LoadingButton>
                </Stack>
            </Form>
        </FormikProvider>
    );
}
