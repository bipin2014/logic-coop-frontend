import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useFormik, Form, FormikProvider } from 'formik';
// material
import { Stack, TextField, MenuItem } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useDispatch, useSelector } from 'react-redux';
import {
  addAccountType,
  updateAccountType,
} from '../../../dashboard/store/accounttype/AccountTypeActions';

export default function AddAccountTypeForm({ handleClose, accountType }) {
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.data.loading);
  const success = useSelector((state) => state.data.success);
  const [submit, setSubmit] = useState(false);

  const AccountSchema = Yup.object().shape({
    title: Yup.string().required('Title is required'),
    intrest: Yup.number().required('Intrest is required'),
    account_number_length: Yup.number().required(
      'Account Number Length is required'
    ),
    account_opening_balance: Yup.number().required(
      'Account Opening Balance is required'
    ),
    closing_charge: Yup.number().required('Closing Charge Length is required'),
    collector_commission: Yup.number().required(
      'Collector Commission is required'
    ),
    prefix: Yup.string().required('Prefix is required'),
    tax_rate: Yup.number().required('Tax Rate is required'),
    minimum_balance: Yup.number().required('Minimum Balance is required'),
  });

  const formik = useFormik({
    initialValues: {
      title: accountType?.title || '',
      intrest: accountType?.intrest || 0.0,
      tax_rate: accountType?.tax_rate || 0,
      account_number_length: accountType?.account_number_length || 5,
      prefix: accountType?.prefix || '',
      account_opening_balance: accountType?.account_opening_balance || 0,
      closing_charge: accountType?.closing_charge || 0,
      collector_commission: accountType?.collector_commission || 0,
      minimum_balance: accountType?.minimum_balance || 0,
      can_withdraw: accountType?.can_withdraw || 1,
    },
    validationSchema: AccountSchema,
    onSubmit: (data) => {
      if (accountType) {
        dispatch(updateAccountType(data, accountType.id));
      } else {
        dispatch(addAccountType(data));
      }
      setSubmit(true);
      // handleClose();
    },
  });

  useEffect(() => {
    if (submit && success) {
      handleClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submit, success]);

  const {
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    getFieldProps,
    setSubmitting,
  } = formik;

  useEffect(() => {
    setSubmitting(loading);
    // eslint-disable-next-line
  }, [loading]);

  return (
    <FormikProvider value={formik}>
      <Form autoComplete='off' noValidate onSubmit={handleSubmit}>
        <Stack spacing={2}>
          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='text'
              label='Title'
              {...getFieldProps('title')}
              error={Boolean(touched.title && errors.title)}
              helperText={touched.title && errors.title}
            />

            <TextField
              fullWidth
              select
              label='Can WithDraw'
              {...getFieldProps('can_withdraw')}
              error={Boolean(touched.can_withdraw && errors.can_withdraw)}
              helperText={touched.can_withdraw && errors.can_withdraw}
            >
              <MenuItem key={'1'} value={1}>
                Can Withdraw
              </MenuItem>
              <MenuItem key={'0'} value={0}>
                Cannot Withdraw
              </MenuItem>
            </TextField>
          </Stack>

          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='number'
              label='Intrest'
              {...getFieldProps('intrest')}
              error={Boolean(touched.intrest && errors.intrest)}
              helperText={touched.intrest && errors.intrest}
            />
            <TextField
              fullWidth
              type='number'
              label='Tax Rate'
              {...getFieldProps('tax_rate')}
              error={Boolean(touched.tax_rate && errors.tax_rate)}
              helperText={touched.tax_rate && errors.tax_rate}
            />
          </Stack>
          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='text'
              label='Prefix'
              {...getFieldProps('prefix')}
              error={Boolean(touched.prefix && errors.prefix)}
              helperText={touched.prefix && errors.prefix}
            />

            <TextField
              fullWidth
              type='number'
              label='Accoun Number Length'
              {...getFieldProps('account_number_length')}
              error={Boolean(
                touched.account_number_length && errors.account_number_length
              )}
              helperText={
                touched.account_number_length && errors.account_number_length
              }
            />
          </Stack>

          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='number'
              label='Account Opening Balance'
              {...getFieldProps('account_opening_balance')}
              error={Boolean(
                touched.account_opening_balance &&
                  errors.account_opening_balance
              )}
              helperText={
                touched.account_opening_balance &&
                errors.account_opening_balance
              }
            />

            <TextField
              fullWidth
              type='number'
              label='Closing Charge'
              {...getFieldProps('closing_charge')}
              error={Boolean(touched.closing_charge && errors.closing_charge)}
              helperText={touched.closing_charge && errors.closing_charge}
            />
          </Stack>

          <Stack direction='row' spacing={1}>
            <TextField
              fullWidth
              type='number'
              label='Collector Commission'
              {...getFieldProps('collector_commission')}
              error={Boolean(
                touched.collector_commission && errors.collector_commission
              )}
              helperText={
                touched.collector_commission && errors.collector_commission
              }
            />

            <TextField
              fullWidth
              type='number'
              label='Minimum Balance'
              {...getFieldProps('minimum_balance')}
              error={Boolean(touched.minimum_balance && errors.minimum_balance)}
              helperText={touched.minimum_balance && errors.minimum_balance}
            />
          </Stack>

          <LoadingButton
            fullWidth
            size='large'
            type='submit'
            variant='contained'
            loading={isSubmitting}
          >
            {accountType ? 'Update Account Type' : 'Add Account Type'}
          </LoadingButton>
        </Stack>
      </Form>
    </FormikProvider>
  );
}
