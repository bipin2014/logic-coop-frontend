import React from 'react';
import {
  AppBar,
  Dialog,
  DialogContent,
  IconButton,
  Toolbar,
  Typography,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

export default function MyDialog({
  open,
  handleClose,
  title,
  maxWidth,
  children,
  fullScreen,
}) {
  return (
    <Dialog
      open={open}
      fullScreen={fullScreen || false}
      fullWidth={true}
      maxWidth={maxWidth || 'md'}
      onClose={handleClose}
      aria-labelledby='responsive-dialog-title'
    >
      <AppBar sx={{ position: 'relative' }}>
        <Toolbar>
          <IconButton
            edge='start'
            color='inherit'
            onClick={handleClose}
            aria-label='close'
          >
            <CloseIcon />
          </IconButton>
          <Typography sx={{ ml: 2, flex: 1 }} variant='h6' component='div'>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent dividers>{children}</DialogContent>
    </Dialog>
  );
}
