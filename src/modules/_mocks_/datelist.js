export const yearList = [2077, 2078];
export const monthList = [
  { name: 'Baishakh', value: '01' },
  { name: 'Jestha', value: '02' },
  { name: 'Ashar', value: '03' },
  { name: 'Shrawan', value: '04' },
  { name: 'Bhadra', value: '05' },
  { name: 'Ashwin', value: '06' },
  { name: 'Kartik', value: '07' },
  { name: 'Mangsir', value: '08' },
  { name: 'Poush', value: '09' },
  { name: 'Magh', value: '10' },
  { name: 'Falgun', value: '11' },
  { name: 'Chaitra', value: '12' },
];
