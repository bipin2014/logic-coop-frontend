import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import AddClientForm from "../../components/_dashboard/client/AddClientForm";

export default function AddClient({ open, handleClose }) {
    return (
        <Dialog
            open={open}
            fullScreen={false}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">Add Client</DialogTitle>
            <DialogContent dividers>
                <AddClientForm handleClose={handleClose} />
            </DialogContent>
        </Dialog>
    );
}
