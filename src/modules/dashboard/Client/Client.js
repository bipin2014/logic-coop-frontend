import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';

// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import AddClient from './AddClient';
import { useDispatch, useSelector } from 'react-redux';
import { getClients } from '../store/client/ClientActions';
import AddTransaction from '../Transaction/AddTransaction';
import { useHistory } from 'react-router';
import MyDialog from '../../components/MyDialog';
import MultipleTransactionForm from '../../components/_dashboard/transaction/MultipleTransactionForm';
import MultipleLoanForm from '../../components/_dashboard/loan/MultipleLoanForm';
//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'account_number', label: 'Account Number', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'account_type', label: 'Account Type', alignRight: false },
  { id: 'balance', label: 'Balance', alignRight: false },
  { id: 'addtransaction', label: 'Add Transaction', alignRight: false },
  { id: 'details', label: 'View', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Client() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [clients, setClients] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');

  const setDialogOpen = (value) => {
    setOpen(value);
  };
  const history = useHistory();

  const viewDetails = (data) => {
    console.log(data);
    history.push(`clients/${data.id}`);
  };

  const clientList = useSelector((state) => state.data.clients);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getClients());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setClients(clientList);
  }, [clientList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = clients.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - clients.length) : 0;

  const filteredUsers = applySortFilter(
    clients,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const addClient = () => {
    setDialog('addClient');
    setDialogOpen(true);
  };
  const addTransactions = () => {
    setDialog('addTransactions');
    setDialogOpen(true);
  };
  const addLoans = () => {
    setDialog('addLoans');
    setDialogOpen(true);
  };

  const addClientTransaction = (data) => {
    setDialog('addClientTransaction');
    setDialogOpen(true);
    setData(data);
  };

  return (
    <Page title='Clients'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Clients
          </Typography>
          <Stack direction='row' alignItems='center' spacing={2}>
            <Button
              variant='contained'
              onClick={addLoans}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Loans
            </Button>
            <Button
              variant='contained'
              onClick={addTransactions}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Tranactions
            </Button>
            <Button
              variant='contained'
              onClick={addClient}
              startIcon={<Icon icon={plusFill} />}
            >
              New Client
            </Button>
          </Stack>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={clients.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const {
                      id,
                      account_number,
                      account_type,
                      name,
                      client_acount,
                    } = row;
                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, name)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{account_number}</TableCell>
                        </TableCell>
                        <TableCell component='th' scope='row' padding='none'>
                          <Stack
                            direction='row'
                            alignItems='center'
                            spacing={2}
                          >
                            <Avatar alt={name} src={''} />
                            <Typography variant='subtitle2' noWrap>
                              {name}
                            </Typography>
                          </Stack>
                        </TableCell>

                        <TableCell align='left'>{account_type.title}</TableCell>
                        <TableCell align='left'>
                          Rs. {client_acount?.balance}
                        </TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => addClientTransaction(row)}
                          >
                            Transaction
                          </Button>
                        </TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => viewDetails(row)}
                          >
                            View
                          </Button>
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={clients.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {dialog === 'addClient' && (
        <AddClient open={open} handleClose={() => setDialogOpen(false)} />
      )}
      {dialog === 'addClientTransaction' && (
        <AddTransaction
          open={open}
          handleClose={() => setDialogOpen(false)}
          data={data}
        />
      )}

      {dialog === 'addTransactions' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Multiple Transactions'
          maxWidth='lg'
        >
          <MultipleTransactionForm
            handleClose={() => setOpen(false)}
            clients={clients}
          />
        </MyDialog>
      )}
      {dialog === 'addLoans' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Multiple Loans'
          maxWidth='lg'
        >
          <MultipleLoanForm
            handleClose={() => setOpen(false)}
            clients={clients}
          />
        </MyDialog>
      )}
    </Page>
  );
}
