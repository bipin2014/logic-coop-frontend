import { useState } from 'react';

// material
import { Stack, Container, Tabs, Tab } from '@mui/material';
// components
import Page from '../../components/Page';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import OtherTransactionList from '../OtherTransaction/OtherTransactionList';
import AccountSummary from '../Summary/AccountSummary';

export default function BankAccountDetails() {
  const { id } = useParams();
  const [tabValue, setTabValue] = useState('transactions');
  const [filterData] = useState({ type: 'bank', item_id: id });

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Page title='Bank Account Details'>
      <Container>
        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value='details' label='Details' />
            <Tab value='transactions' label='Day Transactions' />
            <Tab value='summary' label='Summary' />
          </Tabs>

          {tabValue === 'transactions' && (
            <OtherTransactionList filterData={filterData} />
          )}
          {tabValue === 'summary' && <AccountSummary filterData={filterData} />}
        </Stack>
      </Container>
    </Page>
  );
}
