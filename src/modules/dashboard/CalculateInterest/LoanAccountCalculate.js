import { filter } from 'lodash';
import { useEffect, useState } from 'react';
// material
import {
  Card,
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  Container,
  Stack,
  Button,
  TextField,
  MenuItem,
  Box,
  Typography,
} from '@mui/material';
// components
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { useDispatch, useSelector } from 'react-redux';
import { GetRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import { setToasterState } from '../../components/Toast/store/ToasterAction';
import MyDialog from '../../components/MyDialog';
import Page from '../../components/Page';
import CalculateInterestForm from '../../components/_dashboard/calculateinterest/CalculateInterestForm';
import { getLoanAccountTypes } from '../store/loanaccounttype/LoanAccountTypeActions';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';

//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Member Name', alignRight: false },
  { id: 'account', label: 'Loan Account', alignRight: false },
  { id: 'interest_rate', label: 'Interest Rate', alignRight: false },
  { id: 'interest', label: 'Interest', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user?.loan_account?.member?.name
          .toLowerCase()
          .indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function LoanAccountCalculate() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('title');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [accountInterestList, setAccountInterestList] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const dispatch = useDispatch();
  const [filterData, setFilterData] = useState({});
  const history = useHistory();

  const [totalInterest, setTotalInterest] = useState(0);

  const loanAccountTypeList = useSelector(
    (state) => state.data.loan_account_types
  );

  const getCalculatedInterest = () => {
    GetRequest(`${API_URL}/loan-accounts/calculate-interest`, filterData)
      .then((res) => {
        console.log(res.data);
        setAccountInterestList(res.data.data);
        setTotalInterest(res.data.total_interest);
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to fetch transactions!',
          })
        );
      });
  };

  const calculateInterestDialog = () => {
    setDialog('calculateInterestDialog');
    setOpen(true);
  };

  useEffect(() => {
    dispatch(getLoanAccountTypes());
    // eslint-disable-next-line
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = accountInterestList.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - accountInterestList.length)
      : 0;

  const filteredUsers = applySortFilter(
    accountInterestList,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const changeLoanTypeId = (e) => {
    setFilterData({
      ...filterData,
      loan_type_id: e.target.value,
    });
  };

  const deleteItem = (data) => {

    const newaccountInterestList = accountInterestList.filter(
      (accountInterest, i) =>
        accountInterest.loan_account.id !== data.loan_account.id
    );
    setAccountInterestList([...newaccountInterestList]);
  };

  const changeAmount = (e, data) => {
    const index = accountInterestList.findIndex(
      (accountInterest) =>
        accountInterest.loan_account.id === data.loan_account.id
    );

    const newaccountInterestList = accountInterestList.map(
      (accountInterest, i) => {
        if (index === i) {
          return {
            ...accountInterest,
            interest: parseInt(e.target.value),
          };
        } else {
          return accountInterest;
        }
      }
    );
    setAccountInterestList([...newaccountInterestList]);
  };

   useEffect(() => {
     let total_interest = 0;
     accountInterestList.forEach((accountInterest) => {
       total_interest += accountInterest.interest;
     });
     setTotalInterest(total_interest);
   }, [accountInterestList]);

  const handleClose = () => {
    setOpen(true);
    history.push('/dashboard/loan-account');
  };

  return (
    <Page title='Calculate Loan Interest'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <TextField
            fullWidth
            select
            label='Loan Account Type'
            value={filterData?.loan_type_id}
            onChange={changeLoanTypeId}
          >
            {loanAccountTypeList.map((loanAccountType) => (
              <MenuItem key={loanAccountType.id} value={loanAccountType.id}>
                {loanAccountType.name}
              </MenuItem>
            ))}
          </TextField>

          <Button variant='contained' onClick={getCalculatedInterest}>
            Analyze
          </Button>
        </Stack>
        <Card>
          <Box
            sx={{
              padding: 2,
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Typography variant='h6' component='h1'>
              Total Interest: Rs. {totalInterest}
            </Typography>
            {accountInterestList && accountInterestList.length > 0 && (
              <Button variant='contained' onClick={calculateInterestDialog}>
                Finalize Interests
              </Button>
            )}
          </Box>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={accountInterestList.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, loan_account, interest } = row;
                    const { member } = loan_account;

                    const isItemSelected = selected.indexOf(id) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>

                        <TableCell component='th' scope='row'>
                          {member?.name}
                        </TableCell>

                        <TableCell component='th' scope='row'>
                          {loan_account?.account_number}
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          {loan_account.interest_rate}%
                        </TableCell>
                        <TableCell component='th' scope='row' padding='none'>
                          <Stack direction={'row'} alignItems={'center'}>
                            Rs.{' '}
                            <TextField
                              fullWidth
                              type='number'
                              label='Interest'
                              value={interest}
                              onChange={(e) => changeAmount(e, row)}
                            />
                          </Stack>
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu deleteOnClick={() => deleteItem(row)} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={accountInterestList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>

        {open && dialog === 'calculateInterestDialog' && (
          <MyDialog
            open={open}
            title='Add Interest Transactions'
            handleClose={() => setOpen(false)}
          >
            <CalculateInterestForm
              handleClose={handleClose}
              interestCalculationList={accountInterestList}
              type='loan'
            />
          </MyDialog>
        )}
      </Container>
    </Page>
  );
}
