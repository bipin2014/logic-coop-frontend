import { useState } from 'react';
// material
import { Stack, Container, Tabs, Tab, Typography } from '@mui/material';
// components
import Page from '../../components/Page';
import DepositAccountCalculate from './DepositAccountCalculate';
import LoanAccountCalculate from './LoanAccountCalculate';

export default function CalculateInterest() {
  const [tabValue, setTabValue] = useState('deposit');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Page title='CalculateIntrest'>
      <Container>
        <Stack direction='column' spacing={2}>
          <Typography variant='h4' gutterBottom>
            Calculate Interest
          </Typography>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value='deposit' label='Deposit Account' />
            <Tab value='loan' label='Loan Account' />
          </Tabs>
          {tabValue === 'deposit' && <DepositAccountCalculate />}
          {tabValue === 'loan' && <LoanAccountCalculate />}
        </Stack>
      </Container>
    </Page>
  );
}
