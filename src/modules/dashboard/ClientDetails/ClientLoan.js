// material
import {
  Container,
  Stack,
  Typography,
  Card,
  Button,
  CardContent,
  CardActions,
  Grid,
} from '@mui/material';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { API_URL } from '../../../config';
import { DeleteRequest, PutRequest } from '../../../utils/axios';
import Label from '../../components/Label';
import MyAlert from '../../components/MyAlert';
import MyDialog from '../../components/MyDialog';
import Page from '../../components/Page';
import { setToasterState } from '../../components/Toast/store/ToasterAction';
import AddClientLoan from '../../components/_dashboard/client/AddClientLoan';
import ViewLoanTransactions from '../../components/_dashboard/client/ViewLoanTransactions';
import { setSuccess } from '../store/client/ClientActions';
// ----------------------------------------------------------------------

export default function ClientLoan({ client }) {
  // const navigate = useNavigate();
  const [dialog, setDialog] = useState('');
  const [open, setOpen] = useState(false);
  const [activeData, setActiveData] = useState('');
  const dispatch = useDispatch();

  const openAddLoan = () => {
    setOpen(true);
    setDialog('addLoan');
  };

  const viewTransactions = (data) => {
    setActiveData(data);
    setOpen(true);
    setDialog('viewTransactions');
  };
  const editLoan = (data) => {
    setActiveData(data);
    setOpen(true);
    setDialog('editLoan');
  };
  const closeLoan = (data) => {
    setActiveData(data);
    setOpen(true);
    setDialog('closeLoan');
  };

  const deleteLoan = (id) => {
    setActiveData(id);
    setOpen(true);
    setDialog('deleteLoan');
  };
  const calculateInterest = (data) => {
    setActiveData(data);
    setOpen(true);
    setDialog('calculateInterest');
  };

  const deleteClientLoan = (id) => {
    DeleteRequest(`${API_URL}/loans/${id}`)
      .then((res) => {
        setOpen(false);
        dispatch(setSuccess(true));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Transaction Delete!',
            message: 'Successfully Deleted Transaction!',
          })
        );
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to delete transactions!',
          })
        );
      });
  };
  const closeClientLoan = (id) => {
    PutRequest(`${API_URL}/loans/close/${id}`)
      .then((res) => {
        setOpen(false);
        dispatch(setSuccess(true));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Loan Closed!',
            message: 'Successfully Closed Loan!',
          })
        );
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to delete transactions!',
          })
        );
      });
  };

  return (
    <Page title='Clients'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Client Loan
          </Typography>
          <Button variant='contained' onClick={openAddLoan}>
            New Loan
          </Button>
        </Stack>
        {client.loan_accounts?.length > 0 ? (
          client.loan_accounts.map((loan) => (
            <Card>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography component='div' variant='h5'>
                  Loan Amount: Rs. {loan.loan_amount}
                </Typography>
                <Typography
                  variant='subtitle1'
                  color='text.secondary'
                  component='div'
                >
                  Remaining Amount: Rs. {loan.remaning_amount}
                </Typography>
                <Typography
                  variant='subtitle1'
                  color='text.secondary'
                  component='div'
                >
                  Loan Charge: Rs. {loan.charge}
                </Typography>
                <Typography
                  variant='subtitle1'
                  color='text.secondary'
                  component='div'
                >
                  {loan.loan_date} - {loan.maturity_date}
                </Typography>
                <Grid spacing={2}>
                  <Label color={'success'}>{loan.type}</Label>
                  <Label color={'success'}>{loan.interest}%</Label>
                  <Label
                    variant='ghost'
                    color={loan.is_active ? 'success' : 'error'}
                  >
                    {loan.is_active ? 'Active' : 'Closed'}
                  </Label>
                </Grid>
              </CardContent>
              <CardActions>
                <Button onClick={() => viewTransactions(loan)}>
                  View Transctions
                </Button>
                <Button onClick={() => editLoan(loan)}>Edit</Button>
                <Button onClick={() => closeLoan(loan.id)}>Close Loan</Button>
                <Button onClick={() => deleteLoan(loan.id)}>Delete</Button>
                <Button onClick={() => calculateInterest(loan)}>
                  Calculate Intrest
                </Button>
              </CardActions>
            </Card>
          ))
        ) : (
          <div>No Loans</div>
        )}
      </Container>
      {open && dialog === 'addLoan' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Loan'
        >
          <AddClientLoan client={client} handleClose={() => setOpen(false)} />
        </MyDialog>
      )}
      {open && dialog === 'editLoan' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Edit Loan'
        >
          <AddClientLoan
            client={client}
            handleClose={() => setOpen(false)}
            loanAccount={activeData}
          />
        </MyDialog>
      )}
      {open && dialog === 'viewTransactions' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='View Transactions'
        >
          <ViewLoanTransactions
            loan={activeData}
            handleClose={() => setOpen(false)}
          />
        </MyDialog>
      )}

      {open && dialog === 'closeLoan' && (
        <MyAlert
          open={open}
          handleClose={() => setOpen(false)}
          handleSubmit={() => closeClientLoan(activeData)}
          title='Close Loan'
          content='Sure want to close this loan?'
        />
      )}
      {open && dialog === 'deleteLoan' && (
        <MyAlert
          open={open}
          handleClose={() => setOpen(false)}
          handleSubmit={() => deleteClientLoan(activeData)}
          title='Delete Loan'
        />
      )}
    </Page>
  );
}
