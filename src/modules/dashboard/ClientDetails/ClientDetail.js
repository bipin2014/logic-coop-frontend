import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import backArrowFill from '@iconify/icons-eva/arrow-back-fill';

// material
import {
  Stack,
  Button,
  Container,
  Typography,
  Card,
  Tabs,
  Tab,
} from '@mui/material';
// components
import Page from '../../components/Page';
import { useParams, useHistory } from 'react-router';
import { GetRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import EditClientDetails from '../../components/_dashboard/client/EditClientDetails';
import { Box } from '@mui/system';
import AddDocument from '../../components/_dashboard/client/AddDocument';
import MyDialog from '../../components/MyDialog';
import TransactionList from '../Transaction/TransactionList';
import AddTransactionForm from '../../components/_dashboard/transaction/AddTransactionForm';
import { useSelector } from 'react-redux';
import ClientLoan from './ClientLoan';
import refresh from '@iconify/icons-ic/refresh';
import AddMultipleTransactionForm from '../../components/_dashboard/transaction/AddMultipleTransactionForm';
//

export default function ClientDetail() {
  const { id } = useParams();
  const [client, setClient] = useState({});
  const [dialog, setDialog] = useState('');
  const [open, setOpen] = useState(false);
  const history = useHistory();
  const success = useSelector((state) => state.data.success);

  const [tabValue, setTabValue] = useState('details');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    getClients();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (success) {
      getClients();
    }
    // eslint-disable-next-line
  }, [success]);

  const getClients = () => {
    GetRequest(`${API_URL}/clients/${id}`)
      .then((res) => {
        console.log(res.data);
        setClient(res.data.data);
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const refreshClientBalance = () => {
    GetRequest(`${API_URL}/clients/${id}/transactions/refresh`)
      .then((res) => {
        getClients();
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const openAddTransactionForClient = () => {
    setOpen(true);
    setDialog('addTransaction');
  };
  const openAddMultipleTransactionForClient = () => {
    setOpen(true);
    setDialog('addMultipleTransaction');
  };
  const goBack = () => {
    history.goBack();
  };
  return (
    <Page title='Clients'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Button
            variant='contained'
            onClick={goBack}
            startIcon={<Icon icon={backArrowFill} />}
          >
            Back
          </Button>
          <div>
            <Typography variant='h4' gutterBottom>
              {client.name}
            </Typography>
            <Stack direction='row' alignItems='center'>
              <Typography variant='h6' gutterBottom>
                Saving Balance: Rs. {client?.client_acount?.balance}
              </Typography>
              <Icon icon={refresh} onClick={refreshClientBalance}></Icon>
            </Stack>
          </div>
          <Stack direction='column' spacing={1}>
            <Button
              variant='contained'
              onClick={openAddTransactionForClient}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Transaction
            </Button>
            <Button
              variant='contained'
              onClick={openAddMultipleTransactionForClient}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Multiple Transaction
            </Button>
          </Stack>
        </Stack>
        <Card>
          <Box sx={{ p: 2 }}>
            <Stack direction='column' spacing={2}>
              <Tabs
                value={tabValue}
                onChange={handleChange}
                textColor='secondary'
                indicatorColor='secondary'
                aria-label='secondary tabs example'
              >
                <Tab value='details' label='Details' />
                <Tab value='transactions' label='Transactions' />
                <Tab value='documents' label='Documents' />
                <Tab value='loan' label='Loan' />
              </Tabs>
              {tabValue === 'details' && client.id && (
                <EditClientDetails client={client} />
              )}

              {tabValue === 'transactions' && client.id && (
                <TransactionList client={client} />
              )}
              {tabValue === 'documents' && client.id && (
                <AddDocument
                  handleClose={() => setOpen(false)}
                  client={client}
                />
              )}
              {tabValue === 'loan' && client.id && (
                <ClientLoan client={client} />
              )}
            </Stack>
          </Box>
        </Card>
      </Container>
      {open && dialog === 'addTransaction' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Transaction'
        >
          <AddTransactionForm
            handleClose={() => setOpen(false)}
            client={client}
          />
        </MyDialog>
      )}
      {open && dialog === 'addMultipleTransaction' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Multiple Transaction'
          maxWidth='lg'
        >
          <AddMultipleTransactionForm
            handleClose={() => setOpen(false)}
            client={client}
          />
        </MyDialog>
      )}
    </Page>
  );
}
