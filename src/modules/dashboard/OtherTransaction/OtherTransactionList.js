import { filter } from 'lodash';
import { useEffect, useState } from 'react';
// material
import {
  Card,
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  Typography,
  Box,
  Container,
  Stack,
  Button,
} from '@mui/material';
// components
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { useDispatch, useSelector } from 'react-redux';
import { DeleteRequest, GetRequest } from '../../../utils/axios';
import { API_URL, checkOneDigitDate } from '../../../config';
import { setToasterState } from '../../components/Toast/store/ToasterAction';
import { setSuccess } from '../store/client/ClientActions';
import NepaliDate from 'nepali-date-converter';
import Label from '../../components/Label';
import MyAlert from '../../components/MyAlert';
import MyDialog from '../../components/MyDialog';
import Page from '../../components/Page';
import { Icon } from '@iconify/react';
import plusFill from '@iconify/icons-eva/plus-fill';
import AddOtherTransactionForm from '../../components/_dashboard/othertransaction/AddOtherTransactionForm';
import { TextField, MenuItem } from '@mui/material';
import { getTransactionTypes } from '../store/transactiontype/TransactionypeActions';

//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'date', label: 'Date', alignRight: false },
  { id: 'amount', label: 'Amount', alignRight: false },
  { id: 'transaction_type', label: 'Transaction Type', alignRight: false },
  { id: 'balance', label: 'Balance', alignRight: false },
  { id: 'type', label: 'Type', alignRight: false },
  { id: 'remarks', label: 'Remarks', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user?.member_account?.member?.name
          .toLowerCase()
          .indexOf(query.toLowerCase()) !== -1 &&
        _user?.member_loan_account?.member?.name
          .toLowerCase()
          .indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function OtherTransactionList({ filterData }) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('title');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [transactions, setTransactions] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [activeData, setActiveData] = useState('');
  const dispatch = useDispatch();
  const success = useSelector((state) => state.data.success);
  const [transactionFilter, setTransactionFilter] = useState(filterData);

  const transactionTypeList = useSelector(
    (state) => state.data.transaction_types
  );

  const getOtherTransactions = () => {
    GetRequest(`${API_URL}/othertransactions`, transactionFilter)
      .then((res) => {
        console.log(res.data);
        setTransactions(res.data.data.reverse());
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to fetch transactions!',
          })
        );
      });
  };

  useEffect(() => {
    getOtherTransactions();
    // eslint-disable-next-line
  }, [transactionFilter]);

  const deleteOtherTransactions = (id) => {
    DeleteRequest(`${API_URL}/othertransactions/${id}`)
      .then((res) => {
        setOpen(false);
        getOtherTransactions();
        dispatch(setSuccess(true));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Transaction Delete!',
            message: 'Successfully Deleted Transaction!',
          })
        );
      })
      .catch((errors) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Sorry!',
            message:
              errors?.response?.data?.message ||
              'Failed to delete transactions!',
          })
        );
      });
  };

  useEffect(() => {
    getOtherTransactions();
    dispatch(getTransactionTypes());
    // eslint-disable-next-line
  }, []);
  useEffect(() => {
    if (success) {
      getOtherTransactions();
    }
    // eslint-disable-next-line
  }, [success]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = transactions.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - transactions.length) : 0;

  const filteredUsers = applySortFilter(
    transactions,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const addOtherTransaction = () => {
    setDialog('addOtherTransaction');
    setOpen(true);
  };
  const editOnClick = (transaction) => {
    setActiveData(transaction);
    setOpen(true);
    setDialog('editTransaction');
  };
  const openDeleteAlert = () => {
    setOpen(true);
    setDialog('deleteTransaction');
  };

  const deleteOnClick = (id) => {
    setActiveData(id);
    openDeleteAlert();
  };

  const transactionIdHandler = (e) => {
    setTransactionFilter({
      ...transactionFilter,
      transaction_type_id: e.target.value ? e.target.value : null,
    });
  };

  return (
    <Page title='Other Transactions'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Other Transactions
          </Typography>

          <Button
            variant='contained'
            onClick={addOtherTransaction}
            startIcon={<Icon icon={plusFill} />}
          >
            New Other Transaction
          </Button>
        </Stack>
        <Card>
          <Box
            sx={{
              padding: 2,
              display: 'flex',
              justifyContent: 'space-between',
              '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
          >
            <Typography variant='h6' component='h1'>
              Filter: {JSON.stringify(transactionFilter)}
            </Typography>

            <TextField
              select
              label='Transaction Type'
              value={transactionFilter?.transaction_type_id || ''}
              onChange={transactionIdHandler}
            >
              <MenuItem key={''} value={''}>
                -
              </MenuItem>
              {transactionTypeList.map((transactionType) => (
                <MenuItem key={transactionType.id} value={transactionType.id}>
                  {transactionType.name}
                </MenuItem>
              ))}
            </TextField>
          </Box>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={transactions.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const {
                      id,
                      remarks,
                      transaction_date,
                      amount,
                      transaction_type,
                      type,
                      balance,
                    } = row;

                    let nepaliTransactionDate = new NepaliDate(
                      new Date(transaction_date)
                    ).getBS();
                    let td = new NepaliDate(
                      `${nepaliTransactionDate.year}-${checkOneDigitDate(
                        nepaliTransactionDate.month + 1
                      )}-${checkOneDigitDate(nepaliTransactionDate.date)}`
                    );
                    const isItemSelected = selected.indexOf(id) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>

                        <TableCell component='th' scope='row'>
                          {td.format('YYYY-MM-DD')}
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          Rs. {amount}
                        </TableCell>
                        <TableCell component='th' scope='row' padding='none'>
                          <Label
                            variant='ghost'
                            color={
                              (transaction_type?.type === 'DEBIT' && 'error') ||
                              'success'
                            }
                          >
                            {transaction_type?.name}
                          </Label>
                        </TableCell>

                        <TableCell align='left'>
                          Rs. {balance.toFixed(2)}
                        </TableCell>
                        <TableCell align='left'>{type}</TableCell>
                        <TableCell align='left'>{remarks}</TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            editOnClick={() => editOnClick(row)}
                            deleteOnClick={() => deleteOnClick(id)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={transactions.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>

        {open && dialog === 'deleteTransaction' && (
          <MyAlert
            open={open}
            handleClose={() => setOpen(false)}
            handleSubmit={() => deleteOtherTransactions(activeData)}
            title='Delete Transaction'
          />
        )}

        {open && dialog === 'addOtherTransaction' && (
          <MyDialog
            open={open}
            title='Add Other Transaction'
            handleClose={() => setOpen(false)}
          >
            <AddOtherTransactionForm
              handleClose={() => setOpen(false)}
              transaction={activeData}
            />
          </MyDialog>
        )}

        {open && dialog === 'editTransaction' && (
          <MyDialog
            open={open}
            title='Edit transaction'
            handleClose={() => setOpen(false)}
          >
            <AddOtherTransactionForm
              handleClose={() => setOpen(false)}
              transaction={activeData}
              type={activeData.type}
            />
          </MyDialog>
        )}
      </Container>
    </Page>
  );
}
