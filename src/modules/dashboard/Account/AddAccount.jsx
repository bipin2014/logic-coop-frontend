import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import AddAccountForm from "../../components/_dashboard/account/AddAccountForm";

export default function AddAccount({ open, handleClose }) {
    return (
        <Dialog
            open={open}
            fullScreen={false}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">Add Account</DialogTitle>
            <DialogContent dividers>
                <AddAccountForm handleClose={handleClose} />
            </DialogContent>
        </Dialog>
    );
}
