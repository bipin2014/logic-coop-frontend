import { Icon } from '@iconify/react';
import pieChart2Fill from '@iconify/icons-eva/pie-chart-2-fill';
import Transaction from '@iconify/icons-ant-design/account-book';
import peopleFill from '@iconify/icons-eva/people-fill';
import employee from '@iconify/icons-ic/3d-rotation';
import summary from '@iconify/icons-ic/round-summarize';
import money from '@iconify/icons-ic/monetization-on';
import stock from '@iconify/icons-ant-design/stock';

const getIcon = (name) => <Icon icon={name} width={22} height={22} />;

const sidebarConfig = [
  {
    title: 'dashboard',
    path: '/dashboard/app',
    icon: getIcon(pieChart2Fill),
  },
  {
    title: 'Members',
    path: '/dashboard/members',
    icon: getIcon(peopleFill),
  },
  {
    title: 'Deposit Account',
    path: '/dashboard/members-accounts',
    icon: getIcon(peopleFill),
  },
  {
    title: 'Deposit AccountType',
    path: '/dashboard/account-types',
    icon: getIcon(peopleFill),
  },
  {
    title: 'Loan Account',
    path: '/dashboard/loan-account',
    icon: getIcon(money),
  },
  {
    title: 'Loan AccountType',
    path: '/dashboard/loan-account-type',
    icon: getIcon(money),
  },
  {
    title: 'Employees',
    path: '/dashboard/employees',
    icon: getIcon(employee),
  },
  {
    title: 'TransactionType',
    path: '/dashboard/transaction-types',
    icon: getIcon(Transaction),
  },
  {
    title: 'Day Summary',
    path: '/dashboard/daybook',
    icon: getIcon(summary),
  },
  {
    title: 'Bank Account',
    path: '/dashboard/banks',
    icon: getIcon(money),
  },
  {
    title: 'Other Transactions',
    path: '/dashboard/other-transactions',
    icon: getIcon(money),
  },
  {
    title: 'Calculate Interest',
    path: '/dashboard/calculate-interest',
    icon: getIcon(money),
  },
  {
    title: 'Members Share',
    path: '/dashboard/member-shares',
    icon: getIcon(stock),
  },
];

export default sidebarConfig;
