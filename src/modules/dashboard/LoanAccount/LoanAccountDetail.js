import { useEffect, useState } from 'react';
import { Icon } from '@iconify/react';
// material
import { Container, Stack, Typography, Tab, Tabs, Button } from '@mui/material';
// components
import Page from '../../components/Page';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { API_URL } from '../../../config';
import { GetRequest } from '../../../utils/axios';
import backArrowFill from '@iconify/icons-eva/arrow-back-fill';
import TransactionList from '../Transaction/TransactionList';
import refresh from '@iconify/icons-ic/refresh';
import AddLoanAccountForm from '../../components/_dashboard/loanaccount/AddLoanAccountForm';
import AccountSummary from '../Summary/AccountSummary';

// ----------------------------------------------------------------------

export default function LoanAccountDetail() {
  const { id } = useParams();
  const [loanAccount, setLoanAccount] = useState({});
  const history = useHistory();
  const success = useSelector((state) => state.data.success);

  const [tabValue, setTabValue] = useState('details');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    getLoanAccountDetails();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (success) {
      getLoanAccountDetails();
    }
    // eslint-disable-next-line
  }, [success]);

  const getLoanAccountDetails = () => {
    GetRequest(`${API_URL}/loan-accounts/${id}`)
      .then((res) => {
        console.log(res);
        setLoanAccount(res.data.data);
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const refreshClientBalance = () => {
    GetRequest(`${API_URL}/loan-account/refresh-balance`, { id })
      .then((res) => {
        getLoanAccountDetails();
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const goBack = () => {
    history.goBack();
  };

  return (
    <Page title='Loan Accounts Details'>
      <Container>
        <Button
          variant='contained'
          onClick={goBack}
          startIcon={<Icon icon={backArrowFill} />}
        >
          Back
        </Button>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
        >
          <Typography variant='h4' gutterBottom>
            Account Number: {loanAccount?.account_number}
          </Typography>
          <Typography variant='h4' gutterBottom>
            Interest: Rs.{loanAccount?.total_interest}
          </Typography>
          <Typography variant='h4' gutterBottom>
            Balance: Rs. {loanAccount?.issue_amount}/{loanAccount?.paid_amount}
            <Icon icon={refresh} onClick={refreshClientBalance} />
          </Typography>
        </Stack>

        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value={'details'} label={'Details'} />
            <Tab value={'transactions'} label={'Transactions'} />
            <Tab value={'summary'} label={'Summary'} />
          </Tabs>
          {tabValue === 'details' && loanAccount.id && (
            <AddLoanAccountForm
              loanType={loanAccount.loan_type}
              loanAccount={loanAccount}
              handleClose={() => {}}
            />
          )}
          {tabValue === 'transactions' && loanAccount.id && (
            <TransactionList
              filterData={{ member_loan_account_id: loanAccount.id }}
            />
          )}
          {tabValue === 'summary' && loanAccount.id && (
            <AccountSummary
              filterData={{
                member_loan_account_id: loanAccount.id,
                type: 'loan',
              }}
            />
          )}
        </Stack>
      </Container>
    </Page>
  );
}
