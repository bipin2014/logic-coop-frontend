import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import refreshFill from '@iconify/icons-eva/refresh-outline';

// material
import { Stack, Button, Container, Typography, Tabs, Tab } from '@mui/material';
// components
import Page from '../../components/Page';
import { useDispatch, useSelector } from 'react-redux';
import { getLoanAccounts } from '../store/loanaccount/LoanAccountActions';
import MyDialog from '../../components/MyDialog';
import LoanAccountList from './LoanAccountList';
import { getLoanAccountTypes } from '../store/loanaccounttype/LoanAccountTypeActions';
import AddLoanAccountForm from '../../components/_dashboard/loanaccount/AddLoanAccountForm';
import { PostRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import { setToasterState } from '../../components/Toast/store/ToasterAction';
import NewMultipleTransactionForm from '../../components/_dashboard/transaction/NewMultipleTransactionForm';

export default function LoanAccounts() {
  const [tabValue, setTabValue] = useState('');
  const [activeloanAccountType, setloanActiveAccountType] = useState('');

  const [currentLoanAccountList, setLoanMemberAccountList] = useState([]);
  const loanAccountTypeList = useSelector(
    (state) => state.data.loan_account_types
  );
  const loanAccountList = useSelector((state) => state.data.loan_accounts);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    if (loanAccountTypeList.length > 0) {
      setTabValue(loanAccountTypeList[0].id);
      setData(loanAccountTypeList[0].id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loanAccountTypeList, loanAccountList]);

  useEffect(() => {
    dispatch(getLoanAccountTypes());
    dispatch(getLoanAccounts());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
    setData(newValue);
  };

  const setData = (id) => {
    const accountType = loanAccountTypeList.find(
      (accountType) => accountType.id === id
    );
    setloanActiveAccountType(accountType);
    const currentMemberlist = loanAccountList.filter(
      (memberAccount) => memberAccount.loan_type_id === id
    );
    setLoanMemberAccountList(currentMemberlist);
  };

  const addMemberAccount = () => {
    setOpen(true);
    setDialog('addMemberAccount');
  };

  const addMultiTransactions = () => {
    setDialog('addMultiTransactions');
    setOpen(true);
  };

  const refreshAllLoan = () => {
    PostRequest(`${API_URL}/loan-accounts/refresh-all`)
      .then((res) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Loan Account !',
            message: 'Refreshed Loan Account!',
          })
        );
        dispatch(getLoanAccounts());
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Page title='Loan Accounts'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
        >
          <Typography variant='h4' gutterBottom>
            Loan Accounts
          </Typography>
          <Stack direction='row' alignItems='center' spacing={2}>
            <Button
              variant='contained'
              onClick={addMultiTransactions}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Transactions
            </Button>
            <Button
              variant='contained'
              onClick={refreshAllLoan}
              startIcon={<Icon icon={refreshFill} />}
            >
              Refresh Data
            </Button>
            <Button
              variant='contained'
              onClick={addMemberAccount}
              startIcon={<Icon icon={plusFill} />}
            >
              New Loan Account
            </Button>
          </Stack>
        </Stack>

        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            {loanAccountTypeList.map((accountType) => (
              <Tab value={accountType.id} label={accountType.name} />
            ))}
          </Tabs>
          <LoanAccountList
            loanTypeId={tabValue}
            currentMemberAccountList={currentLoanAccountList}
          />
        </Stack>
      </Container>
      {open && dialog === 'addMemberAccount' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title={'Add Loan Account'}
        >
          <AddLoanAccountForm
            loanType={activeloanAccountType}
            handleClose={() => setOpen(false)}
            memberAccountList={loanAccountList}
          />
        </MyDialog>
      )}

      {open && dialog === 'addMultiTransactions' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title={"Add Today's Transaction"}
        >
          <NewMultipleTransactionForm
            handleClose={() => setOpen(false)}
            memberAccounts={loanAccountList}
            transactionTypeAccount={'loan'}
          />
        </MyDialog>
      )}
    </Page>
  );
}
