import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Page404 from '../../../pages/Page404';
import ROUTELIST from './routeList';

const DashboardRoute = () => {
  return (
    <>
      <React.Suspense fallback={<div>Loading...</div>}>
        <Switch>
          {ROUTELIST.map((route, key) => (
            <Route
              path={`/dashboard${route.path}`}
              name={route.name}
              component={route.component}
              exact={route.exact}
              key={key}
            />
          ))}
          <Route path='*' component={Page404} />
        </Switch>
      </React.Suspense>
    </>
  );
};
export default DashboardRoute;
