import DashboardApp from '../DashboardApp';
import Client from '../Client/Client';
import ClientDetail from '../ClientDetails/ClientDetail';
import Employee from '../Employee/Employee';
import Account from '../Account/Account';
import AccountType from '../AccountType/AccountType';
import TransactionType from '../TransactionType/TransactionType';
import DayBook from '../DayBook/DayBook';
import CalculateInterest from '../CalculateInterest/CalculateInterest';
import PreviousBalance from '../PreviousBalance/PreviousBalance';
import Members from '../Members/Members';
import MembersAccounts from '../MembersAccounts/MembersAccounts';
import MemberDetail from '../MemberDetails/MemberDetails';
import MemberAccountDetail from '../MembersAccounts/MemberAccountDetail';
import LoanAccountType from '../LoanAccountType/LoanAccountType';
import LoanAccounts from '../LoanAccount/LoanAccounts';
import LoanAccountDetail from '../LoanAccount/LoanAccountDetail';
import BankAccounts from '../BankAccount/BankAccount';
import OtherTransactionList from '../OtherTransaction/OtherTransactionList';
import BankAccountDetails from '../BankAccount/BankAccountDetails';
import MemberShare from '../MemberShare/MemberShare';
import MemberShareDetail from '../MemberShare/MemberShareDetail';
import EmployeeDetails from '../Employee/EmployeeDetails';

// eslint-disable-next-line
export default [
  {
    path: '/',
    name: 'app',
    exact: true,
    component: DashboardApp,
  },
  {
    path: '/app',
    name: 'dashboard',
    exact: true,
    component: DashboardApp,
  },
  {
    path: '/clients',
    name: 'clients',
    exact: true,
    component: Client,
  },
  {
    path: '/clients/:id',
    name: 'clientdetails',
    exact: true,
    component: ClientDetail,
  },
  {
    path: '/employees',
    name: 'employees',
    exact: true,
    component: Employee,
  },
  {
    path: '/employees/:id',
    name: 'employees',
    exact: true,
    component: EmployeeDetails,
  },
  {
    path: '/accounts',
    name: 'accounts',
    exact: true,
    component: Account,
  },
  {
    path: '/account-types',
    name: 'accounttypes',
    exact: true,
    component: AccountType,
  },
  {
    path: '/transaction-types',
    name: 'transaction-types',
    exact: true,
    component: TransactionType,
  },
  {
    path: '/daybook',
    name: 'daybook',
    exact: true,
    component: DayBook,
  },
  {
    path: '/interest',
    name: 'interest',
    exact: true,
    component: CalculateInterest,
  },
  {
    path: '/previous-balances',
    name: 'Previous Balances',
    exact: true,
    component: PreviousBalance,
  },

  //new after adjustment

  {
    path: '/members',
    name: 'members',
    exact: true,
    component: Members,
  },
  {
    path: '/members/:id',
    name: 'memberdetails',
    exact: true,
    component: MemberDetail,
  },
  {
    path: '/members-accounts',
    name: 'members-accounts',
    exact: true,
    component: MembersAccounts,
  },
  {
    path: '/members-accounts/:id',
    name: 'members-accounts',
    exact: true,
    component: MemberAccountDetail,
  },
  {
    path: '/loan-account',
    name: 'loan-account',
    exact: true,
    component: LoanAccounts,
  },
  {
    path: '/loan-account/:id',
    name: 'loan-account',
    exact: true,
    component: LoanAccountDetail,
  },
  {
    path: '/loan-account-type',
    name: 'loan-account-type',
    exact: true,
    component: LoanAccountType,
  },
  {
    path: '/banks',
    name: 'banks',
    exact: true,
    component: BankAccounts,
  },
  {
    path: '/banks/:id',
    name: 'banks',
    exact: true,
    component: BankAccountDetails,
  },

  {
    path: '/other-transactions',
    name: 'other-transactions',
    exact: true,
    component: OtherTransactionList,
  },

  {
    path: '/calculate-interest',
    name: 'calculate-interest',
    exact: true,
    component: CalculateInterest,
  },
  {
    path: '/member-shares',
    name: 'member-shares',
    exact: true,
    component: MemberShare,
  },
  {
    path: '/member-shares/:id',
    name: 'member-shares-details',
    exact: true,
    component: MemberShareDetail,
  },
];
