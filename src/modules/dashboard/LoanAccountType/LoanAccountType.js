import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';

// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import { useDispatch, useSelector } from 'react-redux';
import { filter } from 'lodash';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import SearchNotFound from '../../components/SearchNotFound';
import { getLoanAccountTypes } from '../store/loanaccounttype/LoanAccountTypeActions';
import MyDialog from '../../components/MyDialog';
import AddLoanAccountTypeForm from '../../components/_dashboard/loanaccount/AddLoanAccountTypeForm';

const TABLE_HEAD = [
  { id: 'id', label: 'Id', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'prefix', label: 'Prefix', alignRight: false },
  { id: 'interest_rate', label: 'Intrest', alignRight: false },
  {
    id: 'service_charge',
    label: 'Service Charge',
    alignRight: false,
  },
  {
    id: 'collector_commission',
    label: 'Colector Commision',
    alignRight: false,
  },
  { id: '' },
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.title.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function LoanAccountType() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('title');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [loanAccountTypes, setLoanAccountTypes] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');

  const addLoanAccountType = () => {
    setOpen(true);
    setDialog('addLoanAccountType');
  };
  const editLoanAccountType = (value) => {
    setOpen(true);
    setData(value);
    setDialog('editLoanAccountType');
  };

  const loanAccountTypeList = useSelector(
    (state) => state.data.loan_account_types
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLoanAccountTypes());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setLoanAccountTypes(loanAccountTypeList);
  }, [loanAccountTypeList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = loanAccountTypes.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - loanAccountTypes.length)
      : 0;

  const filteredUsers = applySortFilter(
    loanAccountTypes,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title='Loan Account Types'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Loan Account Types
          </Typography>
          <Button
            variant='contained'
            onClick={addLoanAccountType}
            startIcon={<Icon icon={plusFill} />}
          >
            New Loan AccountType
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={loanAccountTypes.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const {
                      id,
                      name,
                      interest_rate,
                      prefix,
                      collector_commission,
                      service_charge,
                    } = row;
                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, name)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{id}</TableCell>
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          {name}
                        </TableCell>

                        <TableCell align='left'>{prefix}</TableCell>
                        <TableCell align='left'>{interest_rate}</TableCell>
                        <TableCell align='left'>{service_charge}</TableCell>
                        <TableCell align='left'>
                          {collector_commission}
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            editOnClick={() => editLoanAccountType(row)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={loanAccountTypes.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'addLoanAccountType' && (
        <MyDialog
          open={open}
          title={'Add Loan Account Type'}
          handleClose={() => setOpen(false)}
        >
          <AddLoanAccountTypeForm handleClose={() => setOpen(false)} />
        </MyDialog>
      )}
      {open && dialog === 'editLoanAccountType' && (
        <MyDialog
          open={open}
          title={'Edit Loan Account Type'}
          handleClose={() => setOpen(false)}
        >
          <AddLoanAccountTypeForm
            loanAccountType={data}
            handleClose={() => setOpen(false)}
          />
        </MyDialog>
      )}
    </Page>
  );
}
