import { useState } from 'react';
// material
import { Stack, Container, Tabs, Tab } from '@mui/material';
// components
import Page from '../../components/Page';
import 'nepali-datepicker-reactjs/dist/index.css';
import DayBookList from '../DayBookList/DayBookList';
import DayBookDetail from './DayBookDetail';
import DayTransactionList from '../Transaction/DayTransactionList';
import OtherTransactionList from '../OtherTransaction/OtherTransactionList';
import ShareTransactionList from '../ShareTransaction/ShareTransactionList';

//

// ----------------------------------------------------------------------

export default function DayBook() {
  const [tabValue, setTabValue] = useState('details');
  const [filterData, setFilterData] = useState('');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Page title='DayBook'>
      <Container>
        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value='details' label='Details' />
            <Tab value='list' label='Day BookList' />
            <Tab value='transactions' label='Day Transactions' />
            <Tab value='othertransactions' label='Other Transactions' />
            <Tab value='sharetransactions' label='Share Transactions' />
          </Tabs>
          {tabValue === 'details' && (
            <DayBookDetail
              handleTabChange={handleChange}
              setFilterData={setFilterData}
            />
          )}
          {tabValue === 'list' && <DayBookList />}
          {tabValue === 'transactions' && (
            <DayTransactionList filterData={filterData} />
          )}
          {tabValue === 'othertransactions' && (
            <OtherTransactionList filterData={filterData} />
          )}
          {tabValue === 'sharetransactions' && (
            <ShareTransactionList filterData={filterData} />
          )}
        </Stack>
      </Container>
    </Page>
  );
}
