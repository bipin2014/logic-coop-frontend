import { useEffect, useState } from 'react';
// material
import {
  Card,
  Table,
  Stack,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  Grid,
  TableHead,
  Button,
} from '@mui/material';
// components
import Page from '../../components/Page';
import { useDispatch, useSelector } from 'react-redux';
import { getDayBook } from '../store/transaction/TransactionActions';
import NepaliDate from 'nepali-date-converter';
import { checkOneDigitDate } from '../../../config';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';
import 'nepali-datepicker-reactjs/dist/index.css';
import { addDayBook } from '../store/daybook/DaybookActions';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'remarks', label: 'Transaction', alignRight: false },
  { id: 'amount', label: 'Amount', alignRight: false },
];

// ----------------------------------------------------------------------

export default function DayBookDetail({ handleTabChange, setFilterData }) {
  const [debitList, setDebitList] = useState([]);
  const [creditList, setCreditList] = useState([]);
  const [debitTotal, setDebitTotal] = useState(0);
  const [creditTotal, setCreditTotal] = useState(0);
  const [closingBalance, setClosingBalance] = useState(0);
  const daybookList = useSelector((state) => state.data.daybook.transactions);
  const prevDaybook = useSelector((state) => state.data.daybook.daybook);
  const dispatch = useDispatch();
  const [nepaliDate, setNepaliDate] = useState(new NepaliDate());
  let date = new Date();
  const [dateString, setDateString] = useState(
    `${date.getFullYear()}-${checkOneDigitDate(
      date.getMonth() + 1
    )}-${checkOneDigitDate(date.getDate())}`
  );

  const closeDayBook = () => {
    const data = {
      opening_balance: prevDaybook?.closing_balance || 0,
      total_income: creditTotal,
      total_expenses: debitTotal,
      closing_balance: closingBalance,
      date: dateString,
    };
    dispatch(addDayBook(data));
  };

  useEffect(() => {
    setClosingBalance(0);
    dispatch(
      getDayBook({
        date: dateString,
      })
    );
    // eslint-disable-next-line
  }, [dateString]);

  useEffect(() => {
    if (daybookList) {
      const debits = daybookList.filter(
        (daybook) =>
          daybook.transaction_type.type === 'DEBIT' ||
          daybook.transaction_type.type === 'BOTH'
      );
      setDebitList(debits);
      let debitTotal = 0;
      debits.forEach((item) => {
        debitTotal += parseFloat(item.total_amount);
      });
      setDebitTotal(debitTotal);

      const credits = daybookList.filter(
        (daybook) =>
          daybook.transaction_type.type === 'CREDIT' ||
          daybook.transaction_type.type === 'BOTH'
      );
      setCreditList(credits);

      let creditTotal = 0;
      credits.forEach((item) => {
        creditTotal += parseFloat(item.total_amount);
      });
      setCreditTotal(creditTotal);

      console.log({ debits, credits });
    }
  }, [daybookList]);

  const clickTableRow = (data) => {
    console.log(data);
    const { transaction_type_id } = data;

    if (data.type === 'deposit') {
      setFilterData({
        transaction_date: dateString,
        account_type_id: data.account_type.id,
        transaction_type_id,
      });
      handleTabChange(null, 'transactions');
    } else if (data.type === 'loan') {
      setFilterData({
        transaction_date: dateString,
        loan_type_id: data.account_type.id,
        transaction_type_id,
      });
      handleTabChange(null, 'transactions');
    } else if (data.type === 'share') {
      setFilterData({
        transaction_date: dateString,
        transaction_type_id,
      });
      handleTabChange(null, 'sharetransactions');
    } else {
      setFilterData({
        transaction_date: dateString,
        type: data.type,
        transaction_type_id,
      });
      handleTabChange(null, 'othertransactions');
    }
  };

  useEffect(() => {
    if (prevDaybook && prevDaybook.closing_balance) {
      setClosingBalance(prevDaybook.closing_balance + creditTotal - debitTotal);
    }
  }, [debitTotal, creditTotal, prevDaybook]);

  const changeNepaliDate = (value) => {
    let date = new NepaliDate(value);
    setNepaliDate(date);
    let nepalidate = date.getAD();
    setDateString(
      `${nepalidate.year}-${checkOneDigitDate(
        nepalidate.month + 1
      )}-${checkOneDigitDate(nepalidate.date)}`
    );
  };

  return (
    <Page title='DayBook'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <NepaliDatePicker onChange={changeNepaliDate} />
          <Typography variant='h4' gutterBottom>
            Daybook of {nepaliDate.format('ddd, MMMM-DD')}
          </Typography>
        </Stack>
        <Typography variant='h4' padding='10px'>
          Opening Balance : Rs. {prevDaybook?.closing_balance || 0}
        </Typography>
        <Grid container spacing={2}>
          <Grid item sm={12} md={6}>
            <Card>
              <Typography variant='h4' textAlign='center' padding='5px'>
                Credit
              </Typography>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {TABLE_HEAD.map((head) => (
                        <TableCell align={head.alignRight ? 'right' : 'left'}>
                          {head.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {creditList.map((row) => {
                      const {
                        id,
                        total_amount,
                        transaction_type,
                        account_type,
                      } = row;

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          onClick={() => clickTableRow(row)}
                        >
                          <TableCell component='th' scope='row'>
                            {account_type?.title} --
                            {transaction_type.name}
                          </TableCell>
                          <TableCell component='th' scope='row' padding='none'>
                            Rs. {total_amount}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                    <TableRow hover key={'credit-total'}>
                      <TableCell component='th' scope='row'>
                        <Typography variant='h5'>Credit Total</Typography>
                      </TableCell>
                      <TableCell component='th' scope='row' padding='none'>
                        <Typography variant='h5'>Rs. {creditTotal}</Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Card>
          </Grid>
          <Grid item sm={12} md={6}>
            <Card>
              <Typography variant='h4' textAlign='center' padding='5px'>
                Debit
              </Typography>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {TABLE_HEAD.map((head) => (
                        <TableCell align={head.alignRight ? 'right' : 'left'}>
                          {head.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {debitList.map((row) => {
                      const {
                        id,
                        total_amount,
                        transaction_type,
                        account_type,
                      } = row;

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          onClick={() => clickTableRow(row)}
                        >
                          <TableCell component='th' scope='row'>
                            {account_type?.title} --
                            {transaction_type.name}
                          </TableCell>
                          <TableCell component='th' scope='row' padding='none'>
                            Rs. {total_amount}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                    <TableRow hover key={'debit-total'}>
                      <TableCell component='th' scope='row'>
                        <Typography variant='h5'>Debit Total</Typography>
                      </TableCell>
                      <TableCell component='th' scope='row' padding='none'>
                        <Typography variant='h5'>Rs. {debitTotal}</Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Card>
            <Typography variant='h4' padding='10px'>
              Closing Balance : Rs. {closingBalance || 0}
            </Typography>
          </Grid>
        </Grid>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mt={2}
        >
          {prevDaybook && prevDaybook.today_record === false && (
            <Button variant='contained' onClick={closeDayBook}>
              Close Today's Day Book
            </Button>
          )}
        </Stack>
      </Container>
    </Page>
  );
}
