import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import AddEmployeeForm from "../../components/_dashboard/employee/AddEmployeeForm";

export default function AddEmployee({ open, handleClose }) {
    return (
        <Dialog
            open={open}
            fullScreen={false}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">Add Employee</DialogTitle>
            <DialogContent dividers>
                <AddEmployeeForm handleClose={handleClose} />
            </DialogContent>
        </Dialog>
    );
}
