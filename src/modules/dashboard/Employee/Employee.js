import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import AddEmployee from './AddEmployee';
import { getEmployees } from '../store/employee/EmployeeActions';
import { useDispatch, useSelector } from 'react-redux';
import AddOtherTransactionForm from '../../components/_dashboard/othertransaction/AddOtherTransactionForm';
import MyDialog from '../../components/MyDialog';
import { useHistory } from 'react-router-dom';
//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'phone', label: 'Phone Number', alignRight: false },
  { id: 'position', label: 'Position', alignRight: false },
  { id: 'address', label: 'Address', alignRight: false },
  { id: 'Salary', label: 'Salary', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Employee() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [employees, setEmployees] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const history = useHistory();

  const employeeList = useSelector((state) => state.data.employees);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getEmployees());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log(employeeList);
    setEmployees(employeeList);
  }, [employeeList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = employees.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const viewEmployee = (data) => {
    history.push(`employees/${data.id}`);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - employees.length) : 0;

  const filteredUsers = applySortFilter(
    employees,
    getComparator(order, orderBy),
    filterName
  );

  const addEmployeeTransaction = () => {
    setDialog('addEmployeeTransaction');
    setOpen(true);
  };
  const addEmployeeDialog = () => {
    setDialog('addEmployeeDialog');
    setOpen(true);
  };

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title='Employees'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Employees
          </Typography>
          <Button
            variant='contained'
            onClick={addEmployeeDialog}
            startIcon={<Icon icon={plusFill} />}
          >
            New Employee
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={employees.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, name, phone, position, address, salary } = row;
                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, name)}
                          />
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          <Stack
                            direction='row'
                            alignItems='center'
                            spacing={2}
                          >
                            <Avatar alt={name} src={''} />
                            <Typography variant='subtitle2' noWrap>
                              {name}
                            </Typography>
                          </Stack>
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{phone}</TableCell>
                        </TableCell>
                        <TableCell align='left'>{position}</TableCell>
                        <TableCell align='left'>{address}</TableCell>
                        <TableCell align='left'>Rs. {salary}</TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={addEmployeeTransaction}
                          >
                            Transaction
                          </Button>
                        </TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => viewEmployee(row)}
                          >
                            View
                          </Button>
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={employees.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'addEmployeeDialog' && (
        <AddEmployee open={open} handleClose={() => setDialog(false)} />
      )}
      {open && dialog === 'addEmployeeTransaction' && (
        <MyDialog
          open={open}
          title='Add Employee Transaction'
          handleClose={() => setOpen(false)}
        >
          <AddOtherTransactionForm
            handleClose={() => setOpen(false)}
            type={'employee'}
          />
        </MyDialog>
      )}
    </Page>
  );
}
