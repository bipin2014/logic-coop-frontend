import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import refreshFill from '@iconify/icons-eva/refresh-outline';

// material
import { Stack, Button, Container, Typography, Tabs, Tab } from '@mui/material';
// components
import Page from '../../components/Page';
import { useDispatch, useSelector } from 'react-redux';
import { getAccountTypes } from '../store/accounttype/AccountTypeActions';
import MemberAccountList from './MemberAccountList';
import { getMemberAccounts } from '../store/memberaccount/MemberAccountActions';
import MyDialog from '../../components/MyDialog';
import AddMemberAccountForm from '../../components/_dashboard/memberaccount/AddMemberAccountForm';
import NewMultipleTransactionForm from '../../components/_dashboard/transaction/NewMultipleTransactionForm';
import { PostRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import { setToasterState } from '../../components/Toast/store/ToasterAction';

export default function MembersAccounts() {
  const [tabValue, setTabValue] = useState('');
  const [activeAccountType, setActiveAccountType] = useState('');

  const [currentMemberAccountList, setCurrentMemberAccountList] = useState([]);
  const accountTypeList = useSelector((state) => state.data.account_types);
  const memberAccountList = useSelector((state) => state.data.member_accounts);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    if (accountTypeList.length > 0) {
      setTabValue(accountTypeList[0].id);
      setData(accountTypeList[0].id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountTypeList, memberAccountList]);

  useEffect(() => {
    dispatch(getAccountTypes());
    dispatch(getMemberAccounts());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
    setData(newValue);
  };

  const setData = (id) => {
    const accountType = accountTypeList.find(
      (accountType) => accountType.id === id
    );
    setActiveAccountType(accountType);
    const currentMemberlist = memberAccountList.filter(
      (memberAccount) => memberAccount.account_type_id === id
    );
    setCurrentMemberAccountList(currentMemberlist);
  };

  const addMemberAccount = () => {
    setDialog('addMemberAccount');
    setOpen(true);
  };

  const addMultiTransactions = () => {
    setDialog('addMultiTransactions');
    setOpen(true);
  };
  const refreshAllAccount = () => {
    PostRequest(`${API_URL}/member-account/refresh-balance`)
      .then((res) => {
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Member Account !',
            message: 'Refreshed Member Account!',
          })
        );
        dispatch(getMemberAccounts());
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Page title='Members Accounts'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
        >
          <Typography variant='h4' gutterBottom>
            Members Deposit Accounts
          </Typography>
          <Stack direction='row' alignItems='center' spacing={2}>
            <Button
              variant='contained'
              onClick={refreshAllAccount}
              startIcon={<Icon icon={refreshFill} />}
            >
              Refresh Balance
            </Button>
            <Button
              variant='contained'
              onClick={addMultiTransactions}
              startIcon={<Icon icon={plusFill} />}
            >
              Add Transactions
            </Button>

            <Button
              variant='contained'
              onClick={addMemberAccount}
              startIcon={<Icon icon={plusFill} />}
            >
              New Members Account
            </Button>
          </Stack>
        </Stack>

        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            {accountTypeList.map((accountType) => (
              <Tab value={accountType.id} label={accountType.title} />
            ))}
          </Tabs>
          <MemberAccountList
            memberAccountId={tabValue}
            currentMemberAccountList={currentMemberAccountList}
          />
        </Stack>
      </Container>
      {open && dialog === 'addMemberAccount' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title={'Add Member Account'}
        >
          <AddMemberAccountForm
            accountType={activeAccountType}
            handleClose={() => setOpen(false)}
            memberAccountList={memberAccountList}
          />
        </MyDialog>
      )}
      {open && dialog === 'addMultiTransactions' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title={"Add Today's Transaction"}
        >
          <NewMultipleTransactionForm
            handleClose={() => setOpen(false)}
            memberAccounts={memberAccountList}
            transactionTypeAccount={'deposit'}
          />
        </MyDialog>
      )}
    </Page>
  );
}
