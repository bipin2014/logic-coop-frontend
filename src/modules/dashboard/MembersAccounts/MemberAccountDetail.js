import { useEffect, useState } from 'react';
import { Icon } from '@iconify/react';
// material
import { Container, Stack, Typography, Tab, Tabs, Button } from '@mui/material';
// components
import Page from '../../components/Page';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { API_URL } from '../../../config';
import { GetRequest } from '../../../utils/axios';
import backArrowFill from '@iconify/icons-eva/arrow-back-fill';
import AddMemberAccountForm from '../../components/_dashboard/memberaccount/AddMemberAccountForm';
import TransactionList from '../Transaction/TransactionList';
import refresh from '@iconify/icons-ic/refresh';
import AccountSummary from '../Summary/AccountSummary';

// ----------------------------------------------------------------------

export default function MemberAccountDetail() {
  const { id } = useParams();
  const [memberAccount, setMemberAccount] = useState({});
  const history = useHistory();
  const success = useSelector((state) => state.data.success);

  const [tabValue, setTabValue] = useState('details');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    getMemberAccountDetails();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (success) {
      getMemberAccountDetails();
    }
    // eslint-disable-next-line
  }, [success]);

  const getMemberAccountDetails = () => {
    GetRequest(`${API_URL}/members-accounts/${id}`)
      .then((res) => {
        console.log(res.data);
        setMemberAccount(res.data.data);
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const refreshClientBalance = () => {
    GetRequest(`${API_URL}/member-account/refresh-balance`, { id })
      .then((res) => {
        getMemberAccountDetails();
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const goBack = () => {
    history.goBack();
  };

  return (
    <Page title='Members Accounts Details'>
      <Container>
        <Button
          variant='contained'
          onClick={goBack}
          startIcon={<Icon icon={backArrowFill} />}
        >
          Back
        </Button>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
        >
          <Typography variant='h4' gutterBottom>
            Accounts Details
          </Typography>
          <Typography variant='h4' gutterBottom>
            Account Number: {memberAccount?.account_number}
          </Typography>
          <Typography variant='h4' gutterBottom>
            Balance: Rs. {memberAccount?.balance}
            <Icon icon={refresh} onClick={refreshClientBalance} />
          </Typography>
        </Stack>

        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value={'details'} label={'Details'} />
            <Tab value={'transactions'} label={'Transactions'} />
            <Tab value={'summary'} label={'Summary'} />
          </Tabs>
          {tabValue === 'details' && memberAccount.id && (
            <AddMemberAccountForm
              memberAccount={memberAccount}
              accountType={memberAccount.account_type}
              handleClose={() => {}}
            />
          )}
          {tabValue === 'transactions' && memberAccount.id && (
            <TransactionList
              filterData={{ member_account_id: memberAccount.id }}
            />
          )}
          {tabValue === 'summary' && memberAccount.id && (
            <AccountSummary
              filterData={{
                member_account_id: memberAccount.id,
                type: 'deposit',
              }}
            />
          )}
        </Stack>
      </Container>
    </Page>
  );
}
