import { filter } from 'lodash';
import { useState } from 'react';

// material
import {
  Card,
  Table,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  TableContainer,
  TablePagination,
  Button,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import MyDialog from '../../components/MyDialog';
import MyAlert from '../../components/MyAlert';
import AddNewTransactionForm from '../../components/_dashboard/transaction/AddNewTransactionForm';
import { useHistory } from 'react-router-dom';
import AddMemberAccountForm from '../../components/_dashboard/memberaccount/AddMemberAccountForm';
import closeFill from '@iconify/icons-eva/close-fill';
import { useDispatch } from 'react-redux';
import { closeMemberAccount } from '../store/memberaccount/MemberAccountActions';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'account_number', label: 'Account Number', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'balance', label: 'Balance', alignRight: false },
  { id: 'action', label: 'Action', alignRight: false },
  { id: 'view', label: 'View', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.member.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function MemberAccountList({
  memberAccountId,
  currentMemberAccountList,
}) {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');
  const history = useHistory();
  const dispatch = useDispatch();

  const closeAccount = (data) => {
    setDialogOpen(false);
    dispatch(closeMemberAccount(data.id));
  };

  const closeAccountDialog = (data) => {
    setData(data);
    setDialog('closeAccountDialog');
    setDialogOpen(true);
  };

  const setDialogOpen = (value) => {
    setOpen(value);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = currentMemberAccountList.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - currentMemberAccountList.length)
      : 0;

  const filteredUsers = applySortFilter(
    currentMemberAccountList,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const addTransaction = (data) => {
    setData(data);
    setDialog('addTransaction');
    setDialogOpen(true);
  };

  const viewMemberAccount = (data) => {
    history.push(`/dashboard/members-accounts/${data.id}`);
  };

  const editMemberAccount = (data) => {
    setData(data);
    setDialog('editMemberAccount');
    setDialogOpen(true);
  };

  

  return (
    <Page title='Member Account'>
      <Container>
        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={currentMemberAccountList.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, balance, account_number, member, status } = row;
                    const isItemSelected = selected.indexOf(id) !== -1;
                    const otherItems = [
                      {
                        name: 'Close Account',
                        onClick: () => closeAccountDialog(row),
                        icon: closeFill,
                      },
                    ];

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{account_number}</TableCell>
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>
                            {member?.name || ''}
                          </TableCell>
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>Rs. {balance}</TableCell>
                        </TableCell>

                        {status ? (
                          <TableCell align='left'>
                            <Button
                              variant='contained'
                              onClick={() => addTransaction(row)}
                            >
                              Transaction
                            </Button>
                          </TableCell>
                        ) : (
                          <TableCell align='left'>
                            <Button variant='outlined'>Closed</Button>
                          </TableCell>
                        )}
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => viewMemberAccount(row)}
                          >
                            View
                          </Button>
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            editOnClick={() => editMemberAccount(row)}
                            otherItems={status ? otherItems : []}
                            row={row}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={currentMemberAccountList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'addTransaction' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Transaction'
          fullScreen={true}
        >
          <AddNewTransactionForm
            handleClose={() => setOpen(false)}
            memberAccount={data}
            transactionTypeAccount={'deposit'}
          />
        </MyDialog>
      )}
      {open && dialog === 'editMemberAccount' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title={'Edit Member Account'}
        >
          <AddMemberAccountForm
            accountType={data.account_type}
            memberAccount={data}
            handleClose={() => setOpen(false)}
          />
        </MyDialog>
      )}

      {open && dialog === 'closeAccountDialog' && (
        <MyAlert
          open={open}
          handleClose={() => setOpen(false)}
          handleSubmit={() => closeAccount(data)}
          title='Close Member Account?'
        />
      )}
      
    </Page>
  );
}
