import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setAccountTypes = (payload) => {
  return {
    type: actions.GET_ACCOUNT_TYPES,
    payload: payload,
  };
};

export const getAccountTypes = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/accounttypes`)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setAccountTypes(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addAccountType = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/accounttypes`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Client!',
          message: 'Successfully Added Account Types!',
        })
      );
      dispatch(getAccountTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add accounttypes!',
        })
      );
    });
};

export const updateAccountType = (payload, accountTypeId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PutRequest(`${API_URL}/accounttypes/${accountTypeId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Account Types!',
          message: 'Successfully Updated Account Types!',
        })
      );
      dispatch(getAccountTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to update accounttypes!',
        })
      );
    });
};
