import {
  DeleteRequest,
  GetRequest,
  PostRequest,
} from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setMemberShares = (payload) => {
  return {
    type: actions.GET_MEMBER_SHARES,
    payload: payload,
  };
};

export const getMemberShares = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/member-share`)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setMemberShares(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch member shares!',
        })
      );
    });
};

export const addMemberShare = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PostRequest(`${API_URL}/member-share`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member Share!',
          message: 'Successfully Added Member Share!',
        })
      );
      dispatch(getMemberShares());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add Member Share!',
        })
      );
    });
};

export const deleteMemberShare = (memberShareId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  DeleteRequest(`${API_URL}/member-share/${memberShareId}`)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member Share!',
          message: 'Successfully Deleted Account Types!',
        })
      );
      dispatch(getMemberShares());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to delete member share!',
        })
      );
    });
};
