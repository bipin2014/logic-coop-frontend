import { PostRequest, PutRequest } from '../../../../utils/axios';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';

export const addFiscalYearLoanBalance = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/fiscal-year-loan-balance`, payload)
    .then((res) => {
      dispatch(setSuccess(true));
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Fiscal Year Balance!',
          message: 'Successfully Added Fiscal Year Balance!',
        })
      );
      dispatch(setSuccess(true));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to add Fiscal Year Balance!',
        })
      );
    });
};
export const editFiscalYearLoanBalance = (payload, id) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/fiscal-year-loan-balance/${id}`, payload)
    .then((res) => {
      dispatch(setSuccess(true));
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Fiscal Year Balance!',
          message: 'Successfully Edited Fiscal Year Balance!',
        })
      );
      dispatch(setSuccess(true));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to edit Fiscal Year Balance!',
        })
      );
    });
};
