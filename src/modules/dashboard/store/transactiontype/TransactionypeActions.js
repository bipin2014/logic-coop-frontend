import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setTransactionTypes = (payload) => {
  return {
    type: actions.GET_TRANSACTION_TYPES,
    payload: payload,
  };
};

export const getTransactionTypes = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/transactiontypes`)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setTransactionTypes(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addTransactionType = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/transactiontypes`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Client!',
          message: 'Successfully Added Transaction Types!',
        })
      );
      dispatch(getTransactionTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to add Transactiontypes!',
        })
      );
    });
};

export const updateTransactionType = (payload, id) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/transactiontypes/${id}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      setSuccess(true);
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction Types!',
          message: 'Successfully Updated Transaction Types!',
        })
      );
      dispatch(getTransactionTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to update Transactiontypes!',
        })
      );
    });
};
