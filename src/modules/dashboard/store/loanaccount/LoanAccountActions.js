import {
  DeleteRequest,
  GetRequest,
  PostRequest,
  PutRequest,
} from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setLoanAccounts = (payload) => {
  return {
    type: actions.GET_LOAN_ACCOUNTS,
    payload: payload,
  };
};

export const getLoanAccounts = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/loan-accounts`)
    .then((res) => {
      console.log(res.data);
      dispatch(setLoanAccounts(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to fetch loan account types!',
        })
      );
    });
};

export const addLoanAccount = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/loan-accounts`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan Account !',
          message: 'Successfully Added Loan Account!',
        })
      );
      dispatch(getLoanAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add loan account!',
        })
      );
    });
};

export const updateLoanAccount = (payload, loanAccountId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PutRequest(`${API_URL}/loan-accounts/${loanAccountId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan Account!',
          message: 'Successfully Updated Loan Account!',
        })
      );
      dispatch(getLoanAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to update loan account!',
        })
      );
    });
};
export const deleteLoanAccount = (loanAccountId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  DeleteRequest(`${API_URL}/loan-accounts/${loanAccountId}`)
    .then((res) => {
      console.log(res);
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan Account!',
          message: 'Successfully Deleted Loan Account!',
        })
      );
      dispatch(getLoanAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to delete loan account!',
        })
      );
    });
};

export const closeLoanAccount = (loanAccountId, payload) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PostRequest(`${API_URL}/loan-accounts/close/${loanAccountId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Close Loan!',
          message: 'Loan Account Closed!',
        })
      );
      dispatch(getLoanAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry Cannot Close Loan!',
          message: 'Error Closing Loan Account',
        })
      );
    });
};
