export const GET_LOADING = 'GET_LOADING';
export const GET_EMPLOYEES = 'GET_EMPLOYEES';
export const GET_CLIENTS = 'GET_CLIENTS';
export const GET_MEMBERS = 'GET_MEMBERS';
export const GET_BANK_ACCOUNTS = 'GET_BANK_ACCOUNTS';
export const GET_MEMBER_ACCOUNTS = 'GET_MEMBER_ACCOUNTS';
export const GET_LOAN_ACCOUNTS = 'GET_LOAN_ACCOUNTS';
export const GET_ACCOUNTS = 'GET_ACCOUNTS';
export const GET_ACCOUNT_TYPES = 'GET_ACCOUNT_TYPES';
export const GET_LOAN_ACCOUNT_TYPES = 'GET_LOAN_ACCOUNT_TYPES';
export const GET_TRANSACTION_TYPES = 'GET_TRANSACTION_TYPES';
export const GET_DAY_BOOK = 'GET_DAY_BOOK';
export const GET_DAY_BOOK_LIST = 'GET_DAY_BOOK_LIST';
export const GET_SUCCESS = 'GET_SUCCESS';
export const GET_MEMBER_SHARES = 'GET_MEMBER_SHARES';
