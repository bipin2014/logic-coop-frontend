import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';

export const setMemberAccounts = (payload) => {
  return {
    type: actions.GET_MEMBER_ACCOUNTS,
    payload: payload,
  };
};

export const getMemberAccounts = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/members-accounts`)
    .then((res) => {
      dispatch(setLoading(false));
      console.log(res.data);
      dispatch(setMemberAccounts(res.data.data));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to fetch Member Account!',
        })
      );
    });
};

export const addMemberAccount = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/members-accounts`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member Account!',
          message: 'Successfully Added Member Account!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add Member Account!',
        })
      );
    });
};

export const updateMemberAccount = (payload, memberAccountId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PutRequest(`${API_URL}/members-accounts/${memberAccountId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member Account!',
          message: 'Successfully Added Member Account!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add Member Account!',
        })
      );
    });
};

export const closeMemberAccount = (id) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/members-accounts/close/${id}`)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member Account Closed!',
          message: 'Successfully Closed Member Account!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to close Member Account!',
        })
      );
    });
};
