import { GetRequest, PostRequest } from "../../../../utils/axios";
import * as actions from "../DashboardType";
import { API_URL } from "../../../../config";
import { setToasterState } from "../../../components/Toast/store/ToasterAction";
import { setLoading } from "../client/ClientActions";
export const setAccounts = (payload) => {
    return {
        type: actions.GET_ACCOUNTS,
        payload: payload,
    };
};

export const getAccounts = () => (dispatch) => {
    dispatch(setLoading(true));
    GetRequest(`${API_URL}/accounts`)
        .then((res) => {
            dispatch(setLoading(false));
            dispatch(setAccounts(res.data.data));
        })
        .catch((errors) => {
            dispatch(setLoading(false));
            dispatch(
                setToasterState({
                    open: true,
                    title: "error",
                    name: "Sorry!",
                    message:
                        errors?.response?.data?.message ||
                        "Failed to fetch accounts!",
                })
            );
        });
};

export const addAccount = (payload) => (dispatch) => {
    dispatch(setLoading(true));
    PostRequest(`${API_URL}/accounts`, payload)
        .then((res) => {
            dispatch(setLoading(false));
            dispatch(
                setToasterState({
                    open: true,
                    title: "success",
                    name: "Client!",
                    message: "Successfully Added Account!",
                })
            );
            dispatch(getAccounts());
        })
        .catch((errors) => {
            dispatch(setLoading(false));
            dispatch(
                setToasterState({
                    open: true,
                    title: "error",
                    name: "Sorry!",
                    message:
                        errors?.response?.data?.message ||
                        "Failed to add account!",
                })
            );
        });
};
