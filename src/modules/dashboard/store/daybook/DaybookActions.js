import { API_URL } from '../../../../config';
import { GetRequest, PostRequest } from '../../../../utils/axios';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading } from '../client/ClientActions';
import * as actions from '../DashboardType';
export const setDayBookList = (payload) => {
  return {
    type: actions.GET_DAY_BOOK_LIST,
    payload: payload,
  };
};

export const getDayBookList = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/daybooks`, payload)
    .then((res) => {
      console.log(res.data);
      dispatch(setLoading(false));
      dispatch(setDayBookList(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addDayBook = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/daybooks`, payload)
    .then((res) => {
      console.log(res.data);
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Loan!',
            message: 'Successfully Add Day Book!',
          })
        );
      dispatch(setLoading(false));
      dispatch(getDayBookList());
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};
