import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
export const setClients = (payload) => {
  return {
    type: actions.GET_CLIENTS,
    payload: payload,
  };
};

export const setLoading = (payload) => {
  return {
    type: actions.GET_LOADING,
    payload: payload,
  };
};

export const setSuccess = (payload) => {
  return {
    type: actions.GET_SUCCESS,
    payload: payload,
  };
};

export const getClients = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/clients`)
    .then((res) => {
      dispatch(setLoading(false));
      console.log(res.data);
      dispatch(setClients(res.data.data));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addClient = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/clients`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Client!',
          message: 'Successfully Added Client!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getClients());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message: errors?.response?.data?.message || 'Failed to add client!',
        })
      );
    });
};

export const updateClient = (payload, clientId) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/clients/${clientId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Client!',
          message: 'Successfully Updated Client!',
        })
      );
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to update client!',
        })
      );
    });
};

export const addClientTransaction = (payload, clientId) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/clients/${clientId}/transactions`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction!',
          message: 'Successfully Added Client Transaction!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getClients());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to add client transaction!',
        })
      );
    });
};

export const editClientTransaction =
  (payload, clientId, transactionId) => (dispatch) => {
    dispatch(setLoading(true));
    PutRequest(
      `${API_URL}/clients/${clientId}/transactions/${transactionId}`,
      payload
    )
      .then((res) => {
        dispatch(setLoading(false));
        dispatch(
          setToasterState({
            open: true,
            title: 'success',
            name: 'Transaction Edit!',
            message: 'Successfully Edited Transaction!',
          })
        );
        dispatch(setSuccess(true));
        dispatch(getClients());
      })
      .catch((errors) => {
        dispatch(setLoading(false));
        dispatch(
          setToasterState({
            open: true,
            title: 'error',
            name: 'Transaction Edit!',
            message:
              errors?.response?.data?.message ||
              'Failed to edit client transaction!',
          })
        );
      });
  };

export const addClientLoan = (payload, clientId) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/clients/${clientId}/loans`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan!',
          message: 'Successfully Added Client Loan!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getClients());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add client loan!',
        })
      );
    });
};
export const editClientLoan = (payload, loanId) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/loans/${loanId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan!',
          message: 'Successfully Edited Client Loan!',
        })
      );
      dispatch(setSuccess(true));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add client loan!',
        })
      );
    });
};

export const addMultipleTransaction = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/transactions/multiple`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Multiple Transaction!',
          message: 'Successfully Added Client Transactions!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getClients());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message ||
            'Failed to add client transaction!',
        })
      );
    });
};
export const addMultipleLoan = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/loans/multiple`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Multiple Loan!',
          message: 'Successfully Added Client Loans!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getClients());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add client loans!',
        })
      );
    });
};
