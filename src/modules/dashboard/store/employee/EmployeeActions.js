import { GetRequest, PostRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setEmployees = (payload) => {
  return {
    type: actions.GET_EMPLOYEES,
    payload: payload,
  };
};

export const getEmployees = () => (dispatch) => {
  GetRequest(`${API_URL}/employees`)
    .then((res) => {
      console.log(res.data);
      dispatch(setEmployees(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addEmployee = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/employees`, payload)
    .then((res) => {
      dispatch(setSuccess(true));
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Employee!',
          message: 'Successfully Added Employee!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getEmployees());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message: errors?.response?.data?.message || 'Failed to add employee!',
        })
      );
    });
};
