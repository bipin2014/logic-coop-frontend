import { API_URL } from '../../../../config';
import { PostRequest, PutRequest } from '../../../../utils/axios';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
import { getMemberAccounts } from '../memberaccount/MemberAccountActions';

export const addOtherTransaction = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/othertransactions`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction!',
          message: 'Successfully Added Transaction!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add transaction!',
        })
      );
    });
};

export const updateOtherTransaction = (payload, id) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/othertransactions/${id}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction!',
          message: 'Successfully Added Transaction!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add transaction!',
        })
      );
    });
};
