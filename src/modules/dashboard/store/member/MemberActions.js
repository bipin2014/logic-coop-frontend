import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setMembers = (payload) => {
  return {
    type: actions.GET_MEMBERS,
    payload: payload,
  };
};

export const getMembers = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/members`)
    .then((res) => {
      dispatch(setLoading(false));
      console.log(res.data);
      dispatch(setMembers(res.data.data));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch members!',
        })
      );
    });
};

export const addMember = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/members`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member!',
          message: 'Successfully Added Member!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMembers());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message: errors?.response?.data?.message || 'Failed to add client!',
        })
      );
    });
};
export const mergeMembers = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/members/merge`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member!',
          message: 'Successfully Merged Members!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMembers());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message: errors?.response?.data?.message || 'Failed to add client!',
        })
      );
    });
};

export const updateMember = (payload, memberId) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/members/${memberId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(getMembers());
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Member!',
          message: 'Successfully Updated Member!',
        })
      );
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to update member!',
        })
      );
    });
};
