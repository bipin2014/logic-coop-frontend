import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setLoanAccountTypes = (payload) => {
  return {
    type: actions.GET_LOAN_ACCOUNT_TYPES,
    payload: payload,
  };
};

export const getLoanAccountTypes = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/loan-accounts-type`)
    .then((res) => {
      dispatch(setLoanAccountTypes(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch loan account types!',
        })
      );
    });
};

export const addLoanAccountType = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/loan-accounts-type`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan Account Types!',
          message: 'Successfully Added Loan Account Types!',
        })
      );
      dispatch(getLoanAccountTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add loan accounttypes!',
        })
      );
    });
};

export const updateLoanAccountType = (payload, loanAccountTypeId) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(setSuccess(false));
  PutRequest(`${API_URL}/loan-accounts-type/${loanAccountTypeId}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(setSuccess(true));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Loan Account Types!',
          message: 'Successfully Updated Loan Account Types!',
        })
      );
      dispatch(getLoanAccountTypes());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to update loan accounttypes!',
        })
      );
    });
};
