import { GetRequest, PostRequest } from '../../../../utils/axios';
import * as actions from '../DashboardType';
import { API_URL } from '../../../../config';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
export const setBankAccounts = (payload) => {
  return {
    type: actions.GET_BANK_ACCOUNTS,
    payload: payload,
  };
};

export const getBanks = () => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/bank-accounts`)
    .then((res) => {
      dispatch(setLoading(false));
      console.log(res.data);
      dispatch(setBankAccounts(res.data.data));
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch bank accounts!',
        })
      );
    });
};

export const refreshBanks = () => (dispatch) => {
  GetRequest(`${API_URL}/bank-account/refresh`)
    .then((res) => {
      dispatch(getBanks());
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Bank Account!',
          message: 'Successfully Refreshed Bank Account!',
        })
      );
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch bank accounts!',
        })
      );
    });
};

export const addBankAccount = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/bank-accounts`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Bank Account!',
          message: 'Successfully Added Bank Account!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getBanks());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add Bank Account!',
        })
      );
    });
};

export const updateBankAccount = (payload, id) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/bank-accounts/${id}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Bank Account!',
          message: 'Successfully Added Bank Account!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getBanks());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message: errors?.response?.data?.message || 'Failed to add client!',
        })
      );
    });
};
