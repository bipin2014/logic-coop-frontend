import * as actionTypes from './DashboardType';
const initialState = {
  employees: [],
  clients: [],
  members: [],
  member_accounts: [],
  member_shares: [],
  loan_accounts: [],
  accounts: [],
  account_types: [],
  loan_account_types: [],
  transaction_types: [],
  day_book_list: [],
  banks: [],
  daybook: [],
  loading: false,
  success: false,
};

const DashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_EMPLOYEES:
      return {
        ...state,
        employees: action.payload,
        loading: false,
      };
    case actionTypes.GET_CLIENTS:
      return {
        ...state,
        clients: action.payload,
        loading: false,
      };
    case actionTypes.GET_MEMBERS:
      return {
        ...state,
        members: action.payload,
        loading: false,
      };
    case actionTypes.GET_MEMBER_ACCOUNTS:
      return {
        ...state,
        member_accounts: action.payload,
        loading: false,
      };
    case actionTypes.GET_LOAN_ACCOUNTS:
      return {
        ...state,
        loan_accounts: action.payload,
        loading: false,
      };
    case actionTypes.GET_ACCOUNTS:
      return {
        ...state,
        accounts: action.payload,
        loading: false,
      };
    case actionTypes.GET_ACCOUNT_TYPES:
      return {
        ...state,
        account_types: action.payload,
        loading: false,
      };
    case actionTypes.GET_LOAN_ACCOUNT_TYPES:
      return {
        ...state,
        loan_account_types: action.payload,
        loading: false,
      };
    case actionTypes.GET_TRANSACTION_TYPES:
      return {
        ...state,
        transaction_types: action.payload,
        loading: false,
      };
    case actionTypes.GET_DAY_BOOK:
      return {
        ...state,
        daybook: action.payload,
        loading: false,
      };
    case actionTypes.GET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case actionTypes.GET_SUCCESS:
      return {
        ...state,
        success: action.payload,
      };
    case actionTypes.GET_DAY_BOOK_LIST:
      return {
        ...state,
        day_book_list: action.payload,
      };
    case actionTypes.GET_BANK_ACCOUNTS:
      return {
        ...state,
        banks: action.payload,
      };
    case actionTypes.GET_MEMBER_SHARES:
      return {
        ...state,
        member_shares: action.payload,
      };
    default:
      return state;
  }
};

export default DashboardReducer;
