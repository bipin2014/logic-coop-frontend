import { API_URL } from '../../../../config';
import { GetRequest, PostRequest, PutRequest } from '../../../../utils/axios';
import { setToasterState } from '../../../components/Toast/store/ToasterAction';
import { setLoading, setSuccess } from '../client/ClientActions';
import * as actions from '../DashboardType';
import { getMemberAccounts } from '../memberaccount/MemberAccountActions';
export const setDayBook = (payload) => {
  return {
    type: actions.GET_DAY_BOOK,
    payload: payload,
  };
};

export const getDayBook = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  GetRequest(`${API_URL}/transactions/daybook`, payload)
    .then((res) => {
      console.log(res.data);
      dispatch(setLoading(false));
      dispatch(setDayBook(res.data.data));
    })
    .catch((errors) => {
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to fetch employee!',
        })
      );
    });
};

export const addTransaction = (payload) => (dispatch) => {
  dispatch(setLoading(true));
  PostRequest(`${API_URL}/transactions`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction!',
          message: 'Successfully Added Transaction!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add transaction!',
        })
      );
    });
};

export const updateTransaction = (payload, id) => (dispatch) => {
  dispatch(setLoading(true));
  PutRequest(`${API_URL}/transactions/${id}`, payload)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'success',
          name: 'Transaction!',
          message: 'Successfully Added Transaction!',
        })
      );
      dispatch(setSuccess(true));
      dispatch(getMemberAccounts());
    })
    .catch((errors) => {
      dispatch(setLoading(false));
      dispatch(
        setToasterState({
          open: true,
          title: 'error',
          name: 'Sorry!',
          message:
            errors?.response?.data?.message || 'Failed to add transaction!',
        })
      );
    });
};
