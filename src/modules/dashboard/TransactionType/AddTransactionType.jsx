import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import AddTransactionTypeForm from "../../components/_dashboard/transactiontypes/AddTransactionTypeForm";

export default function AddTransactionType({ open, handleClose }) {
    return (
        <Dialog
            open={open}
            fullScreen={false}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">
                Add Transaction Type
            </DialogTitle>
            <DialogContent dividers>
                <AddTransactionTypeForm handleClose={handleClose} />
            </DialogContent>
        </Dialog>
    );
}
