import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { useDispatch, useSelector } from 'react-redux';
import { getTransactionTypes } from '../store/transactiontype/TransactionypeActions';
import Label from '../../components/Label';
import MyDialog from '../../components/MyDialog';
import AddTransactionTypeForm from '../../components/_dashboard/transactiontypes/AddTransactionTypeForm';

//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'id', label: 'Id', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'action', label: 'Action', alignRight: false },
  { id: 'type', label: 'Type', alignRight: false },
  { id: 'account_type', label: 'Account Type', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function TransactionType() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [transactionTypes, setTransactionTypes] = useState([]);
  const [open, setOpen] = useState(false);
  const [data, setData] = useState('');
  const [dialog, setDialog] = useState('');

  const transactionTypeList = useSelector(
    (state) => state.data.transaction_types
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTransactionTypes());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setTransactionTypes(transactionTypeList);
  }, [transactionTypeList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = transactionTypes.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - transactionTypes.length)
      : 0;

  const filteredUsers = applySortFilter(
    transactionTypes,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const editTransactionType = (data) => {
    setDialog('editTransactionType');
    setData(data);
    setOpen(true);
  };

  const addTransactionType = (data) => {
    setDialog('addTransactionType');
    setOpen(true);
  };

  return (
    <Page title='Transaction Types'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Transaction Type
          </Typography>
          <Button
            variant='contained'
            onClick={addTransactionType}
            startIcon={<Icon icon={plusFill} />}
          >
            New Transaction Type
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={transactionTypes.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, name, action, type, account_type } = row;
                    const isItemSelected = selected.indexOf(name) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, name)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{id}</TableCell>
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          {name}
                        </TableCell>

                        <TableCell align='left'>{action}</TableCell>
                        <TableCell align='left'>
                          <Label
                            variant='ghost'
                            color={(type === 'DEBIT' && 'error') || 'success'}
                          >
                            {type}
                          </Label>
                        </TableCell>
                        <TableCell component='th' scope='row' padding='none'>
                          {account_type}
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            editOnClick={() => editTransactionType(row)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={transactionTypes.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'addTransactionType' && (
        <MyDialog
          title={'Add Transaction Type'}
          open={open}
          handleClose={() => setDialog(false)}
        >
          <AddTransactionTypeForm handleClose={() => setDialog(false)} />
        </MyDialog>
      )}
      {open && dialog === 'editTransactionType' && (
        <MyDialog
          title={'Edit Transaction Type'}
          open={open}
          handleClose={() => setOpen(false)}
        >
          <AddTransactionTypeForm
            handleClose={() => setOpen(false)}
            transactionType={data}
          />
        </MyDialog>
      )}
    </Page>
  );
}
