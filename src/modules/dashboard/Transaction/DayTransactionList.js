import { useEffect, useState } from 'react';
// material
import { Card, Stack, Typography } from '@mui/material';
// components
import { checkOneDigitDate } from '../../../config';
import NepaliDate from 'nepali-date-converter';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';
import TransactionList from './TransactionList';

export default function DayTransactionList({ filterData }) {
  const [nepaliDate, setNepaliDate] = useState(new NepaliDate());
  const [nepaliDateString, setNepaliDateString] = useState('');
  let date = new Date();
  const [dateString, setDateString] = useState('');

  useEffect(() => {
    if (filterData) {
      if (filterData.transaction_date) {
        changeNepaliDate(filterData.transaction_date, true);
      }
    } else {
      setDateString(
        `${date.getFullYear()}-${checkOneDigitDate(
          date.getMonth() + 1
        )}-${checkOneDigitDate(date.getDate())}`
      );
    }
    // eslint-disable-next-line
  }, [filterData]);

  useEffect(() => {
    setNepaliDateString(
      `${nepaliDate.getYear()}-${checkOneDigitDate(
        nepaliDate.getMonth() + 1
      )}-${checkOneDigitDate(nepaliDate.getDate())}`
    );
  }, [nepaliDate]);

  const changeNepaliDate = (value, english) => {
    let date;
    if (english) {
      date = new NepaliDate(new Date(value));
    } else {
      date = new NepaliDate(value);
    }
    setNepaliDate(date);
    let nepalidate = date.getAD();
    setDateString(
      `${nepalidate.year}-${checkOneDigitDate(
        nepalidate.month + 1
      )}-${checkOneDigitDate(nepalidate.date)}`
    );
  };

  return (
    <Card>
      <Stack
        direction='row'
        alignItems='center'
        justifyContent='space-between'
        spacing={2}
        p={2}
      >
        <NepaliDatePicker
          value={nepaliDateString}
          onChange={changeNepaliDate}
        />
        <Typography variant='h4' gutterBottom>
          Transactions of {nepaliDate.format('ddd, MMMM-DD')}
        </Typography>
      </Stack>
      {dateString && (
        <TransactionList
          filterData={
            filterData
              ? { ...filterData, transaction_date: dateString }
              : { transaction_date: dateString }
          }
        />
      )}
    </Card>
  );
}
