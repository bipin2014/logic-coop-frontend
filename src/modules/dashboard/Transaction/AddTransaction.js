import React from "react";
import { Dialog, DialogContent, DialogTitle } from "@mui/material";
import AddTransactionForm from "../../components/_dashboard/transaction/AddTransactionForm";

export default function AddTransaction({ open, handleClose, data }) {
    return (
        <Dialog
            open={open}
            fullScreen={false}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">
                Add Transaction
            </DialogTitle>
            <DialogContent dividers>
                <AddTransactionForm handleClose={handleClose} client={data} />
            </DialogContent>
        </Dialog>
    );
}
