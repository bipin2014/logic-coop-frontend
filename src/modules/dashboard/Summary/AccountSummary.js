import { useEffect, useState } from 'react';
// material
import {
  Card,
  Table,
  Stack,
  TableRow,
  TableBody,
  TableCell,
  Typography,
  TableContainer,
  Grid,
  TableHead,
  CardHeader,
  Box,
} from '@mui/material';
// components
import NepaliDate from 'nepali-date-converter';
import { API_URL, checkOneDigitDate } from '../../../config';
import { NepaliDatePicker } from 'nepali-datepicker-reactjs';
import 'nepali-datepicker-reactjs/dist/index.css';
import { GetRequest } from '../../../utils/axios';
import Page from '../../components/Page';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'remarks', label: 'Transaction', alignRight: false },
  { id: 'amount', label: 'Amount', alignRight: false },
];

// ----------------------------------------------------------------------

export default function AccountSummary({ filterData }) {
  const [debitList, setDebitList] = useState([]);
  const [creditList, setCreditList] = useState([]);
  const [debitTotal, setDebitTotal] = useState(0);
  const [creditTotal, setCreditTotal] = useState(0);
  const [endNepaliDate, setEndNepaliDate] = useState('');
  const [endEnglishDate, setEndEnglishDate] = useState('');
  const [startNepaliDate, setStartNepaliDate] = useState('');
  const [startEnglishDate, setStartEnglishDate] = useState('');
  const [daybookList, setDaybookList] = useState([]);

  useEffect(() => {
    let endDate = new Date();
    let startDate = new Date();
    startDate.setMonth(startDate.getMonth() - 1);

    console.log({ startDate, endDate });

    setEndEnglishDate(
      `${endDate.getFullYear()}-${checkOneDigitDate(
        endDate.getMonth() + 1
      )}-${checkOneDigitDate(endDate.getDate())}`
    );

    setStartEnglishDate(
      `${startDate.getFullYear()}-${checkOneDigitDate(
        startDate.getMonth() + 1
      )}-${checkOneDigitDate(startDate.getDate())}`
    );

    endDate = new NepaliDate(endDate).getBS();
    setEndNepaliDate(
      `${endDate.year}-${checkOneDigitDate(
        endDate.month + 1
      )}-${checkOneDigitDate(endDate.date)}`
    );

    startDate = new NepaliDate(startDate).getBS();
    setStartNepaliDate(
      `${startDate.year}-${checkOneDigitDate(
        startDate.month + 1
      )}-${checkOneDigitDate(startDate.date)}`
    );
  }, []);

  useEffect(() => {
    const getSummary = () => {
      GetRequest(`${API_URL}/transactions/get-account-summary`, {
        ...filterData,
        start_transaction_date: startEnglishDate,
        end_transaction_date: endEnglishDate,
      })
        .then((res) => {
          setDaybookList(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    if (startEnglishDate && endEnglishDate) {
      getSummary();
    }
    // eslint-disable-next-line
  }, [startEnglishDate, endEnglishDate]);

  useEffect(() => {
    if (daybookList) {
      const debits = daybookList.filter(
        (daybook) => daybook.transaction_type.type === 'DEBIT'
      );
      setDebitList(debits);
      let debitTotal = 0;
      debits.forEach((item) => {
        debitTotal += parseFloat(item.total_amount);
      });
      setDebitTotal(debitTotal);

      const credits = daybookList.filter(
        (daybook) => daybook.transaction_type.type === 'CREDIT'
      );
      setCreditList(credits);

      let creditTotal = 0;
      credits.forEach((item) => {
        creditTotal += parseFloat(item.total_amount);
      });
      setCreditTotal(creditTotal);

      console.log({ debits, credits });
    }
  }, [daybookList]);

  const changeStartDate = (value) => {
    let date = new NepaliDate(value);
    let nepalidate = date.getAD();
    setStartEnglishDate(
      `${nepalidate.year}-${checkOneDigitDate(
        nepalidate.month + 1
      )}-${checkOneDigitDate(nepalidate.date)}`
    );
  };
  const changeEndDate = (value) => {
    let date = new NepaliDate(value);
    let nepalidate = date.getAD();
    setEndEnglishDate(
      `${nepalidate.year}-${checkOneDigitDate(
        nepalidate.month + 1
      )}-${checkOneDigitDate(nepalidate.date)}`
    );
  };

  return (
    <Page title='Employees'>
      <CardHeader title='Summary' subheader={JSON.stringify(filterData)} />
      <Box sx={{ p: 3, pb: 1 }}>
        <Stack direction='row' alignItems='center' spacing={1} mb={5}>
          <Stack>
            <Typography>Start Date</Typography>
            <NepaliDatePicker
              onChange={changeStartDate}
              value={startNepaliDate}
              options={{ calenderLocale: 'en', valueLocale: 'en' }}
            />
          </Stack>

          <Stack>
            <Typography>End Date</Typography>
            <NepaliDatePicker
              onChange={changeEndDate}
              value={endNepaliDate}
              options={{ calenderLocale: 'en', valueLocale: 'en' }}
            />
          </Stack>
        </Stack>
        <Grid container spacing={2}>
          <Grid item sm={12} md={6}>
            <Card>
              <Typography variant='h4' textAlign='center' padding='5px'>
                Credit
              </Typography>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {TABLE_HEAD.map((head) => (
                        <TableCell align={head.alignRight ? 'right' : 'left'}>
                          {head.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {creditList.map((row) => {
                      const {
                        id,
                        total_amount,
                        transaction_type,
                        account_type,
                      } = row;

                      return (
                        <TableRow hover key={id} tabIndex={-1}>
                          <TableCell component='th' scope='row'>
                            {account_type?.title} --
                            {transaction_type.name}
                          </TableCell>
                          <TableCell component='th' scope='row' padding='none'>
                            Rs. {total_amount}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                    <TableRow hover key={'credit-total'}>
                      <TableCell component='th' scope='row'>
                        <Typography variant='h5'>Credit Total</Typography>
                      </TableCell>
                      <TableCell component='th' scope='row' padding='none'>
                        <Typography variant='h5'>Rs. {creditTotal}</Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Card>
          </Grid>
          <Grid item sm={12} md={6}>
            <Card>
              <Typography variant='h4' textAlign='center' padding='5px'>
                Debit
              </Typography>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      {TABLE_HEAD.map((head) => (
                        <TableCell align={head.alignRight ? 'right' : 'left'}>
                          {head.label}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {debitList.map((row) => {
                      const {
                        id,
                        total_amount,
                        transaction_type,
                        account_type,
                      } = row;

                      return (
                        <TableRow hover key={id} tabIndex={-1}>
                          <TableCell component='th' scope='row'>
                            {account_type?.title} --
                            {transaction_type.name}
                          </TableCell>
                          <TableCell component='th' scope='row' padding='none'>
                            Rs. {total_amount}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                    <TableRow hover key={'debit-total'}>
                      <TableCell component='th' scope='row'>
                        <Typography variant='h5'>Debit Total</Typography>
                      </TableCell>
                      <TableCell component='th' scope='row' padding='none'>
                        <Typography variant='h5'>Rs. {debitTotal}</Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Card>
          </Grid>
        </Grid>
      </Box>
    </Page>
  );
}
