import {
  Button,
  Card,
  CardActions,
  CardContent,
  Container,
  Stack,
  Typography,
} from '@mui/material';
import React from 'react';
import Label from '../../components/Label';
import Page from '../../components/Page';

const MemberLoanAccount = ({ member, closeLoanAccount }) => {
  return (
    <Page title='Member Loan Account'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Member Loan Account
          </Typography>
        </Stack>
        <Stack spacing={1}>
          {member.loan_accounts?.length > 0 ? (
            member.loan_accounts.map((member_loan_account) => (
              <Card>
                <CardContent sx={{ flex: '1 0 auto' }}>
                  <Typography component='div' variant='h5'>
                    Loan Amount: Rs. {member_loan_account.issue_amount}
                  </Typography>
                  <Typography component='div' variant='h5'>
                    Paid Amount: Rs. {member_loan_account.paid_amount}
                  </Typography>
                  <Typography
                    variant='subtitle1'
                    color='text.secondary'
                    component='div'
                  >
                    Account Number Rs. {member_loan_account.account_number}
                  </Typography>
                  <Stack direction={'row'} spacing={1}>
                    <Label color={'success'}>
                      {member_loan_account.loan_start_date}
                    </Label>
                    <Label color={'success'}>
                      {member_loan_account.loan_end_date}
                    </Label>
                    <Label color={'success'}>
                      {member_loan_account.interest_rate}
                    </Label>
                    <Label
                      variant='ghost'
                      color={member_loan_account.status ? 'success' : 'error'}
                    >
                      {member_loan_account.status ? 'Active' : 'Closed'}
                    </Label>
                  </Stack>
                </CardContent>
                <CardActions>
                  <Button>View Transctions</Button>
                  <Button>Calculate Intrest</Button>
                  <Button
                    variant='contained'
                    onClick={() => closeLoanAccount(member_loan_account)}
                  >
                    Close Loan
                  </Button>
                </CardActions>
              </Card>
            ))
          ) : (
            <div>No Member Loan Accounts</div>
          )}
        </Stack>
      </Container>
    </Page>
  );
};

export default MemberLoanAccount;
