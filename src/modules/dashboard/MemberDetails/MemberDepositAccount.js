import {
  Button,
  Card,
  CardActions,
  CardContent,
  Container,
  Stack,
  Typography,
} from '@mui/material';
import React from 'react';
import Label from '../../components/Label';
import Page from '../../components/Page';

const MemberDepositAccount = ({ member }) => {
  return (
    <Page title='Member Deposit Account'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Member Deposit Account
          </Typography>
        </Stack>
        <Stack spacing={1}>
          {member.member_accounts?.length > 0 ? (
            member.member_accounts.map((member_account) => (
              <Card>
                <CardContent sx={{ flex: '1 0 auto' }}>
                  <Typography component='div' variant='h5'>
                    Balance: Rs. {member_account.balance}
                  </Typography>
                  <Typography
                    variant='subtitle1'
                    color='text.secondary'
                    component='div'
                  >
                    Account Number Rs. {member_account.account_number}
                  </Typography>
                  <Stack direction={'row'} spacing={1}>
                    <Label color={'success'}>
                      {member_account.interest} {member_account.interest_type}
                    </Label>
                    <Label
                      variant='ghost'
                      color={member_account.status ? 'success' : 'error'}
                    >
                      {member_account.status ? 'Active' : 'Closed'}
                    </Label>
                  </Stack>
                </CardContent>
                <CardActions>
                  <Button>View Transctions</Button>
                  <Button>Calculate Intrest</Button>
                </CardActions>
              </Card>
            ))
          ) : (
            <div>No Member Accounts</div>
          )}
        </Stack>
      </Container>
    </Page>
  );
};

export default MemberDepositAccount;
