import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import backArrowFill from '@iconify/icons-eva/arrow-back-fill';

// material
import {
  Stack,
  Button,
  Container,
  Typography,
  Card,
  Tabs,
  Tab,
} from '@mui/material';
// components
import Page from '../../components/Page';
import { useParams, useHistory } from 'react-router';
import { GetRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import { Box } from '@mui/system';
import { useSelector } from 'react-redux';
import AddMemberForm from '../../components/_dashboard/member/AddMemberForm';
import LoanAccountList from '../LoanAccount/LoanAccountList';
import MemberAccountList from '../MembersAccounts/MemberAccountList';
//

export default function MemberDetail() {
  const { id } = useParams();
  const [member, setMember] = useState({});
  const history = useHistory();
  const success = useSelector((state) => state.data.success);

  const [tabValue, setTabValue] = useState('details');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    getMember();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (success) {
      getMember();
    }
    // eslint-disable-next-line
  }, [success]);

  const getMember = () => {
    GetRequest(`${API_URL}/members/${id}`)
      .then((res) => {
        console.log(res.data);
        setMember(res.data.data);
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const goBack = () => {
    history.goBack();
  };
  return (
    <Page title='Member Detail'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Button
            variant='contained'
            onClick={goBack}
            startIcon={<Icon icon={backArrowFill} />}
          >
            Back
          </Button>
          <div>
            <Typography variant='h4' gutterBottom>
              {member.name}
            </Typography>
            <Stack direction='row' alignItems='center'></Stack>
          </div>
          <Stack direction='column' spacing={1}></Stack>
        </Stack>
        <Card>
          <Box sx={{ p: 2 }}>
            <Stack direction='column' spacing={2}>
              <Tabs
                value={tabValue}
                onChange={handleChange}
                textColor='secondary'
                indicatorColor='secondary'
                aria-label='secondary tabs example'
              >
                <Tab value='details' label='Details' />
                <Tab value='depositaccount' label='Deposit Account' />
                <Tab value='loanaccount' label='Loan Account' />
              </Tabs>
              {tabValue === 'details' && member.id && (
                <AddMemberForm member={member} handleClose={() => {}} />
              )}
              {tabValue === 'depositaccount' && member.id && (
                <MemberAccountList
                  currentMemberAccountList={member.member_accounts}
                />
              )}
              {tabValue === 'loanaccount' && member.id && (
                <LoanAccountList
                  currentMemberAccountList={member.loan_accounts}
                />
              )}
            </Stack>
          </Box>
        </Card>
      </Container>
    </Page>
  );
}
