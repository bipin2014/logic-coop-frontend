import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import mergeFill from '@iconify/icons-eva/award-fill';

// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { useDispatch, useSelector } from 'react-redux';
import MyDialog from '../../components/MyDialog';
import { getMembers, mergeMembers } from '../store/member/MemberActions';
import AddMemberForm from '../../components/_dashboard/member/AddMemberForm';
import { useHistory } from 'react-router-dom';
import MyAlert from '../../components/MyAlert';
//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'id', label: 'Member No.', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'phone', label: 'Phone', alignRight: false },
  { id: 'details', label: 'View', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function Members() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [members, setMembers] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');
  const history = useHistory();

  const setDialogOpen = (value) => {
    setOpen(value);
  };

  const memberList = useSelector((state) => state.data.members);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMembers());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setMembers(memberList);
  }, [memberList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = members.map((n) => n.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };
  const mergeTwoMembers = () => {
    const payload = {
      member_one: selected[0],
      member_two: selected[1],
    };
    dispatch(mergeMembers(payload));
    setOpen(false);
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - members.length) : 0;

  const filteredUsers = applySortFilter(
    members,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const viewMember = (data) => {
    history.push(`members/${data.id}`);
  };
  const addMember = () => {
    setDialog('addMember');
    setDialogOpen(true);
  };

  const confirmMerge = () => {
    setDialog('confirmMerge');
    setDialogOpen(true);
  };

  const editMember = (data) => {
    setDialog('editMember');
    setDialogOpen(true);
    setData(data);
  };

  return (
    <Page title='Members'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Members
          </Typography>
          <Stack direction='row' alignItems='center' spacing={2}>
            {selected && selected.length === 2 && (
              <Button
                variant='contained'
                onClick={confirmMerge}
                startIcon={<Icon icon={mergeFill} />}
              >
                Merge Members
              </Button>
            )}
            <Button
              variant='contained'
              onClick={addMember}
              startIcon={<Icon icon={plusFill} />}
            >
              New Members
            </Button>
          </Stack>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={members.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, name, phone } = row;
                    const isItemSelected = selected.indexOf(id) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>
                        <TableCell align='left'>{id}</TableCell>
                        <TableCell component='th' scope='row' padding='none'>
                          <Stack
                            direction='row'
                            alignItems='center'
                            spacing={2}
                          >
                            <Avatar alt={name} src={''} />
                            <Typography variant='subtitle2' noWrap>
                              {name}
                            </Typography>
                          </Stack>
                        </TableCell>
                        <TableCell align='left'>{phone}</TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => viewMember(row)}
                          >
                            View
                          </Button>
                        </TableCell>
                        <TableCell align='right'>
                          <UserMoreMenu editOnClick={() => editMember(row)} />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={members.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {dialog === 'addMember' && (
        <MyDialog
          open={open}
          title='Add Member'
          handleClose={() => setOpen(false)}
        >
          <AddMemberForm open={open} handleClose={() => setDialogOpen(false)} />
        </MyDialog>
      )}
      {dialog === 'confirmMerge' && (
        <MyAlert
          open={open}
          title='Merge Members'
          content='Are you sure want to merge these members?'
          handleClose={() => setOpen(false)}
          handleSubmit={mergeTwoMembers}
        />
      )}
      {dialog === 'editMember' && (
        <MyDialog
          open={open}
          title='Edit Member'
          handleClose={() => setOpen(false)}
        >
          <AddMemberForm
            open={open}
            handleClose={() => setDialogOpen(false)}
            member={data}
          />
        </MyDialog>
      )}
    </Page>
  );
}
