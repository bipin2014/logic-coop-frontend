import { useState } from 'react';
// material
import { Stack, Tab, Tabs } from '@mui/material';
import FiscalYearBalance from './FisacalYearBalance';
import FiscalYearLoanBalance from './FisacalYearLoanBalance';

export default function PreviousBalance() {
  const [tabValue, setTabValue] = useState('saving');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <Stack direction='column' spacing={2}>
      <Tabs
        value={tabValue}
        onChange={handleChange}
        textColor='secondary'
        indicatorColor='secondary'
        aria-label='secondary tabs example'
      >
        <Tab value='saving' label='Saving Account' />
        <Tab value='loan' label='Loan Account' />
      </Tabs>
      {tabValue === 'saving' && <FiscalYearBalance />}
      {tabValue === 'loan' && <FiscalYearLoanBalance />}
    </Stack>
  );
}
