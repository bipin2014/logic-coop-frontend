import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';

// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { GetRequest } from '../../../utils/axios';
import { API_URL } from '../../../config';
import MyDialog from '../../components/MyDialog';
import AddFiscalYearForm from '../../components/_dashboard/fiscalyear/AddFiscalYearForm';
import { useSelector } from 'react-redux';
//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'account_number', label: 'Account Number', alignRight: false },
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'balance', label: 'Balance', alignRight: false },
  { id: 'year', label: 'Year', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.client.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function FiscalYearBalance() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [balances, setBalances] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');
  const success = useSelector((state) => state.data.success);

  const setDialogOpen = (value) => {
    setOpen(value);
  };

  useEffect(() => {
    const getPreviousYearBalance = () => {
      GetRequest(`${API_URL}/fiscal-year-balance`).then((res) => {
        console.log(res.data);
        setBalances(res.data.data);
      });
    };
    getPreviousYearBalance();
    // eslint-disable-next-line
  }, [success]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = balances.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - balances.length) : 0;

  const filteredUsers = applySortFilter(
    balances,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  const addPreviousYearBalance = () => {
    setDialog('addPreviousYearBalance');
    setDialogOpen(true);
  };

  const editPreviousYearBalance = (data) => {
    setDialog('editPreviousYearBalance');
    setDialogOpen(true);
    setData(data);
  };

  return (
    <Page title='FiscalYear Balance'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Fiscal Year Balance
          </Typography>
          <Button
            variant='contained'
            onClick={addPreviousYearBalance}
            startIcon={<Icon icon={plusFill} />}
          >
            Add Balance
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={balances.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, year, balance, client } = row;
                    const isItemSelected = selected.indexOf(id) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>
                            {client.account_number}
                          </TableCell>
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{client.name}</TableCell>
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>Rs. {balance}</TableCell>
                        </TableCell>

                        <TableCell align='left'>{year}</TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            editOnClick={() => editPreviousYearBalance(row)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={balances.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'editPreviousYearBalance' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Edit Fiscal Year Balance'
        >
          <AddFiscalYearForm
            handleClose={() => setOpen(false)}
            fiscalYear={data}
          />
        </MyDialog>
      )}

      {open && dialog === 'addPreviousYearBalance' && (
        <MyDialog
          open={open}
          handleClose={() => setOpen(false)}
          title='Add Fiscal Year Balance'
        >
          <AddFiscalYearForm handleClose={() => setOpen(false)} />
        </MyDialog>
      )}
    </Page>
  );
}
