import { Button } from '@mui/material';
import { useDispatch } from 'react-redux';
import * as actions from '../auth/store/AuthActions';
import { useHistory } from 'react-router-dom';

// ----------------------------------------------------------------------

export default function AccountPopover() {
  const dispatch = useDispatch();
  const history = useHistory();

  const logout = () => {
    console.log('logout');
    dispatch(actions.logout());
    history.push('/');
  };

  return (
    <>
      <Button fullWidth color='primary' variant='outlined' onClick={logout}>
        Logout
      </Button>
    </>
  );
}
