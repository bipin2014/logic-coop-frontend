import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';
// components
import Page from '../../components/Page';
import SearchNotFound from '../../components/SearchNotFound';
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from '../../components/_dashboard/user';
import { useDispatch, useSelector } from 'react-redux';
import MyDialog from '../../components/MyDialog';
import AddMemberShareForm from '../../components/_dashboard/membershare/AddMemberShareForm';
import AddShareTransactionForm from '../../components/_dashboard/sharetransaction/AddShareTransactionForm';
import {
  getMemberShares,
  deleteMemberShare,
} from '../../dashboard/store/membershare/MemberShareActions';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import MyAlert from '../../components/MyAlert';

//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'id', label: 'Id', alignRight: false },
  { id: 'member', label: 'Name', alignRight: false },
  { id: 'unit', label: 'Units', alignRight: false },
  { id: 'amount', label: 'Amounts', alignRight: false },
  { id: '' },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) =>
        _user.member.name.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function MemberShare() {
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('id');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [memberShares, setMemberShares] = useState([]);
  const [open, setOpen] = useState(false);
  const [dialog, setDialog] = useState('');
  const [data, setData] = useState('');
  const history = useHistory();

  const addMemberShare = () => {
    setDialog('addMemberShare');
    setOpen(true);
  };
  const deleteMemberShareConfirm = (data) => {
    setData(data);
    setDialog('deleteMemberShare');
    setOpen(true);
  };

  const deleteMemberShareApi = (id) => {
    setOpen(false);
    dispatch(deleteMemberShare(id));
  };
  const addShareTransaction = (data) => {
    setData(data);
    setDialog('addShareTransaction');
    setOpen(true);
  };

  const viewShareTransaction = (data) => {
    history.push(`/dashboard/member-shares/${data.id}`);
  };

  const memberShareList = useSelector((state) => state.data.member_shares);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMemberShares());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log({ open, dialog });
  }, [open, dialog]);

  useEffect(() => {
    setMemberShares(memberShareList);
  }, [memberShareList]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = memberShares.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - memberShares.length) : 0;

  const filteredUsers = applySortFilter(
    memberShares,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <Page title='Member Shares'>
      <Container>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
          mb={5}
        >
          <Typography variant='h4' gutterBottom>
            Member Shares
          </Typography>
          <Button
            variant='contained'
            onClick={addMemberShare}
            startIcon={<Icon icon={plusFill} />}
          >
            Member Shares
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead
                order={order}
                orderBy={orderBy}
                headLabel={TABLE_HEAD}
                rowCount={memberShares.length}
                numSelected={selected.length}
                onRequestSort={handleRequestSort}
                onSelectAllClick={handleSelectAllClick}
              />
              <TableBody>
                {filteredUsers
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const { id, member, units, amounts } = row;
                    const isItemSelected = selected.indexOf(id) !== -1;

                    return (
                      <TableRow
                        hover
                        key={id}
                        tabIndex={-1}
                        role='checkbox'
                        selected={isItemSelected}
                        aria-checked={isItemSelected}
                      >
                        <TableCell padding='checkbox'>
                          <Checkbox
                            checked={isItemSelected}
                            onChange={(event) => handleClick(event, id)}
                          />
                        </TableCell>
                        <TableCell align='left'>
                          <TableCell align='left'>{id}</TableCell>
                        </TableCell>

                        <TableCell component='th' scope='row' padding='none'>
                          {member?.name}
                        </TableCell>

                        <TableCell align='left'>{units}</TableCell>
                        <TableCell align='left'>{amounts}</TableCell>

                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => addShareTransaction(row)}
                          >
                            Transaction
                          </Button>
                        </TableCell>
                        <TableCell align='left'>
                          <Button
                            variant='contained'
                            onClick={() => viewShareTransaction(row)}
                          >
                            View
                          </Button>
                        </TableCell>

                        <TableCell align='right'>
                          <UserMoreMenu
                            deleteOnClick={() =>
                              deleteMemberShareConfirm(row.id)
                            }
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              {isUserNotFound && (
                <TableBody>
                  <TableRow>
                    <TableCell align='center' colSpan={6} sx={{ py: 3 }}>
                      <SearchNotFound searchQuery={filterName} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
            </Table>
          </TableContainer>

          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component='div'
            count={memberShares.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
      {open && dialog === 'addMemberShare' && (
        <MyDialog
          open={open}
          title='Add Deposit Account Type'
          handleClose={() => setOpen(false)}
        >
          <AddMemberShareForm handleClose={() => setOpen(false)} />
        </MyDialog>
      )}

      {open && dialog === 'addShareTransaction' && (
        <MyDialog
          open={open}
          title='Add Share Transaction'
          handleClose={() => setOpen(false)}
        >
          <AddShareTransactionForm
            handleClose={() => setOpen(false)}
            memberShare={data}
          />
        </MyDialog>
      )}
      {open && dialog === 'deleteMemberShare' && (
        <MyAlert
          open={open}
          handleClose={() => setOpen(false)}
          handleSubmit={() => deleteMemberShareApi(data)}
          title='Delete Transaction'
        />
      )}
    </Page>
  );
}
