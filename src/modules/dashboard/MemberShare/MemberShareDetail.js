import { useEffect, useState } from 'react';
import { Icon } from '@iconify/react';
// material
import { Container, Stack, Typography, Tab, Tabs, Button } from '@mui/material';
// components
import Page from '../../components/Page';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { API_URL } from '../../../config';
import { GetRequest } from '../../../utils/axios';
import backArrowFill from '@iconify/icons-eva/arrow-back-fill';
import refresh from '@iconify/icons-ic/refresh';
import ShareTransactionList from '../ShareTransaction/ShareTransactionList';
import AccountSummary from '../Summary/AccountSummary';

// ----------------------------------------------------------------------

export default function MemberShareDetail() {
  const { id } = useParams();
  const [memberShare, setMemberShare] = useState({});
  const history = useHistory();
  const success = useSelector((state) => state.data.success);

  const [tabValue, setTabValue] = useState('transactions');

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    getMemberShareDetails();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (success) {
      getMemberShareDetails();
    }
    // eslint-disable-next-line
  }, [success]);

  const getMemberShareDetails = () => {
    GetRequest(`${API_URL}/member-share/${id}`)
      .then((res) => {
        console.log(res.data);
        setMemberShare(res.data.data);
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const refreshClientBalance = () => {
    GetRequest(`${API_URL}/member-account/refresh-balance`, { id })
      .then((res) => {
        getMemberShareDetails();
      })
      .catch((errors) => {
        console.log(errors);
      });
  };

  const goBack = () => {
    history.goBack();
  };

  return (
    <Page title='Members Accounts Details'>
      <Container>
        <Button
          variant='contained'
          onClick={goBack}
          startIcon={<Icon icon={backArrowFill} />}
        >
          Back
        </Button>
        <Stack
          direction='row'
          alignItems='center'
          justifyContent='space-between'
        >
          <Typography variant='h4' gutterBottom>
            Name: {memberShare?.member?.name}
          </Typography>
          <Typography variant='h4' gutterBottom>
            Units: {memberShare?.units}
          </Typography>
          <Typography variant='h4' gutterBottom>
            Amounts: Rs.{memberShare?.amounts}
            <Icon icon={refresh} onClick={refreshClientBalance} />
          </Typography>
        </Stack>

        <Stack direction='column' spacing={2}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            textColor='secondary'
            indicatorColor='secondary'
            aria-label='secondary tabs example'
          >
            <Tab value={'transactions'} label={'Transactions'} />
            <Tab value={'summary'} label={'Summary'} />
          </Tabs>
          {tabValue === 'transactions' && memberShare.id && (
            <ShareTransactionList
              filterData={{ member_share_id: memberShare.id }}
            />
          )}
          {tabValue === 'summary' && memberShare.id && (
            <AccountSummary
              filterData={{ member_share_id: memberShare.id, type: 'share' }}
            />
          )}
        </Stack>
      </Container>
    </Page>
  );
}
