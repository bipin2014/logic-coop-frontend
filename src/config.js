export const BACKEND_URL = process.env.REACT_APP_API;
export const API_URL = BACKEND_URL + '/api';
export const DEFAULT_FISCAL_YEAR = '78/79';
export const ORGANIZATION_NAME = 'Logic Multipurpose Co-Operative Ltd';

export const removeEmptyKeyFromObject = (data) => {
  return Object.keys(data)
    .filter((key) => data[key]?.length !== 0)
    .reduce((obj, key) => {
      obj[key] = data[key];
      return obj;
    }, {});
};

export const checkOneDigitDate = (date) => {
  if (date < 10) {
    return `0${date}`;
  }
  return date;
};

export const getFormatedDateFromDateObject = (date) => {
  return `${date.year}-${checkOneDigitDate(date.month + 1)}-${checkOneDigitDate(
    date.date
  )}`;
};

export const addMonthToDate = (date) => {
  let new_month = date.month + 3;
  if (new_month > 11) {
    date.year = date.year + 1;
    date.month = new_month - 12;
  }else{
    date.month = new_month;
  }
  return `${date.year}-${checkOneDigitDate(date.month + 1)}-${checkOneDigitDate(
    date.date
  )}`;
};
