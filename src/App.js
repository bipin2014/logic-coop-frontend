// theme
import ThemeConfig from './theme';
import GlobalStyles from './theme/globalStyles';
//components
import ScrollToTop from './modules/components/ScrollToTop';
import { BaseOptionChartStyle } from './modules/components/charts/BaseOptionChart';
import MainRoute from './routes';
import { useDispatch } from 'react-redux';
import * as actions from './store/action';
import { useEffect } from 'react';
import Toast from './modules/components/Toast'

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    reauthenticate().then((r) => true);
    // eslint-disable-next-line
  }, []);

  const reauthenticate = async () => {
    await dispatch(actions.reauthenticate());
  };
  return (
    <ThemeConfig>
      <ScrollToTop />
      <GlobalStyles />
      <BaseOptionChartStyle />
      <Toast />
      <MainRoute />
    </ThemeConfig>
  );
}

export default App;
