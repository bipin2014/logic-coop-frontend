import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = (props) => {
  const { component: Component, ...rest } = props;
  const isLoggedIn = !!localStorage.getItem('token');

  // useEffect(() => {
  //     !isLoggedIn && openToaster()
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [isLoggedIn])
  console.log(isLoggedIn);

  return (
    <Route
      {...rest}
      render={(props) =>
        isLoggedIn ? <Component {...props} /> : <Redirect to={'/'} />
      }
    />
  );
};

export default PrivateRoute;
