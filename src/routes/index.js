import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Page404 from '../pages/Page404';
import PrivateRoute from './PrivateRoute';
import routeList from './routeList';

const MainRoute = () => {
  return (
    <Switch>
      {routeList.map((route, key) =>
        !route.isAuth ? (
          <Route
            path={route.path}
            name={route.name}
            component={route.component}
            exact={route.exact}
            key={key}
          />
        ) : (
          <PrivateRoute
            path={route.path}
            name={route.name}
            component={route.component}
            exact={route.exact}
            key={key}
          />
        )
      )}
      <Route path='*' component={Page404} />
    </Switch>
  );
};

export default MainRoute;
