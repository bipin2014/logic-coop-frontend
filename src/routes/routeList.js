import AuthLayout from '../modules/auth/AuthLayout';
import DashboardLayout from '../modules/dashboard';

// eslint-disable-next-line
export default [
  {
    path: '/',
    name: 'auth',
    exact: true,
    isAuth: false,
    component: AuthLayout,
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    exact: false,
    isAuth: true,
    component: DashboardLayout,
  },
];
